# Sources du catalogue Les sculptures de la villa romaine de Chiragan
Ce dépôt rassemble les sources du catalogue _Les sculptures de la villa romaine de Chiragan_, un livre numérique et imprimé édité par [le musée Saint-Raymond, musée d'Archéologie de Toulouse](https://saintraymond.toulouse.fr/).

## Livre numérique : site web
Le livre numérique, sous forme de site web, est disponible en ligne : [https://villachiragan.saintraymond.toulouse.fr](https://villachiragan.saintraymond.toulouse.fr/)

## Livre imprimé
La version imprimée sera publiée début 2020 (date à confirmer).

## Chaîne éditoriale et système de publication
Les deux versions du site sont produites à partir d'une même source et par le biais de programmes et logiciels inspirés du domaine du développement web.
Le système de publication modulaire est fortement inspiré par [Quire](https://gettypubs.github.io/quire/), la chaîne de publication numérique de Getty Publications.

Ce catalogue est la concrétisation des principes présentés dans le mémoire de Master d'Antoine Fauchié, [Vers un système de publication modulaire, éditer avec le numérique](https://memoire.quaternum.net).

## Équipe
Liste des différentes personnes qui interviennent sur ce projet éditorial (présentation par ordre alphabétique) :

- Laure Barthet : directrice de la publication, conservateur du Patrimoine, directrice du musée Saint-Raymond, musée d’Archéologie de Toulouse ;
- [Julie Blanc](https://julie-blanc.fr/) : design graphique des versions numérique et imprimée, gestion de projet ;
- [Louis-Olivier Brassard](https://www.loupbrun.ca/) : développement du système de publication pour l’internationalisation (i18n) du catalogue ;
- Pascal Capus : responsable des collections de sculptures romaines et numismatiques, musée Saint-Raymond, musée d’Archéologie de Toulouse ;
- [Antoine Fauchié](https://www.quaternum.net/) : développement du système de publication, gestion de projet ;
- Christelle Molinié : responsable des ressources documentaires, musée Saint-Raymond, musée d’Archéologie de Toulouse ;
- équipe du musée Saint-Raymond, musée d'Archéologie de Toulouse.

## Briques logicielles
Ce catalogue peut être produit grâce à plusieurs logiciels ou programmes, chacun dispose de sa propre licence d'utilisation :

- [Jekyll](https://jekyllrb.com/) : générateur de site statique écrit en Ruby ;
- [Jekyll microtypo](https://github.com/borisschapira/jekyll-microtypo/) : plugin Jekyll pour améliorer la microtypographie ;
- [Jekyll scholar](https://github.com/inukshuk/jekyll-scholar) : plugin Jekyll pour gérer l'affichage des citations bibliographiques et des bibliographies ;
- [Zotero](https://www.zotero.org/) : logiciel et service en ligne pour gérer les références bibliographiques ;
- [Better BibTeX](https://retorque.re/zotero-better-bibtex/) : plugin Zotero pour générer les exports des bibliographies ;
- [Forestry.io](https://forestry.io/) : système de gestion de contenu pour rédiger et organiser les contenus ;
- [Netlify](https://www.netlify.com/) : service en ligne de déploiement et d'hébergement de site statique ;
- [GitLab](https://gitlab.com/) : plateforme de dépôt de code et service en ligne de développement et d'intégration continus ;
- [Sass](https://sass-lang.com/) : préprocesseur CSS pour la génération de feuilles de style en cascade ;
- [paged.js](https://www.pagedmedia.org/paged-js/) : bibliothèque open source pour paginer des pages web, autrement appelé _polyfill_.


### Environnement de programmation
Jekyll, le moteur au centre de la chaîne de publication, requiert un interpréteur Ruby.
De plus, certains plugins de Jekyll ont des caprices liés à des versions particulières ; certains plugins ne fonctionnent d’ailleurs pas avec la version la plus récente de Ruby (ex. `3.2.x`).

**En date de février 2025**, il faut installer la version `3.0` de Ruby, **avec les en-têtes de développement** ; cela est aussi connu sous l’appellation « ruby avec les outils de développement », ou le paquet séparé `ruby-dev` (ou `ruby-devel`, selon les environnements).

`bundler` (le gestionnaire de dépendances Ruby) peut être utilisé pour gérer les paquets Ruby, listées dans le fichier `Gemfile`.

```sh
bundle install
```

Les librairies logicielles seront installées dans le dossier `vendor/bundle/` et peuvent être utilisées à l’aide de `bundle`.

### Git et `git-lfs`
[`git-lfs`](https://github.com/git-lfs/git-lfs) permet d’optimiser la gestion de fichiers statiques, comme des images.
Il est possible de l’activer avec GitLab ([documentation](https://docs.gitlab.com/topics/git/lfs/)).

Si cela est le cas, **il faut installer `git-lfs` sur le poste de développement et l’activer** (sous forme de drapeau ou de variable d’environnement) sur des plateformes tierces, comme c’est le cas avec une plateforme commerciale comme Netlify ([#50](https://gitlab.com/musee-saint-raymond/villa-chiragan/-/issues/50)).

- Lors du premier clonage en local, on peut faire `git lfs clone` (facultatif).
- Dans un projet _déjà cloné_, il faut lancer la commande `git lfs pull`.

## Construction du site
Pour construire le catalogue, on utilisera la commande Jekyll correspondante (`bundle` permet de référencer la version de Jekyll installée spécifiquement pour ce projet) :

```sh
bundle exec jekyll build
```

Le site en français sera construit dans le dossier `_site/`.

### Version anglaise du catalogue
La construction du site en anglais se fait avec une configuration distincte ; il est possible de gérer la génération du catalogue en français et en anglais indépendamment (la lourdeur de la routine de génération rend cette séparation presque nécessaire en pratique).

```sh
bundle exec jekyll build --config config.en_EN.yml
```

Les fichiers seront générés dans le dossier `_site/en/` ; le site sera servi sous le chemin `/en`.

## Servir le site en production
Le dossier `_site/` (ignoré dans le dépôt Git) contiendra les fichiers publics à mettre sur un serveur web.
Les fichiers sont conçus pour être servis à la racine (`/`) d’un domaine, par exemple `https://villachiragan.saintraymond.toulouse.fr/`, et non dans un sous-dossier, comme `mondomaine.org/villachiragan/`.
(Cela pourrait néanmoins être ajusté dans la configuration du site, soit `_config.yml`.)

De plus, le serveur web devrait respecter la **réécriture des adresses** (_rewrite_) avec une extension `.html` : le document `/ra-123.html` devrait être accessible au chemin `/ra-123`.

## Signalement de problèmes avec le site web
Deux options pour signaler un problème :

- ouvrir une _issue_ sur GitLab, pour cela un compte GitLab est nécessaire : [ouvrir une nouvelle issue](https://gitlab.com/musee-saint-raymond/villa-chiragan/issues/new) ;
- écrire à [Antoine Fauchié](mailto:antoine@quaternum.net).
