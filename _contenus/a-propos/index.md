---
title: À propos
layout: a-propos
def: a_propos
type: statique

---
Il revient aujourd’hui au musée Saint-Raymond, en tant que lieu de conservation de l'ensemble des sculptures en marbre de la _villa_ romaine de Chiragan, de répondre aux interrogations d’un public toujours plus nombreux mais souvent dérouté face à ces austères visages impériaux ou à ces dieux qui lui sont plus ou moins familiers. Deux démarches concomitantes ont été entreprises à ce sujet par l’institution toulousaine depuis la restructuration globale du bâtiment et la modernisation des espaces d’exposition à la fin du siècle dernier. D’une part, Daniel Cazes, alors conservateur en chef et directeur, accorda, en 1999, une place importante aux sculptures de la _villa_ dans son ouvrage général du musée, destiné à une large audience. De l’autre, un travail d’ordre scientifique fut engagé par Jean-Charles Balty, dès 1995, auquel se joignit Emmanuelle Rosso, à partir de 2011. La rigueur qui caractérise ces études, enrichies par l’abondance des images, permirent d’apporter un éclairage édifiant sur la série des portraits de Chiragan. Il s’agissait là d’un substantiel outil pour les chercheurs, étudiants et passionnés.

Livrer aujourd’hui une synthèse de ces différentes sources devenait fondamental. Elle s’appuie, en grande partie, sur les études précédemment citées auxquelles s’ajoutent les travaux d’universitaires et de chercheurs internationaux, régulièrement référencés dans ce catalogue. La pertinence de leurs analyses a largement contribué, depuis une quarantaine d’années, au renouvellement de notre regard sur la sculpture romaine. La démarche est bien entendu amplement complétée par le travail de recherche au sein même du musée et la réalisation d’une campagne photographique de l’ensemble des sculptures exposées.

Afin de faciliter l’accès à ces ressources et leur offrir l’opportunité de nourrir d’autres travaux d’étude ou d’être réexploitées dans des contextes et domaines divers, les contenus sont publiés sous licence ouverte en conformité avec la politique d’_open data_ de la mairie de Toulouse. La chaîne d’édition inspirée de celle des publications du Getty Museum, est constituée de modules indépendants et basée sur l’utilisation d’outils _open source_ et collaboratifs à partir desquels est par ailleurs générée la version imprimée. Cette réalisation innovante réinvente la forme traditionnelle du catalogue de collection muséale.

Christelle Molinié et Pascal Capus
