---
title: Buste de Commode
id_inventaire: Ra 118
donnees_biographiques1: 161 - 192
donnees_biographiques2: Empereur de 180 à 192
type_oeuvre: III ou « Vatican, Busti 368 »
date_creation: Fin de l'année 180
materiau: Marbre de Paros II, Chorodaki
hauteur: "44"
largeur: "39"
longueur: ''
profondeur: '23,5'
epaisseur: 
id_wikidata: Q24477272
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 180
priority: 3
id_notice: ra-118
image_principale: ra-118-1
vues: 8
image_imprime: 
redirect_from:
- ra-118
- ark:/87276/a_ra_118
id_ark: a_ra_118
precisions_biblio:
- clef: balty_les_2012
  note: p. 56, 59, 73-74, fig. 55, 57, 71-73, p. 250, 252-256, fig. 179-180, 182-184, 186-188
- clef: braemer_les_1952
  note: p. 143-148
- clef: cazes_musee_1999-1
  note: p. 127
- clef: esperandieu_recueil_1908
  note: p. 68-69, n° 965
- clef: joulin_les_1901
  note: p. 118-119, pl. XX, n° 283 d
- clef: musee_royal_mariemont_au_2018
  note: III.A.2, p. 309
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 12
- clef: musee_saint-raymond_image_2011
  note: p. 72
- clef: rachou_catalogue_1912
  note: p. 59, n° 118
- clef: rosso_image_2006
  note: p. 465-466, n° 223
- clef: wegner_herrscherbildnisse_1939
  note: p. 272
- clef: wegner_verzeichnis_1979
  note: p. 12-116, 180

---
Le sinistre empereur Commode, fils de Marc Aurèle et de Faustine la Jeune, fut le dernier représentant de la dynastie des Antonins. Il naquit dans la _villa_ familiale, située au sud de Rome, dans les monts Albains, à _Lanuvium_. Petit-fils d'Antonin le Pieux, il passa son enfance à la cour et fut associé par son père à l'Empire alors qu'il n'avait que seize ans. Son règne personnel dura douze ans et demi, une période durant laquelle les types iconographiques de cet empereur, fasciné par la gladiature, furent nombreux. On dénombre en effet huit voire neuf types : trois de l'enfance et cinq ou six de l'âge adulte.

Le type III, auquel appartient le buste toulousain, fut créé à l'avènement du prince comme seul empereur à la mort de son père, en 180. Il est ainsi généralement daté des derniers mois de l'année. Ce portrait montre pour la première fois l’empereur portant une barbe complète, couvrant les joues et le menton. Commode est alors âgé de 19 ans, il est désormais un adulte. On connaît une dizaine de répliques de ce type, aujourd'hui conservées au Vatican, dans la Sala dei Busti (368, inv. 617, tête de série, ou «Leitstück», de ce type, en raison de sa qualité et de son état de conservation), Vatican encore, Loggia Scoperta (25 \[16\]), à Copenhague (Glyptothèque Ny Carlsberg 715, inv. 1901), à Liverpool (Ince Blundell Collection 438), Malibu (J. Paul Getty Museum, inv. 92.SA48, autrefois à Castle Howard), à Mantoue (Palazzo Ducale, inv. 6730), qui représente l'exemplaire le plus proche, à bien des égards, de celui de Toulouse, Naples (Museo nazionale), Paris (Louvre MA 1127), Rome (Musée des Thermes 699, inv. 124489).

L'œuvre du musée Saint-Raymond est caractérisée par une imprécision de la chevelure et une sorte d'imprécision au niveau du visage, un étrange flou qui donnent l'impression d'un portrait inachevé. Derrière les oreilles, deux plans de collage et des mortaises sont des traces des fixations de certaines parties qui avaient dû malencontreusement sauter au moment où le sculpteur dégageait le pavillon des oreilles et la nuque.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 249-257.
