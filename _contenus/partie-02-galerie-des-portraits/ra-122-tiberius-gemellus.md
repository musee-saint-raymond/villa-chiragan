---
title: Tête d’adolescent (Tiberius Gemellus ?)
id_inventaire: Ra 122
donnees_biographiques1: 19 - 37/38
donnees_biographiques2: Petit-fils de Tibère
type_oeuvre: "« Capitole-Toulouse »"
date_creation: Années 30 du I<sup>er</sup> siècle
materiau: Marbre lychnites (île de Paros)
hauteur: "26"
largeur: '19,5'
longueur: ''
profondeur: "17"
epaisseur: 
id_wikidata: Q26707665
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 50
priority: 3
id_notice: ra-122
image_principale: ra-122-1
vues: 8
image_imprime: 
redirect_from:
- ra-122
- ark:/87276/a_ra_122
id_ark: a_ra_122
precisions_biblio:
- clef: balty_les_2005
  note: p. 101-118, fig. p. 100, p. 102 (n° 30), 104 (n° 34), 105 (n° 32), 108, 109, 115 (n° 42), 117 (n° 43)
- clef: bergmann_chiragan_1999
  note: p. 28, n° 143
- clef: bonifacio_ritratti_1997
  note: p. 95-96
- clef: boschung_bidlnistypen_1993
  note: p. 48
- clef: boschung_gens_2002
  note: p. 128-129, n° 45.2, pl. 95.2 ("Marcellus ?")
- clef: cazes_musee_1999-1
  note: p. 118, fig. 120
- clef: curtius_ikonographische_1932
  note: p. 229
- clef: curtius_ikonographische_1948
  note: ''
- clef: de_franciscis_il_1951
  note: p. 47-48
- clef: esperandieu_recueil_1908
  note: n° 957
- clef: fittschen_katalog_1985
  note: p. 19-20 à propos du n° 19 (réplique "d" ; "Marcellus")
- clef: giuliano_museo_1987
  note: p. 144
- clef: hannestad_tradition_1994
  note: p. 133, fig. 86-88
- clef: heilmeyer_kaiser_1988
  note: p. 329
- clef: joulin_les_1901
  note: n° 268
- clef: kiss_iconographie_1975
  note: p. 66
- clef: lebegue_notice_1891
  note: p. 415, pl. XXVIII.1
- clef: lebegue_notice_1892
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 242, fig. 102
- clef: musee_saint-raymond_regard_1995
  note: p. 63, fig. 22, p. 87, fig. 45
- clef: pietrangeli_agrippa_1958
  note: p. 159
- clef: pietrangeli_famiglia_1938
  note: p. 44-45
- clef: queyrel_antonia_1992
  note: p. 78-81
- clef: rachou_catalogue_1912
  note: p. 60, n° 122
- clef: rosso_image_2006
  note: p. 440-442, n° 210
- clef: rosso_portrait_1998
  note: ''
- clef: salviat_a_1980
  note: p. 69, fig. 4
- clef: studnicska_agrippa_1917
  note: à propos du n° 1001 p. 10 (Agrippa Postumus)
- clef: trunk_casa_2002
  note: p. 166 à propos du n° 7 (Marcellus)
- clef: west_romische_1933
  note: p. 166-167 (Agrippa Postumus)

---
Ce visage de jeune garçon, encore marqué par certains traits de l'enfance, ne peut qu'être celui d’un prince de la famille impériale julio-claudienne, peut-être datable de la fin du règne de Tibère ou du début de celui de Caligula (autour de 40). L'œuvre est très proche d’une tête, <a href="/images/comp-ra-122-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-122-1.jpg" alt="Marcus Junius Brutus, Musées du Capitole, Carlo Brogi /Wikimedia Commons Public Domain"/></span>conservée à Rome (musées du Capitole)</a>, célèbre depuis le XVIII<sup>e</sup> siècle, qui représente la tête de série («Leitstück»), autrement dit, la réplique d'un type iconographique officiel qui, par sa qualité, doit être considérée comme l'œuvre la plus proche de la création initiale, le prototype. Trois effigies en tout proposent le même type iconographique, à Rome, nous l'avons dit, à Naples (provenant de Pompéi) {% cite bonifacio_ritratti_1997 -L none -l n° 37, p. 94-96, pl. XXXa-d %}, la plus ancienne, et, enfin, la tête toulousaine. Cette dernière fut conçue, de toute évidence, dans le même atelier que le portrait du Capitole. L'adolescent devait tourner la tête vigoureusement, vers la droite, comme le prouve la partie de cou subsistant de ce côté et la dissymétrie qui en résulte. La frange forme une sorte de visière et renvoie à un style de coiffure connue depuis l’époque hellénistique.

On a généralement toujours vu, dans cette image de jeune homme, un membre de la _domus Augusta_, la maison impériale d'Auguste. Marcellus, notamment, neveu d'Auguste, est l'une des identifications proposées {% cite fittschen_katalog_1985 -L none -l n° 19, p. 19-21 %}. Tiberius Gemellus pourrait cependant représenter un meilleur candidat. Né en 19, petit-fils de Tibère, on sait que son frère jumeau mourut en 23. Adopté par son cousin Caligula, obtenant ainsi le statut d’héritier présomptif du trône, il fut assassiné par ce même empereur, en 37 ou 38, sous prétexte de complot. Tiberius avait alors 17 ou 18 ans. Le grand classicisme de l'œuvre transmet une certaine froideur, renforcée par la frange rigide, le poli de la carnation, la finesse du dessin des mèches en virgule, étagées en quinconce. On y reconnaît le style dominant de l'époque de Tibère, dont témoignent les portraits de l'empereur et de son fils, Drusus le Jeune, le père de Tiberius Gemellus.

D'après J.-C. Balty 2005, _Les portraits romains , 1 : Époque julio-claudienne, 1.1_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 99-118.
