---
title: Buste de Marc Aurèle césar
id_inventaire: Ra 61 a
donnees_biographiques1: 121 - 180
donnees_biographiques2: Empereur de 161 à 180
type_oeuvre: "« Offices - Toulouse »"
date_creation: Entre 144 et 147
materiau: Marbre de Göktepe (Turquie)
hauteur: '78,5'
largeur: '56,5'
longueur: 
profondeur: '32,5'
epaisseur: 
id_wikidata: Q24475762
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 150
priority: 3
id_notice: ra-61-a
image_principale: ra-61-a-1
vues: 8
image_imprime: 
redirect_from:
- ra-61-a
- ark:/87276/a_ra_61_a
id_ark: a_ra_61_a
precisions_biblio:
- clef: balty_les_2012
  note: p. 34-35, fig. 28-30, p. 208, 210, 212, 214-215, fig. 137-141, 143-144, 146-148, p. 262, fig.193
- clef: bergmann_marc_1978
  note: p. 40
- clef: bernoulli_romische_1882
  note: 'II : Die Bildnisse der römischen Kaiser. 2 : Von Galba bis Commodus, p. 176, n° 122'
- clef: braemer_les_1952
  note: p. 145
- clef: cazes_musee_1999-1
  note: p. 125
- clef: clarac_musee_1841
  note: p. 587
- clef: du_mege_description_1835
  note: p. 116, n° 205
- clef: du_mege_description_1844
  note: n° 367
- clef: du_mege_notice_1828
  note: p. 65-66, n° 126
- clef: esperandieu_recueil_1908
  note: p. 66, n° 960
- clef: fittschen_prinzenbildnisse_1999
  note: p. 23, pl.
- clef: flisi_questioni_1989
  note: p. 57, n° 37 d
- clef: joulin_les_1901
  note: p. 118 et pl. XX, n° 282 B
- clef: musee_saint-raymond_image_2011
  note: p. 50
- clef: musee_saint-raymond_regard_1995
  note: p. 164
- clef: musees_departementaux_de_loire-atlantique_tresor_1987
  note: p. 76, n° 98, fig. p. 20, 51
- clef: rachou_catalogue_1912
  note: p. 43, n° 61 a
- clef: roschach_catalogue_1892
  note: p. 33, n° 61 a
- clef: rosso_image_2006
  note: p. 460-461, n° 220
- clef: wegner_herrscherbildnisse_1939
  note: p. 20, 204
- clef: wegner_verzeichnis_1979
  note: p. 176

---
Marcus Annius (?) Catilius Severus, futur Marc Aurèle, est né le 26 avril 121 à Rome. Sa mère était la demi-sœur d'Hadrien. Il était donc le neveu de l'empereur. l’_Histoire auguste_ rapporte qu'il « fut élevé sous la surveillance attentive d’Hadrien qui \[...\] lui confia à six ans la dignité du cheval public et l’introduisit à huit ans dans le collège des Saliens \[prêtres danseurs, célébrants du dieu Mars\]. \[...\] Il prit la toge virile au cours de sa quinzième année et fut aussitôt fiancé à la fille de L. Ceionius Commodus, selon la volonté d’Hadrien » . Il fut adopté par Antonin le Pieux au moment où ce dernier l’était lui-même par Hadrien, le 25 février 138 ; il n’avait pas encore dix-sept ans. Il n'est donc pas étonnant qu’il existe, et en assez grand nombre, des portraits du jeune homme. Deux types iconographiques successifs, mais dont la diffusion s’entremêla très vite, le représentent alors dont celui-ci, le deuxième connu pour Marc Aurèle, dit « type Offices – Toulouse », datable des années 140-160/161.

Durant ces vingt à vingt-deux ans (139/140-160/161), qui correspondent presque exactement au règne d’Antonin le Pieux, il hérita du pouvoir sous le nom d’Aurelius Caesar. À la demande d’Antonin, il dut rompre ses fiançailles pour épouser la propre fille de l’empereur, Annia Galeria Faustina, de neuf ans sa cadette. Associé, pour l’année 140, au troisième consulat d’Antonin, il fut également invité à s’installer dans le palais de Tibère (_domus Tiberiana_), au Palatin. Aurelius Caesar avait dix-huit ans en 139 ; il avait presque quarante ans quand il accéda au trône impérial, à la mort d’Antonin, le 7 mars 161. Or, ce n’est qu’à ce moment qu’un nouveau type iconographique allait voir le jour. Cette très grande stabilité des images est en concordance avec Antonin lui-même, qui ne fut représenté, durant toute cette période, que par un seul et même type iconographique.

Mais l’adolescent qu’était encore Aurelius Caesar en 139 était devenu un homme d’âge mûr dans les dix années qui précédèrent son avènement. Père d’une famille nombreuse – Faustine était enceinte de jumeaux, leurs dixième et onzième enfants, au moment de l’avènement –, il ne pouvait être indéfiniment portraituré avec les traits qui avaient été les siens dix, voire quinze ans plus tôt. Comme il arriva quelquefois sous l’Empire, le « type de l’adolescence », sans être fondamentalement modifié, fut progressivement « vieilli » par certains artifices iconographiques. La barbe prend de l’épaisseur et une petite « mouche » ponctue la lèvre inférieure. Avec la moustache, elle gagne encore en volume sur les derniers exemplaires, annonçant ainsi d’assez près le type de l’avènement, dont les traits seront simplement amaigris et la coiffure très légèrement modifiée. Un examen attentif des monnaies autorise à dater avec quelque précision ces différentes adaptations.

Le buste de Toulouse rentre dans un de ces sous-groupes (sous-groupe c de la classification de Klaus Fittschen), qui se situe dans les années 144-147 et qui comporte à ce jour neuf exemplaires, découverts tant en Gaule, en Afrique et en Asie Mineure qu’en Italie. Ce nombre relativement élevé de portraits s’explique très vraisemblablement par le fait qu’Aurelius Caesar fut désigné pour un deuxième consulat en 144, revêtit cette charge en 145 et épousa Faustine au printemps de cette même année. Ce mariage scellait définitivement le choix d’Antonin de le destiner à l’Empire. C’est de ces années décisives que date donc le buste de Chiragan et c’est assurément ce nouveau statut d’Aurelius Caesar qui conduisit à en faire parvenir une image tout officielle dans la _villa_, où elle vint s’ajouter au portrait du souverain régnant et aux effigies impériales des règnes précédents.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 207-218.
