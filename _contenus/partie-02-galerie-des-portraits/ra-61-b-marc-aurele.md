---
title: Buste cuirassé de Marc Aurèle âgé
id_inventaire: Ra 61 b
donnees_biographiques1: 121 - 180
donnees_biographiques2: Empereur de 161 à 180
type_oeuvre: IV (variante A)
date_creation: Entre 170 et 180
materiau: Marbre de Dokimium (Turquie)
hauteur: '76,5'
largeur: '53,5'
longueur: 
profondeur: "29"
epaisseur: 
id_wikidata: Q24266956
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 160
priority: 3
id_notice: ra-61-b
image_principale: ra-61-b-1
image_imprime: 
redirect_from:
- ra-61-b
- ark:/87276/a_ra_61_b
id_ark: a_ra_61_b
bibliographie: oui
vues: 8
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 232, fig. 24
- clef: balty_les_2012
  note: p. 36-37, fig. 31-33, p. 220, 223-225, 226, 228, 231, fig. 151-156, 159-160, p. 262, fig.194
- clef: bergmann_marc_1978
  note: p. 41
- clef: bernoulli_romische_1882
  note: vol. II-2, p. 170, n° 57
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 102-103
- clef: clarac_musee_1841
  note: p. 587
- clef: du_mege_description_1835
  note: p. 116, n° 206
- clef: du_mege_description_1844
  note: n° 367
- clef: du_mege_notice_1828
  note: p. 66, n° 127
- clef: esperandieu_recueil_1908
  note: p. 66-67, n° 961
- clef: fittschen_katalog_1985
  note: p. 74-75, n° 68
- clef: joulin_les_1901
  note: p. 118, pl. XX, n° 281 B
- clef: massendari_haute-garonne_2006-1
  note: p. 244, fig. 107
- clef: musee_archeologique_du_val-doise_tresors_2003
  note: ''
- clef: musee_darcheologie_mediterraneenne_egypte_1997
  note: n° 23, fig.
- clef: musee_petrarque_triomphe_2004
  note: n° 37, p. 92
- clef: musee_royal_mariemont_au_2018
  note: p. 77, fig. 1, III.A.1, p. 309
- clef: musee_saint-raymond_essentiel_2011
  note: p. 44-45
- clef: musee_saint-raymond_image_2011
  note: p. 8, 66
- clef: musee_saint-raymond_regard_1995
  note: p. 165
- clef: rachou_catalogue_1912
  note: p. 43, n° 61 B
- clef: roschach_catalogue_1892
  note: p. 33, n° 61 B
- clef: rosso_image_2006
  note: p. 461-463, n° 221
- clef: sanchez_montes_civilizacion._2006
  note: p. 214
- clef: wegner_herrscherbildnisse_1939
  note: p. 104, 204
- clef: wegner_verzeichnis_1979
  note: p. 176

---
Ce second buste de Marc Aurèle appartient au type IV de l’iconographie de l’empereur, que l’on s’accorde à dater autour des années 170/180, soit des dernières années du règne. Une des raisons les plus vraisemblables pour la création de ce portrait paraît être l’important changement que représentait la mort tout à fait inattendue de Lucius Verus, en janvier ou février 169, après huit ans de règne conjoint avec Marc Aurèle, et le fait que celui-ci se trouvait désormais seul à la tête de l’Empire. Mais il y a plus : le monnayage de l’année 171 comporte de claires allusions à la célébration des _decennalia_ (dix ans de règne) du 7 mars 170 et introduit, dans la titulature de l’empereur, l’épithète _Germanicus_, qui couronna ses campagnes militaires dans ces provinces de Germanie. Serait-ce, par conséquent, plus précisément de ce moment que daterait la création du type IV ?

Durant ces mêmes années, Marc Aurèle n’était certes pas à Rome : ayant quitté l’_Urbs_ en septembre–octobre 169 pour une première expédition, il ne devait y revenir qu’en novembre 176. Force est donc d’admettre que le portrait officiel avait été créé en dehors de Rome, sans doute pendant les quartiers d’hiver de l’armée à _Carnuntum_, en Pannonie (actuelle Autriche), et dépêché à Rome, où les principaux ateliers officiels le copièrent et d’où ils le diffusèrent.

S’il faut, suivant en cela Marianne Bergmann – dont la subdivision de ce quatrième type iconographique en deux sous-groupes n’a cependant pas été retenue par Klaus Fittschen –, distinguer une variante A, à la barbe plus souple et plus large, d’une variante B à la barbe plus compacte, c’est dans le premier sous-groupe que devrait être rangé le portrait toulousain, comme elle l’a fait elle-même. Le buste de Chiragan présente, par ailleurs, deux caractéristiques qui le distinguent de la quasi-totalité des autres statues et bustes cuirassés connus : le protège-nuque y est presque collé au cou, ce qui donne, de profil, la fâcheuse impression d’une cuirasse trop petite pour le personnage qui la porte, et qui l’« étrangle » quelque peu ; ce n’est, vraisemblablement, qu’une maladresse du sculpteur. Plus curieusement, sans doute, le haut du bras droit y était rapporté dans une large mortaise (détail technique que l’on rencontre souvent pour des statues et qui s’explique évidemment, dans ce cas, par le souci d’économiser le matériau en travaillant à part le bras détaché du corps) et le pourtour de la mortaise est cerné par une sorte de collerette dentée que l’on ne retrouve pas sur les autres bustes cuirassés connus à ce jour. Il ne peut s’agir du départ des lanières de cuir qui recouvraient les manches de la tunique et le haut des bras ; elles sont en effet, habituellement, beaucoup plus souples et parfois animées d’un certain mouvement (ces « dents » sont d’ailleurs complètes et ne sauraient être des lanières cassées). Ces éléments semblent fixés au rebord de la cuirasse. Étaient-ils métalliques, comme cette dernière, ou en cuir (?), comme les lanières (ce que leur aspect assez raide, mais différent du reste de la cuirasse, laisserait penser) ?

Très proche, du point de vue de la facture, d’un bel exemplaire du Palazzo Braschi, à Rome, que Klaus Fittschen est tenté d’attribuer à un même atelier, l’œuvre toulousaine en diffère beaucoup au plan technique par la raideur du buste et les détails soulignés ci-dessus, au point que, si l’on peut effectivement songer à un même praticien, maîtrisant parfaitement son métier pour la tête, on admettra que le buste avait été confié à un ouvrier nettement moins habile ou moins expérimenté. On percevrait ainsi une certaine répartition du travail au sein de l’officine, que d’autres observations du même genre, sur d’autres portraits, tendraient à confirmer.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_, Toulouse, p. 220-232.
