---
title: Tête d'inconnu
id_inventaire: Ra 69
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 215 - 220
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "30"
largeur: "19"
longueur: ''
profondeur: "22"
epaisseur: 
id_wikidata: Q27096208
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 330
priority: 3
id_notice: ra-69
image_principale: ra-69-1
vues: 8
image_imprime: 
redirect_from:
- ra-69
- ark:/87276/a_ra_69
id_ark: a_ra_69
precisions_biblio:
- clef: balty_les_2021
  note: p. 27-36, 239-243, fig. 20, 23
- clef: braemer_les_1952
  note: p. 143-147
- clef: du_mege_description_1835
  note: n° 217
- clef: du_mege_notice_1828
  note: n° 135
- clef: esperandieu_recueil_1908
  note: n° 974
- clef: joulin_les_1901
  note: n° 313 E
- clef: meischneir_privatportrats_1984
  note: p. 319-351
- clef: rachou_catalogue_1912
  note: n° 69
- clef: roschach_catalogue_1892
  note: n° 69

---
La tête, cubique et vigoureusement structurée, ne manque pas de frapper par son expression très particulière, ses yeux aux paupières supérieures très marquées et assez lourdes sous des arcades sourcilières fortement ouvertes et un regard nettement dirigé vers la droite, mais aussi par son profil au front bas et bombé, au départ du nez très marqué, à la lèvre inférieure courte et rentrante. Les cheveux très près du crâne et peignés en longues mèches à pointe bifide — prélude aux coiffures « _a penne_ » des années 215-250 —, la moustache relativement courte et la barbe bouclée, taillée assez bas sur le menton, renvoient à un type de coiffure et une mode caractéristiques du milieu et de la fin de l’époque sévérienne dont un portrait des réserves des Musées Capitolins (inv. 3387), autrefois dans le parc de la Villa Borghèse, fournit un bon exemple et constitue en même temps un précieux parallèle stylistique pour la tête toulousaine.

D'un autre côté, les effigies du type « 2. Thronfolgertypus » (ou « Consulatstypus ») de Caracalla, datées des années 208 et suivantes, pour leur coiffure en mèches rases — mais beaucoup plus courtes — et leur barbe bouclée, constituent également un utile repère chronologique.                                                                                                    Plusieurs portraits du deuxième quart du siècle ont, par ailleurs, le même regard « en coin » : pupille et iris vigoureusement déplacés dans l’angle des yeux de façon quelque peu « expressionniste » y connotent, comme on l’a souvent écrit, l’inquiétude, voire l’angoisse de cette période de crise dans laquelle plonge l‘Empire avec les « empereurs soldats » (« Soldatenkaiser »).

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 239-243.