---
title: Buste à paludamentum frangé d’un haut fonctionnaire de l’Empire
id_inventaire: Ra 72
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: dit « Palazzo Corsini – Toulouse »
date_creation: Fin du II<sup>e</sup> - début du III<sup>e</sup> siècle
materiau: Marbre de Göktepe 4 (Turquie)
hauteur: "82"
largeur: "58"
longueur: ''
profondeur: "34"
epaisseur: 
id_wikidata: Q27096201
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 290
priority: 3
id_notice: ra-72
image_principale: ra-72-1
vues: 8
image_imprime: 
redirect_from:
- ra-72
- ark:/87276/a_ra_72
id_ark: a_ra_72
precisions_biblio:
- clef: balty_les_2021
  note: p. 41-44, 177-190
- clef: bergmann_studien_1977
  note: p.33
- clef: du_mege_description_1835
  note: n° 225
- clef: du_mege_notice_1828
  note: n° 140
- clef: esperandieu_recueil_1908
  note: n° 983
- clef: joulin_les_1901
  note: n° 310
- clef: rachou_catalogue_1912
  note: n° 72
- clef: roschach_catalogue_1865
  note: p. 36-37, n°72
- clef: roschach_catalogue_1892
  note: n° 72
- clef: wood_roman_1986
  note: ''

---
M. Bergmann {% cite bergmann_studien_1977 -L none -l n. 104 p. 33 %} remarqua que le buste toulousain figurait le même personnage qu’un portrait du Palazzo Corsini, à Rome. Il s'agit donc, une fois encore, d'un portrait privé dont nous ont été conservées, par chance, au moins deux répliques. On sait, en effet, que les plus grands personnages de l’Empire ont été honorés de nombreuses statues et bustes, à la fois dans la ville dont ils étaient originaires et dans celles où ils exercèrent leur charge administrative, les inscriptions sont là pour le rappeler.

Les deux œuvres, romaine et toulousaine, montrent des caractéristiques absolument identiques : proportions et expression du visage, dessin des arcades sourcilières, des yeux, des lèvres et de la moustache, dessin et volume de la barbe, dessin du nez, fortement aquilin, des narines et des plis naso-labiaux. Seuls divergent le dessin et le modelé des mèches de la coiffure sur les tempes et à l’arrière du crâne, et le fait que les oreilles soient entièrement dégagées à Toulouse alors même que l’ourlet supérieur est légèrement recouvert par la chevelure à Rome. Ces mèches sont, sur l'exemplaire de Toulouse, dessinées avec un soin extrême, beaucoup plus «individualisées» et refendues de fins canaux de trépan destinés à leur donner davantage de matière tandis que celles du portrait Corsini paraissent banales et « mécaniques », comme si le sculpteur avait manqué de modèle pour cette partie-là de son travail et recopié, de façon presque aléatoire, ces boucles coquillées que l’on trouve sur tous les portraits de l’époque antonine et du début de l’époque sévérienne. On n’en regrettera que davantage l’effroyable massacre perpétré à Toulouse sur le dessus du crâne pour faire adhérer une chevelure de plâtre. Sur ce point précis, la réplique Corsini est donc bien un témoin privilégié du dessin de la frange frontale qui, comme tout ce qui touchait à la face de ce portrait, avait dû suivre avec précision le modèle fourni.

Le grand manteau (_paludamentum_), à franges, est maintenu sur l’épaule droite par une grosse fibule ronde en forme de rosace et recouvre tout le torse ; seuls l’épaule et le départ du bras droit s’en dégagent. Ce type de vêtement, d'origine tardo-hellénistique, a été mis en relation avec des fonctions militaires. Dans l’état actuel de notre documentation, il semble bien que les bustes revêtus du _paludamentum_ frangé les plus anciens soient apparus sous le règne d’Antonin le Pieux. Le type de buste où le paludamentum frangé recouvre la tunique, apparu pour des représentations d’empereurs sous le règne de Marc Aurèle et Lucius Vérus et attesté jusque sous celui de Septime Sévère, aurait été repris par le portrait privé dès l’époque antonine et aurait connu une réelle faveur à la fin de cette période et au début du règne de Sévère.

Ce que l’on retrouve, à Chiragan, le portrait d’un haut fonctionnaire dont les collections d’antiques de Rome et d’Italie conservent également un exemplaire ne surprendra pas. Si la « villa » de Martres-Tolosane est bien le domaine impérial que nous avons cru pouvoir identifier, c’est à des effigies de procurateurs du patrimoine impérial (_patrimonium principis_) que l’on songera mais aussi peut-être à l’image de ceux qui géraient pour le compte de l’empereur carrières et forêts voisines et étaient sans doute également en poste à Chiragan.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 177-190.