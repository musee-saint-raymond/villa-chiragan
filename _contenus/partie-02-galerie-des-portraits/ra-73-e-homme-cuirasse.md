---
title: Buste d’homme cuirassé
id_inventaire: Ra 73 e
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 130
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "78"
largeur: '54,5'
longueur: 
profondeur: "29"
epaisseur: 
id_wikidata: Q24520896
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 90
priority: 3
id_notice: ra-73-e
image_principale: ra-73-e-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-e
- ark:/87276/a_ra_73_e
id_ark: a_ra_73_e
precisions_biblio:
- clef: balty_les_2012
  note: p. 43-44, fig. 42-43, p.165-168, 170, fig. 90-91, 93, 95, 97-100, p. 268, fig. 202
- clef: cazes_musee_1999-1
  note: p. 136
- clef: du_mege_description_1835
  note: p. 119, n° 211
- clef: du_mege_description_1844
  note: n° 373
- clef: du_mege_notice_1828
  note: p. 68, n° 131
- clef: esperandieu_recueil_1908
  note: p. 76-77, n° 978
- clef: joulin_les_1901
  note: pl. XXIII, n° 302 b
- clef: musee_saint-raymond_regard_1995
  note: p. 158
- clef: rachou_catalogue_1912
  note: p. 48-49, n° 73 e

---
Seuls les empereurs se faisaient représenter en buste cuirassé, et ce jusqu'au règne d'Hadrien, période durant laquelle les exemplaires demeurent relativement rares. Ce portrait est très proche de plusieurs bustes de l'empereur, notamment par certains détails, comme la tête de Gorgone (_gorgoneion_) aux ailes étrangement relevées vers le haut, sur le plastron de la cuirasse.

Le regard très expressif, sous des arcades sourcilières saillantes et froncées, le nez en bec d’aigle et la bouche dédaigneuse rappellent que le courant réaliste du portrait romain demeure vivace. Cette veine réaliste caractérise sans doute, tout particulièrement, les portraits de militaires et, dans une moindre mesure, ceux de philosophes et rhéteurs, tributaires d’une iconographie née en Grèce vers la fin de l’époque classique ; voire ceux d’une certaine « classe moyenne », celle des esclaves affranchis notamment.

Ces inconnus portant la cuirasse, membres de l'ordre équestre, ne sont pas (ou plus) des militaires. Probablement doit-on les considérer comme des hommes ayant brigué et obtenu la fonction de procurateur. Ces hauts fonctionnaires endossaient, dans les provinces, des charges administratives et fiscales. Autorisés, depuis le règne d'Auguste, à porter l'habit militaire, ils affichaient, jusque dans leurs représentations de marbre, leur statut de chevalier.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 163-170.
