---
title: Tête d’inconnu
id_inventaire: Ra 73 f
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 195 - 205
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "29"
largeur: '23,1'
longueur: 
profondeur: '23,7'
epaisseur: 
id_wikidata: Q48323047
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 310
priority: 3
id_notice: ra-73-f
image_principale: ra-73-f-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-f
- ark:/87276/a_ra_73_f
id_ark: a_ra_73_f
precisions_biblio:
- clef: balty_les_2021
  note: p. 48-54, 199-204
- clef: rachou_catalogue_1912
  note: n° 73 f
- clef: roschach_catalogue_1892
  note: n° 73 f
- clef: joulin_les_1901
  note: n° 275
- clef: esperandieu_recueil_1908
  note: n° 951

---
On peut hésiter quelque peu sur la datation précise du portrait de Chiragan, indéniablement issu de l'un des ateliers de l'_Urbs_ (Rome) : époque antonine tardive ou bien début du règne de Septime Sévère. C’est plutôt, cependant, de ce dernier règne que de celui de Marc Aurèle ou de Commode que l’on serait tenté de dater cet usage accentué du trépan qui déchiquète les boucles de la chevelure sur les tempes et les mèches d’une barbe portée plus rase que sous les derniers Antonins. On rapprochera donc le portrait des effigies de Septime Sévère plutôt que de celles de ses prédécesseurs.

Il s'agit ici d'un personnage d’un certain âge, dont les traits étaient rendus avec un réalisme certes moins prononcé que celui qui s'affiche au niveau du [buste d'inconnu inv. Ra 70](/ra-70), mais bien réel : qu’on en juge par le froncement des sourcils et une certaine fixité dans le regard, mais aussi par l’accentuation des plis naso-labiaux et cette curieuse expression de la bouche légèrement entrouverte.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 199-204.