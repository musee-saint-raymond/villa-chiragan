---
title: Tête de femme
id_inventaire: Ra 82
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Époque théodosienne. Vers 375 - 425
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: '33,5'
largeur: "30"
longueur: 
profondeur: "30"
epaisseur: 
id_wikidata: Q24636324
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 5
order: 440
priority: 3
id_notice: ra-82
image_principale: ra-82-1
vues: 8
image_imprime: 
redirect_from:
- ra-82
- ark:/87276/a_ra_82
id_ark: a_ra_82
precisions_biblio:
- clef: alfoldi-rosenbaum_portrait_1968
  note: p. 35-40, fig. 25-28
- clef: balmelle_les_2001
  note: p. 83, fig. 22, p. 229-230
- clef: baratte_histoire_1996
  note: p. 250-251
- clef: beckmann_idiom_2020
  note: ''
- clef: cazes_periple_2003
  note: p. 198-201, n° 364
- clef: centre_culturel_abbaye_de_daoulas_rome_1993
  note: n° 85.07
- clef: delbrueck_spatantike_1933
  note: n° 49 f, fig. 19
- clef: du_mege_description_1835
  note: p. 134-135, n° 239
- clef: du_mege_notice_1828
  note: p. 100, n° 242
- clef: ensoli_aurea_2000
  note: ''
- clef: esperandieu_recueil_1908
  note: p. 100, n° 1030
- clef: eydoux_france_1962-1
  note: chap. XI, p. 179
- clef: eydoux_monuments_1958
  note: couv.
- clef: gauzi_au_1925
  note: p. 84-85
- clef: heintze_spatantikes_1971
  note: p. 90, n° 104
- clef: kiilerich_late_1993
  note: p. 123-124, p. 354, fig. 68 bis
- clef: meischner_bildnisse_2001
  note: p. 403, pl. 92-fig. 2
- clef: meischner_portrat_1991
  note: p. 385‑407, n° 106
- clef: musee_saint-raymond_essentiel_2011
  note: p. 48-49
- clef: musee_saint-raymond_regard_1995
  note: p. 45, n° 7
- clef: musee_saint-raymond_wisigoths_2020
  note: p. 71-72
- clef: mussot-goulard_les_1999
  note: p. 114
- clef: popova-moroz_roman_1992
  note: p. 13-21. (bibl. de comp.)
- clef: rachou_catalogue_1912
  note: p. 52, n° 82
- clef: roschach_catalogue_1892
  note: p. 38, n° 79
- clef: stutzinger_bronzebildnis_1986
  note: p. 155, n° 37, pl. 26 b
- clef: veyne_kunst_2009
  note: p. 112
- clef: veyssiere_occupation_2008
  note: p. 327

---
Ce portrait féminin se distingue nettement de tous les autres visages qui furent mis au jour à Chiragan. Du reste, et malgré les assertions des conservateurs Ernest Roschach (1865) et Henri Rachou (1912), rien ne nous permet de confirmer que l'œuvre provienne bien de la _villa_ des bords de Garonne.

Le regard, tour à tour sévère et mélancolique selon les angles de vue, impose un profond respect. Les pommettes sont saillantes, à l'image du menton, les yeux cernés, les paupières lourdes et le profil anguleux des arcades sourcilières se prolonge, de manière graphique et géométrique, en un nez long et fin. Les sillons naso-labiaux traduisent un âge avancé, peut-être antérieur à quarante ans {% cite cazes_periple_2003 -l 200 %}. La coiffure est relativement proche d'un <a href="/images/comp-ra-82-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-82-1.jpg" alt="Buste de femme au parchemin, inv. 66.25 Metropolitan museum, Public Domain http://www.metmuseum.org/art/collection/search/468716"/></span>buste byzantin de jeune femme, conservé au Metropolitan Museum of Art de New York</a> {% cite alfoldi-rosenbaum_portrait_1968 -L none -l p. 21, fig. 2-5 %}.

Schématisme et solennité de ce visage altier autorisent une datation tardive. L'œuvre est contemporaine de la séparation de l'Empire romain d’Orient et de l'Empire romain d’Occident, depuis la mort de Théodose I, en 395. Ainsi le règne de cet empereur a-t-il pu servir de repère. Il correspondrait même probablement, à notre avis, à la date la plus haute, le _terminus post quem_ à partir duquel il faudrait situer ce portrait, la date limite la plus basse pouvant se rapporter à la fin du règne de Théodose II, au milieu du V<sup>e</sup> siècle. Théodosienne serait bien, par conséquent, l'époque dans laquelle s'inscrit ce visage saisissant. Cependant, et contrairement ce qu'il fut légitime de croire, l'œuvre, qui appartint peut-être à une statue, ne fut conçue ni en Orient ni à Rome dans un atelier oriental mais bien au pied des Pyrénées. Les analyses entreprises par D. Attanasio, M. Bruno et W. Prochaska révèlent en effet que le marbre provient de Saint-Béat (Haute-Garonne). Dans le cas où cette tête proviendrait de Chiragan, cette seule information prouverait non seulement le maintien de l'activité de la _villa_ durant cette époque tardive (une monnaie de l'époque de Théodose et d'Arcadius a par ailleurs été retrouvée sur le site) mais également l'importance de ses propriétaires au service desquels travaillaient des ateliers aguerris.

Un autre document archéologique, récemment mis au jour, doit être tout particulièrement considéré dans le cadre de l'étude de ce portrait d'origine incertaine. Il s'agit d'une tête en calcaire, découverte en 2005 dans le cadre d'une intervention archéologique menée par l'Inrap, aux portes de Toulouse, sur le site du Barricou, à Beauzelle. Elle fut découverte dans les terres de comblement de l'un des puits de la ferme à vocation agro-pastorale {% cite veyssiere_occupation_2008 -L none %}. Le même voile caractéristique circonscrit le visage en formant un bandeau, qui prend ici l'aspect d'un gros boudin, et encadre un visage mûr, aux yeux immenses. L'effigie de la dame de Beauzelle entre dans le champ catégoriel d'une sculpture conventionnellement dite "provinciale". Si le portrait est ingrat et maladroit, il autorise donc pourtant la prise en considération d'un modèle commun et représente par conséquent un intéressant parallèle. Il témoignerait de la notoriété d'un personnage au service duquel fut mise en place un type de représentation aisément identifiable. Celle qui pour nous, aujourd'hui, est une inconnue devait donc incarner une puissance certaine. L'éminent personnage appartenait-il au milieu impérial?

Il faut rappeler, enfin,  qu'une importante production statuaire de petit et moyen format, issue des décombres de la _villa_, remonterait au IV<sup>e</sup> siècle voire à la première moitié du siècle suivant. Qu'elle ait été héritée des précédents propriétaires ou d'éventuels aïeux ou encore fraîchement acquise, la sculpture mythologique, passée par le filtre esthétique de l’Orient romain, demeure bien en usage au cœur même du V<sup>e</sup>e siècle. Ainsi, ce portrait, au cœur d'un tel ensemble, ne serait-il pas aussi isolé que ce que l'on a bien voulu penser.

P. Capus