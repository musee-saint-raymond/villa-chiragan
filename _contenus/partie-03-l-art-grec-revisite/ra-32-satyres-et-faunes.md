---
title: Repos de deux faunes dans un paysage
id_inventaire: Ra 32
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 'I<sup>er</sup>-II<sup>e</sup> siècle '
materiau: Marbre
hauteur: "35"
largeur: "35"
longueur: ''
profondeur: "8"
epaisseur: 
id_wikidata: Q28732530
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 120
priority: 3
id_notice: ra-32
image_principale: ra-32-1
vues: 1
image_imprime: 
redirect_from:
- ra-32
- ark:/87276/a_ra_32
id_ark: a_ra_32
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 183
- clef: du_mege_notice_1828
  note: n° 91
- clef: esperandieu_recueil_1908
  note: n° 932
- clef: joulin_les_1901
  note: n° 171
- clef: massendari_haute-garonne_2006-1
  note: p. 257, fig. 145
- clef: rachou_catalogue_1912
  note: n° 32
- clef: roschach_catalogue_1892
  note: n° 32

---
Outre le tableau de l'enlèvement de Perséphone, qui correspond à une catégorie d'œuvre très rare, un second _pinax_ de marbre provient, lui aussi, de la _villa_. Il représente deux satyres, masculin et féminin, reconnaissables, au premier regard, à leurs pattes de boucs. La scène se déroule devant une grotte, au pied d'un pin pinier (_pinus pinea_) ou pin parasol. La créature de droite, couchée, prend appui sur son bras gauche, autour duquel est enroulée la pardalide (peau de panthère). Il tend le bras en direction de l'une des trois pommes de pin (_strobilos_) visibles dans l'arbre. Le satyre féminin lui fait face ; plus petite, elle est assise sur un rocher, le corps enveloppé dans sa peau de bouc, dont l'une des pattes retombe au-devant de l'épaule gauche. Les créatures sont malheureusement acéphales. Ne reste que la trace d'un tenon, à droite, vestige de la liaison de la tête de la créature couchée avec la surface du tableautin. À ces décapitations, s'ajoutent d'autres lacunes : jambe gauche du satyre ou encore bras et jambe du côté droit de la satyresse, également brisés. Enfin, comparant ce relief au tableau montrant Hadès et Perséphone, on constate ici quelques maladresses de proportions et peut-être une plus grande hâte dans l'exécution.

Petits génies champêtres, les satyres évoquent la nature sauvage, à l'image des nymphes ou des sylvains. Le thème est en étroite correspondance avec Dionysos, dont les proches compagnons, comme les symboles, sont largement majoritaires dans le décor de la _villa_. En témoignent également les dizaines de fragments conservés dans les réserves du musée. La représentation des satyres, partenaires du dieu des forces sauvages du monde, est donc en parfait accord avec cet environnement bachique.

Les pommes de pin font allusion au cône fixé sur la hampe (thyrse) du dieu d'origine orientale. Par ailleurs, à travers la grotte résonne le passage vers un autre monde dont l'antre demeure un symbole puissant. Ce thème de la limite, associé à ceux du mystère et de l'au-delà, semble particulièrement récurrent dans l'iconographie. Il fut visiblement privilégié par des propriétaires probablement férus de messages spirituels, philosophiques comme eschatologiques. La cavité naturelle nous rappelle aussi un épisode de l'enfance du dieu, lorsque les nourrices placèrent le fils de Zeus et de Sémélé dans une boîte en bois de pin et le cachèrent dans une grotte, afin qu'il ne soit pas atteint par la colère de la jalouse Héra {% cite motte_lexpression_1986 -l 215 %}. On n'oubliera pas non plus que, dans le cadre des mystères dionysiaques, certaines initiations, les catabases (descente symbolique dans le monde des Enfers) étaient apparemment organisées dans des antres rocheux {% cite burkert_les_2003 -l 95 %}. Ainsi Pausanias évoque-t-il la «grotte-sanctuaire de Dionysos» (_Périégèse_, V, 19, 6). L'ambiance bucolique, inhérente aux créatures qui forment le cortège (thiase) et l'environnement de Dionysos/Bacchus, mais également une certaine atmosphère spirituelle sont donc symbolisées par ces deux élements naturels que sont le pin et la grotte.

P. Capus