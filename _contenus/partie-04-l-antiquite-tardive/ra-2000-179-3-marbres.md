---
title: Plaque de décor
id_inventaire: 2000.179.3
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup>-IV<sup>e</sup> siècle
materiau: Marbre des Pyrénées
hauteur: '25,5'
largeur: "28"
longueur: 
profondeur: "3"
epaisseur: 
id_wikidata: Q48414269
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 450
priority: 3
id_notice: 2000-179-3
image_principale: 2000-179-3-1
vues: 1
image_imprime: 
redirect_from:
- 2000-179-3
- ark:/87276/a_2000_179_3
id_ark: a_2000_179_3
precisions_biblio:
- clef: cazes_musee_1999-1
  note: ''
- clef: joulin_les_1901
  note: p. 84, pl. V, n° 24

---
