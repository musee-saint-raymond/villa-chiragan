---
title: Fragment d’un pilastre ou d’un jambage de porte
id_inventaire: Ra 23 d
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: "178"
largeur: "60"
longueur: 
profondeur: "10"
epaisseur: 
id_wikidata: Q48409765
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 330
priority: 3
id_notice: ra-23-d
image_principale: ra-23-d-1
vues: 1
image_imprime: 
redirect_from:
- ra-23-d
- ark:/87276/a_ra_23_d
id_ark: a_ra_23_d
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 259
- clef: joulin_les_1901
  note: fig. 10 b
- clef: massendari_haute-garonne_2006-1
  note: 'p. 238, fig. 96 '
- clef: rachou_catalogue_1912
  note: n° 23 c
- clef: roschach_catalogue_1892
  note: n° 23 d

---
Trois enroulements d’un grand rinceau sont ici peuplés d’un lézard et de deux oiseaux.