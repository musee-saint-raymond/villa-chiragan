---
title: Tête d’Hercule barbu
id_inventaire: Ra 28 k
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: '29,5'
largeur: '23,5'
longueur: 
profondeur: "22"
epaisseur: 
id_wikidata: Q26720748
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 170
priority: 3
id_notice: ra-28-k
image_principale: ra-28-k-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-k
- ark:/87276/a_ra_28_k
id_ark: a_ra_28_k
precisions_biblio:
- clef: balty_les_2008
  note: p. 135 fig. 115
- clef: du_mege_description_1835
  note: n° 177
- clef: du_mege_notice_1828
  note: n° 85
- clef: esperandieu_recueil_1908
  note: n° 899, p. 37, fig. 4
- clef: joulin_les_1901
  note: n° 112 b
- clef: rachou_catalogue_1912
  note: n° 28 k
- clef: roschach_catalogue_1865
  note: n° 28 i

---
On ne peut déterminer avec certitude à quel relief des Travaux d’Hercule cette tête doit être associée. La barbe fournie donne au moins un indice chronologique : [_Hercule et la reine des Amazones](/ra-28-h) ou bien [_Hercule et les pommes d'or du jardin des Hespérides_](/ra-28-f) ou encore [_Hercule et Cerbère_](/ra-28-e). Comme cela fut précisé dans la notice du relief d'[_Hercule et l'Hydre de Lerne_](/ra-28-b), l'absence ou la présence de la barbe induit une progression chronologique et le poids du temps qui s'écoule, depuis les années de jeune adulte à celles de la maturité. Cette pilosité faciale représente par conséquent l'affirmation de l'évolution psychologique du héros.

Le bourrelet sus-orbitaire très prononcé, les mèches effilées et divisées au moyen d'un sillon conçu au ciseau droit, le creusement profond au trépan qui permet de séparer distinctement les boucles généreuses de la barbe sont autant de signes qui renvoient à bien d'autres têtes du grand décor de la _villa_ durant l'Antiquité tardive. La physionomie d'Hercule est, en l'occurrence, particulièrement proche de celle des divinités masculines sur bouclier, en premier lieu [_Esculape_](/ra-34-m).

P. Capus