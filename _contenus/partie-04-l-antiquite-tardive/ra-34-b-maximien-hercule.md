---
title: Tête quasi colossale de Maximien Hercule
id_inventaire: Ra 34 b
donnees_biographiques1: Vers 240/250-310, empereur de 286 à 305 et de 306 à 310
donnees_biographiques2: 
type_oeuvre: 
date_creation: Après 293
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "43"
largeur: "26"
longueur: 
profondeur: '30,5'
epaisseur: 
id_wikidata: Q25113430
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 10
priority: 3
id_notice: ra-34-b
image_principale: ra-34-b-1
vues: 8
image_imprime: 
redirect_from:
- ra-34-b
- ark:/87276/a_ra_34_b
id_ark: a_ra_34_b
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 230, fig. 125 a
- clef: balty_les_2008
  note: ill. de couv., p. 13, 15, 24-25, 35, 37 fig. 5, 41 fig. 8, 45 fig. 14, 48, 50-51, 53 fig. 23, 127 fig 104, 128 fig. 106, 129 fig. 108
- clef: beckmann_idiom_2020
  note: p. 146-148, fig. 12
- clef: bergmann_chiragan_1999
  note: p. 26-44, pl. 9-3, 11-5, 12, 13-4
- clef: bergmann_ensemble_1995
  note: p. 198, fig. 2-3, p. 200
- clef: cazes_musee_1999-1
  note: p. 143
- clef: du_mege_description_1835
  note: p. 129-130, n° 230
- clef: ensoli_aurea_2000
  note: p. 459-460, n° 59
- clef: eppinger_hercules_2015
  note: p. 158
- clef: esperandieu_recueil_1908
  note: p. 32, n° 892.8
- clef: joulin_les_1901
  note: p. 304, pl. VI, n° 60
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig. 112
- clef: musee_saint-raymond_essentiel_2011
  note: p. 30-31
- clef: musee_saint-raymond_regard_1995
  note: p. 235, n° 171
- clef: rachou_catalogue_1912
  note: p. 32-33, n° 34 b
- clef: roschach_catalogue_1865
  note: p. 30, n° 50
- clef: rosso_image_2006
  note: p. 486-488, n° 236
- clef: turcan_constantin_2006
  note: p. 32, fig. 9

---
À la fin de l'année 284, Dioclétien devient empereur (Auguste). Confronté aux menaces de peuples étrangers sur le _limes_ (frontière) ainsi qu'aux velléités d'usurpation du pouvoir de certains généraux, le souverain s’adjoint un co-empereur, Maximien, auquel est octroyé le titre de césar. Les deux gouvernants sont originaires d'Europe centrale : Maximien naquit en Pannonie, entre cinq et huit ans avant Dioclétien, originaire de Dalmatie.

En 286, Dioclétien élève Maximien au rang d'Auguste, titre qui en fait un empereur à part entière. Cette dyarchie, pouvoir à deux têtes, qui pouvait rappeler celle de Marc Aurèle et de Lucius Verus durant le siècle précédent, était cependant appelée à évoluer. Ainsi, le 1<sup>er</sup> mars 293, fait nouveau dans l'histoire institutionnelle impériale, fut institué un mode de gouvernement bien plus innovant encore : la Tétrarchie. Aux deux augustes, Dioclétien et Maximien, sont en effet associés deux césars. Maximien, auguste d'Occident, est alors épaulé par Constance Chlore tandis que Dioclétien, auguste d'Orient, est assisté de Galère. Maximien choisit pour protecteur Hercule, héros devenu immortel, et administre l'Italie, la Rhétie (province comprise entre le Danube et la Vénétie), l'Afrique et l'Espagne. Son césar, Constance Chlore, père du futur Constantin le Grand, se consacre, de son côté, aux provinces de Gaule et de Bretagne (actuels Pays de Galles et Angleterre).

Comme nous l'avons développé dans l'introduction de cette partie, l'ensemble du décor exécuté dans du marbre de Saint-Béat ainsi que ces quatre portraits dépendent certainement de l'époque de cette première Tétrarchie et témoigneraient d'une restauration fastueuse de la _villa_ de Chiragan sous Maximien. La statue à laquelle appartenait cette tête devait atteindre deux mètres soixante-quinze de hauteur ; elle était donc plus grande que la seconde représentation, très fragmentaire, de cet empereur, qui correspond à un relief particulièrement impressionnant dont les lacunes sont aujourd'hui restituées graphiquement dans le musée.

L'Empereur, non content de considérer Hercule, devenu immortel, comme son dieu tutélaire personnel, légitimait également son pouvoir au moyen de liens familiaux directs avec lui. Il en fit le patron de sa famille, devenue herculéenne à son tour {% cite eppinger_hercules_2015 -l 158 %}. Il faut enfin noter, sur ce portrait, le dessin des mèches de la frange, des tempes et de la barbe qui rappellent sans équivoque la tête de l'_Hercule combattant Diomède_ du grand cycle sculpté, non loin duquel cette statue de Maximien devait avoir été élevée.

D'après J.-C. Balty, _Les portraits romains , La Tétrarchie, 1.5_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, 2008, p. 33-53.
