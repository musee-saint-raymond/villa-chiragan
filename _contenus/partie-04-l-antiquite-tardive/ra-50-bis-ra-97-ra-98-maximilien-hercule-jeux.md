---
title: Maximien Hercule (?) donnant le signal d'ouverture des jeux
id_inventaire: Ra 50 bis, Ra 97 et Ra 98
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: 30 (tête) - 51 (bras) - 35 (main gauche)
largeur: 22,5 (tête) - 23,5 (bras) - 19 (main gauche)
longueur: 
profondeur: 16,5 (tête) - 17,5 (bras) - 14,5 (main gauche)
epaisseur: 
id_wikidata: Q26708104
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 50
priority: 3
id_notice: ra-50-bis-ra-97-ra-98
image_principale: ra-50-bis-ra-97-ra-98-1
vues: 1
image_imprime: 
redirect_from:
- ra-50-bis-ra-97-ra-98
- ark:/87276/a_ra_50_bis_ra_97_ra_98
id_ark: a_ra_50_bis_ra_97_ra_98
precisions_biblio:
- clef: balty_les_2008
  note: p. 24-25-26, fig. 22-23, p. 54, 56, 58-61, 71
- clef: beckmann_idiom_2020
  note: p. 149-154, fig. 14
- clef: bergmann_chiragan_1999
  note: p. 34, n° 9, pl. 13.1-3
- clef: esperandieu_recueil_1908
  note: n° 895.2
- clef: joulin_les_1901
  note: n° 114 B, p. 92 (308), p. 108, fig. 220 B, pl. XIV, n° 201 B
- clef: rachou_catalogue_1912
  note: n° 50 bis, n°97-98

---
Grâce à deux statues conservées à Rome, dans les musées du Capitole, on peut restituer ici l'attitude du personnage comme ses vêtements : tunique à manches longues qui descend jusqu'aux chevilles, recouverte par une seconde tunique un peu plus courte, sans manches, et une toge pourpre (_toga picta_). Il tient dans sa main droite une étoffe (la _mappa_). Ce tissu blanc était jeté par l'_editor_ (magistrat qui ordonnait les jeux dans l'arène ou le cirque) afin de donner le signal du début des combats ou le départ de la course de chars. Aussitôt, la _tuba_ (longue trompe en bronze) sonnait et les compétitions débutaient. La main gauche tient un sceptre (_scipio_), insigne du pouvoir. Le même individu servit indéniablement de modèle pour un [portrait monumental](/ra-34-b), également en marbre de Saint-Béat, mis au jour dans la _villa_. Il s'agirait de l'empereur Maximien Hercule.

Dans les années 293/296, des invasions de peuples migrateurs en Afrique du Nord et des razzias de pirates francs sur les côtes espagnoles obligent Maximien à se rendre dans ces provinces. La construction du palais de Cercadilla, près de Cordoue, fut rattaché à cette période. Les Pyrénées centrales durent alors servir de passage pour les légions venues de Germanie et de Gaule. C'est à partir de cette époque de la Tétrarchie (gouvernement de l'Empire à quatre têtes), à la fin du III<sup>e</sup> siècle, que la _villa_ de Chiragan connut une période de très grande activité et d'importantes transformations architecturales. L'extraordinaire décor conçu dans ces années-là est sans doute destiné à célébrer les combats de l'empereur, comparables à ceux menés par Hercule, le patron divin de Maximien.

D'après J.-C. Balty,  _Les portraits romains , La Tétrarchie, 1.5_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, 2008, p. 54-74.