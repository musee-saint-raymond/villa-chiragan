---
title: Junon (?)
id_inventaire: Ra 51bis
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: '36,5'
largeur: "33"
longueur: 
profondeur: "24"
epaisseur: 
id_wikidata: Q27926279
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 260
priority: 3
id_notice: ra-51-bis
image_principale: ra-51-bis-1
vues: 8
image_imprime: 
redirect_from:
- ra-51-bis
- ark:/87276/a_ra_51_bis
id_ark: a_ra_51_bis
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 139
- clef: du_mege_notice_1813
  note: p. 55, n° 127
- clef: du_mege_notice_1818
  note: p. 71, n° 21
- clef: du_mege_notice_1828
  note: n° 59
- clef: esperandieu_recueil_1908
  note: p. 40, n° 901
- clef: joulin_les_1901
  note: fig. 72 B
- clef: rachou_catalogue_1912
  note: n° 51 bis
- clef: roschach_catalogue_1892
  note: n° 51

---
Cette tête pourrait représenter Junon (Héra chez les Grecs), l’épouse de Jupiter (Zeus). Protectrice des femmes, elle symbolise le mariage lorsqu’elle porte, comme ici, un voile sur la tête. Son diadème n’est pas lisse mais richement décoré de rinceaux d’acanthe qui devaient renvoyer à la fécondité et au renouveau. Ces enroulements végétaux répondent à ceux qui se déploient sur les grands pilastres qui scandaient le vaste espace dévolu à ce décor de l'Antiquité tardive. Car cette tête décapitée de Junon, devait, sans aucun doute, dépendre de la série des médaillons des dieux, un ensemble probablement associé aux parties les plus hautes des parois, peut-être au-dessus des reliefs des [Travaux d'Hercule](/ra-28-b).

Les trous, particulièrement indignes, qui parsèment le nez et les lèvres, rappellent le travail des restaurateurs du XIX<sup>e</sup> siècle, qui n'ont pas hésité à entailler certaines parties des œuvres afin de mieux y fixer les compléments en plâtre (généralement le nez, les oreilles ou le menton). Ces ajouts furent retirés au cours du siècle dernier, laissant apparaître creusements et orifices des goujons, au détriment de l'esthétique de la sculpture.

P. Capus