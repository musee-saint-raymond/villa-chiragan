---
title: About
layout: a-propos
def: a_propos
type: statique

---

As the repository of all the marble sculptures discovered on the site of the Roman *Villa* Chiragan, the onus is now on the Musée Saint-Raymond to answer the questions of an ever-increasing number of people bewildered by the austere faces of emperors and depictions of gods that they are often ill-acquainted with. To do so, two concurrent approaches have been implemented by this Toulouse-based institution since the end of the last century, when the building was completely reorganised and its exhibition areas were modernised. On the one hand, in 1999, Daniel Cazes, chief curator and director of the museum at that time, decided to devote a large part of the building to the *villa*'s sculptures, which were destined to be seen by a large audience. On the other, Jean-Charles Balty embarked on his scientific work in 1995, later joined by Emmanuelle Rosso in 2011. The exhaustive nature of their studies enhanced by an abundance of images has shed valuable light on the series of portraits unearthed in Chiragan, all of which have since proved to be a substantial tool for researchers, students and enthusiasts alike.

The need to provide a summary of these different sources has become vital. It is based, for the most part, on the aforementioned studies, as well as the works of international academics and researchers regularly referenced in this catalogue. Over the past forty years or so, the relevance of their analyses has largely helped to change the way we look at Roman sculpture. This approach has of course been amply complemented by the research work conducted by the museum itself, and the completion of a photography campaign that concerns all the sculptures on display.

In order to make these resources more accessible, and allow them to be used to feed other research work, or to be reused in various contexts and fields, their contents are published under an open license, in accordance with the open data policy implemented by the Toulouse City Council. The publishing chain, inspired by those of Getty Museum publications, consists of independent modules based on the use of collaborative open-source tools used to generate the printed version. This innovative achievement has thus supplanted the traditional form of the museum's collection catalogue.

Christelle Molinié and Pascal Capus
