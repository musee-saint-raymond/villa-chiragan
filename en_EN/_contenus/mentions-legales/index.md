---
title: Legal Notices
layout: page
def: mentions_legales
type: statique

---
The multimedia content of the sculpture catalogue for Chiragan _villa_ is the property of the City Hall of Toulouse.

The [inventory data](https://data.toulouse-metropole.fr/explore/dataset/inventaire-principal-musee-saint-raymond/information/?q=chiragan), the images of Musée Saint-Raymond sculptures, and its texts and plans are published under the [Open Licence Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf), licence in compliance with [Toulouse Métropole's _open data_ policy](https://data.toulouse-metropole.fr/page/modedemploi/).

Images which are reused will be accompanied by the following notice: Title of the sculpture, inventory No., Musée Saint-Raymond, the archaeological museum of Toulouse, photo: Daniel Martin.

All other images are reproduced with the authorisation of the rights holders indicated in the captions and are expressly excluded from the free licences which cover the rest of this publication. Unless otherwise stated, these images cannot be reproduced, copied, transmitted or manipulated without the owners' consent.

The logos of the City Hall of Toulouse and Musée Saint-Raymond are protected by intellectual property law. As such, it is not permitted to reproduce or modify (completely or partially) these graphic items without prior written authorisation.

Catalogue source files: https://gitlab.com/musee-saint-raymond/villa-chiragan