---
title: Pedestal of a lost bust
id_inventaire: Ra 178
donnees_biographiques1:
donnees_biographiques2: 
date_creation: 3<sup>rd</sup> century
materiau: Lychnites marble (Paros)
hauteur: '19,5'
largeur: "23"
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q48322083
bibliographie: oui
layout: notice
type: notice
partie: 1
sous_groupe: 
order: 10
priority: 3
id_notice: ra-178
image_principale: ra-178-1
image_imprime: 
vues: 1
redirect_from:        
- ra-178                
- ark:/87276/a_ra_178
id_ark: a_ra_178
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 75
- clef: ensoli_aurea_2000
  note: p. 172-173
- clef: graillot_villa_1908
  note: p. 12-15
- clef: hirschfeld_cil_1899
  note: n°11007
- clef: joulin_les_1901
  note: p. 293-294, 341, pl. XXIV, n° 307 E
- clef: massendari_haute-garonne_2006-1
  note: p. 237, fig. 95
- clef: musee_saint-raymond_regard_1995
  note: p. 211

---

As with most Roman *villae*, the identity of Villa Chiragan's owners is lost in the mist of time, and of course it is also impossible to reduce such a domain to a single type of owner when its activity spans several centuries. More than the architectural alterations, extensions and reorganisations, it is the impressive number of portraits in particular, which include a noteworthy number of probable senior officials, that has prompted the assumption that the residence was that of a provincial governor. What’s more, and precisely because of the sumptuous marble embellishments that adorned its imposing architecture, it has always been more or less considered to have been an imperial residence.

So who owned this extraordinary accumulation of sculptures? Italian connections are of course very powerfully entrenched in this collection, and the portraits in particular. Most of them, be they imperial or anonymous, can be directly linked to Roman workshops. Now exhibited alongside the imperial portraits, the significant series of nameless figures dates back to the Antonine and Severan dynasties. It most probably pertains to people who belonged to the equestrian order and were important protagonists in Rome’s fiscal and administrative life. Among these unidentified faces, some appear to have been reproduced elsewhere in Western Rome. The simultaneous presence of similar portraits in different parts of the Empire could therefore bear witness to the duties of procuratorials, that is to say single individuals officiating in several provinces {% cite bergmann_chiragan_1999 balty_les_2012 -L none -l p. 30, 42-43 -l 268-269 %}.

With regard to *Villa* Chiragan, we must regrettably be content with the single inscription engraved on this piedouche (pedestal), now deprived of its bust, which was discovered on the western side of the large south-facing courtyard, where a group of rooms once stood bordered by a cryptoporticus. It bears the words GENIO C. ACONI TAURI VET., which translates as: "To the genius of *Aconius Taurus*" {% cite hirschfeld_cil_1899 -L none -l 11007 %}. The proper name *Aconius* (gentile name) is correctly positioned between the first name (*praenomen*) *Gaius*, abbreviated to C., and the "nickname" (*cognomen*), *Taurus* {% cite graillot_villa_1908 -l 14-15 %}. Although this epigraph in no way answers all the questions relating to the owners or administrators of this domain, it is nevertheless a significant clue towards making some interesting assumptions. This inscription, which is incomplete and believed, based on the shape of its letters, to be no earlier than the 2nd century {% cite eck_sugli_2000 bergmann_chiragan_1999 -l 172 -l 43 %}, is therefore dedicated to the *genius* (the tutelary deity) of *Gaius Aconius Taurus*. Two different families of *Aconii* are known to have lived in Italy at the end of the 3rd century. One of them lived in Perugia, where a private portrait, similar to one unearthed in Chiragan, was found {% cite stirling_learned_2005 -l 63 %}. A senator named *Aco Catullinus* is also recorded to have been in Rome at the beginning of the 4th century.

The name of Aconius reappears in Rome at the end of the 4th and beginning of the 5th century; one of the most famous members of the family is *Aconia*, wife of *Vettius Agorius Praetextatus*. Both spouses were notorious for their predilection for paganism, as well as their allegiance to many mystery religions {% cite stirling_learned_2005 -l 62 %}. However, there is insufficient evidence to link the words engraved on a statue unearthed in southern Gaul, and possibly dated to the second century AD, to names that are admittedly identical yet listed much later in Rome and Italy {% cite eck_sugli_2000 -l 172-173 %}. Yet it is worth bearing in mind that during the 17th century, the site containing the remains of Villa Chiragan was referred to locally as *Angonia*, after *Villa Aconiaca*, a possible reference to one of the owners of the estate. Could this portrait, of which only the inscribed base remains, have been that of one of these high-ranking officials, owners or administrators of the domain?

Pascal Capus
