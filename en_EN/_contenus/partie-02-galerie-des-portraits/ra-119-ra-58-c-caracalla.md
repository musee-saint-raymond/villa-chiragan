---
title: Bust of Caracalla as a child wearing a cuirass
id_inventaire: Ra 119 - Ra 58 c
donnees_biographiques1: 188 - 217
donnees_biographiques2: "Son of [Septimius Severus](https://en.wikipedia.org/wiki/Septimius_Severus) and [Julia Domna](https://en.wikipedia.org/wiki/Julia_Domna), brother of Geta. Emperor  de 211 à 217"
type_oeuvre: 'Of the “Arcus Argentariorum” type or “Successor Type II”; “Typus Argentarierbogen” or “1. Thronfolgertypus”'
date_creation: Between 195/196 and 205
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "48"
largeur: "49"
longueur: ''
profondeur: "26"
epaisseur:
id_wikidata: Q27106354
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 240
priority: 3
id_notice: ra-119-ra-58-c
image_principale: ra-119-ra-58-c-1
vues: 8
image_imprime:
redirect_from:
- ra-119-ra-58-c
- ark:/87276/a_ra_119_ra_58_c
id_ark: a_ra_119_ra_58_c
precisions_biblio:
- clef: balty_les_2012
  note: p. 266, fig.199
- clef: balty_les_2021
  note: p. 62-64, 125-138
- clef: balty_style_1983
  note: p. 301-314, pl. p. 307-308
- clef: braemer_les_1952
  note: p. 143-148
- clef: bruneau_sculpture_1991
  note: p. 168
- clef: budde_jugendbildnisse_1951
  note: pl. 10, 12
- clef: cazes_musee_1999-1
  note: p. 130
- clef: du_mege_notice_1828
  note: p.110, n°194
- clef: esperandieu_recueil_1908
  note: n° 996, p.62-63, n°954
- clef: fittschen_bemerkungen_1969
  note: p. 198-236, pl. 220
- clef: fittschen_katalog_1985
  note: p. 99, n° 86
- clef: giuliano_museo_1979
  note: p. 332-333
- clef: joulin_les_1901
  note: n° 280 D, pl. XVI, n°260 B
- clef: lebegue_notice_1892
  note: p.32, n°58 c
- clef: massendari_haute-garonne_2006-1
  note: p. 246, fig. 109
- clef: rachou_catalogue_1912
  note: n° 119, p.42, n° 58 c
- clef: roschach_catalogue_1892
  note: p.32, n°58 c
- clef: rosso_image_2006
  note: p. 474-475, n° 229
- clef: wiggers_caracalla_1971
  note: p. 19, 22, 46, 87, 97, pl. 3b

---

This bust, which was discovered during the excavations in 1826, was only recently reunited with its head, unearthed more than sixty years later, in 1890. Both parts were joined together in 2015, and the difference in colour is the result of them having been buried in different sites. Like all the portraits found during the last excavation campaigns (1890-1891 and 1897), the head and top of the bust, both in an excellent state of conservation, have not been restored. The face and neck have thus retained the final polish that was given to them when they were completed by the workshop.

The extraordinary polish of the face, which contrasts with the mass of curly hair divided by long channels created with a trepanning tool creates an aesthetic play of light and shade. Here the marble skin has the shine of porcelain, altered only by the slight scratch marks used to render the eyebrows.

Lucius Septimius Bassianus, also known as Caracalla, born in Lyon in 188, was the son of Septimius Severus, originally from the Roman province of Africa, and Julia Domna, from Syria. This portrait is very similar to that on the Arcus Argentariorum in Rome, a monument erected on the banks of the Tiber, where the [*vicus Jugarius*](https://en.wikipedia.org/wiki/Vicus_Jugarius) entered the [*Forum Boarium*](https://en.wikipedia.org/wiki/Forum_Boarium). This arch had been erected in honour of the imperial family by the bankers and cattle merchants of this important trading area.

The sacrificial scene involving the imperial couple depicted on the inner panel on the eastern side of this construction is still visible today. Septimius Severus and his wife Julia Domna were originally accompanied by Geta, who had been elevated to the rank of Caesar, but his representation was later chiselled out at the order of his brother, Caracalla, who had him murdered (at the end of AD 211) then tried to erase him from memory (*damnatio memoriae*). Opposite this relief, <a href="/images/comp-ra-119-ra-58-c-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-119-ra-58-c-1.jpg" alt="Representation of Caracalla on the Arcus Argentarorium in Rome, Diletta Menghinello / Wikimedia Commons CC BY"/></span>a second panel</a>, on the western pile, originally showed Caracalla with his wife Plautilla, and his father-in-law Plautianus. After being brutally murdered by order of Septimius's son, their portraits were subsequently also removed.

It is possible to date the creation of the "Arcus Argentariorum" type back to 198, when Caracalla was elevated to the rank of Augustus, at the tender age of 8. There are currently some fifty known similar specimens. These numerous representations of the young prince were designed over a very short period of time, probably at the time of the self-proclaimed adoption of their father, Septimius Severus, as son of Marcus Aurelius and brother of Commodus, in 195, and the fictitious accession of his eldest son Bassianus to the gens Aurelia under the name of Marcus Aurelius Antoninus, in 196. In 205, a new image was disseminated to commemorate the joint consulate of the two brothers. The circulation of this first type therefore goes hand in hand with that of the first two "Adoption" and "Serapis" types of Septimius Severus, a characteristic sign of the desire to confirm the advent of a new dynasty. Among the most similar copies of this portrait of Caracalla unearthed in Chiragan, there is the one discovered in Rome in the House of the Vestals, now kept in the <a href="/images/comp-ra-119-ra-58-c-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-119-ra-58-c-2.jpg" alt="Bust of child Caracalla from the Vestales house of the roman forum/Jastrow Wikimedia Commons Public Domain"/></span>Museo Nazionale delle Terme</a>, and those in the National Archaeological Museums of Naples and Mantua.

The muscle cuirass ("Muskelpanzer"), with its undecorated shoulder pieces, includes a wide knotted belt (*cingulum*) at the waist. This element, which can be found on many cuirass-wearing statues as early as the reign of Trajan, is only rarely and much later seen on busts, as until the end of the 2nd century, busts were cut off too high for them to be visible. The cuirass also has a series of short and rounded tongues around the right armhole, the edge of which is highlighted by a fine incision reminiscent of those on the bust of Marcus Aurelius discovered in Chiragan (inv. [Ra 61 b](/en/ra-61-b)). This particular feature is seldom seen on the cuirasses of later Roman busts, and the same applies to statues and reliefs. This additional feature seems to have been restricted to senior army officials, officers and the emperor, as shown by various documents and funerary monuments in the provinces of Noricum and Pannonia. The most recent statues leave us in no doubt when seeking to identity and define the function of this part of the cuirass, which appears to be made of soft leather sometimes reinforced with ornate metal plaques.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 125-138.
