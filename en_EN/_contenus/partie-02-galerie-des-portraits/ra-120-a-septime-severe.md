---
title: Head of Septimius Severus
id_inventaire: Ra 120 a
donnees_biographiques1: 145 - 211
donnees_biographiques2: Emperor from 193 to 211
type_oeuvre: Of the "advent" type
date_creation: Between 193 and 195
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "34"
largeur: "24"
longueur:
profondeur: "24"
epaisseur:
id_wikidata: Q26707669
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 200
priority: 3
id_notice: ra-120-a
image_principale: ra-120-a-1
vues: 8
image_imprime:
redirect_from:
- ra-120-a
- ark:/87276/a_ra_120_a
id_ark: a_ra_120_a
precisions_biblio:
- clef: balty_essai_1966
  note: p. 38, n° 9
- clef: balty_les_1964
  note: p. 60-61, n° 7
- clef: balty_les_2012
  note: p. 66, fig. 62, p. 264, fig.195
- clef: balty_les_2021
  note: p. 61, 85-93
- clef: braemer_les_1952
  note: p. 145
- clef: bulletin_1936
  note: p. 831
- clef: cazes_musee_1999-1
  note: p. 128
- clef: esperandieu_recueil_1907
  note: p. 68, n° 963
- clef: felletti_maj_museo_1953
  note: p. 128
- clef: heintze_studien_1966
  note: p. 198, n° 42, n° 3
- clef: joulin_les_1901
  note: p. 337, pl. XXII, n° 294 D
- clef: mccann_portraits_1968
  note: p. 135-136, n° 14, pl. XXXIII a-b
- clef: musee_saint-raymond_regard_1995
  note: p. 173
- clef: poulsen_portratstudien_1928
  note: p. 27, n° 1
- clef: rachou_catalogue_1912
  note: p. 60, n° 120
- clef: rosso_image_2006
  note: p. 468-470, n° 225
- clef: saastamoinen_afrikan_2009
  note: p. 38
- clef: soechting_portrats_1972
  note: p. 146, n° 23, pl. 3 c-d

---

Septimius Severus, a senator born in Leptis Magna (in Tripolitania, modern-day Libya) in the Roman province of Africa, went on to become the governor of the province of Pannonia Superior, in the heart of Europe. Following the assassination of Emperor Commodus, then of Pertinax, he was made emperor by the legions of the Danube. With the support of his armies, he marched on Rome to establish his legitimacy and eliminate his main rivals.

This outstanding head, designed in an official workshop in Rome, dates from his accession to power in AD 193, a year that brought a number of political twists and turns. On the 28<sup>th</sup> March, Pertinax was assassinated after only eighty-seven days of reign. This event ushered in a period of unrest, similar to that experienced throughout the Empire in 69 following the elimination of Nero. In Rome, on the day of Pertinax's death, Didius Julianus was proclaimed emperor by the praetorians and Senate; but on the 9<sup>th</sup> April, the legions of Pannonia Superior invested Septimius Severus, the governor of the province, with imperial dignity, and two months later he was in Rome, where the Senate had finally acknowledged him, and where Didius Julianus had just been murdered. In the East, the governor of Syria, Pescennius Niger, who had also been elevated to *Augustus* by his troops in April 193, had set off for Rome. Severus went to meet him, defeated him near Cyzicus (on the southern coast of the Sea of Marmara, ancient Propontius) and then returned to Rome where the announcement of a new rebellion awaited him, that of Clodius Albinus, the governor of Britain, whom he had however designated as Caesar in April 193, and with whom he had shared the consulate in 194. Declared a public enemy (*hostis*) in December 195, Albinus was beaten in Lyon in February 197, and killed while attempting to flee.

This portrait discovered in Chiragan corresponds to "Type I" of the Emperor's portraits. Septimius Severus was 38 years old at the time. The hairstyle and beard are short, and there are three curls on his forehead. His gaze is directed upwards, and his head is turned to the right. The head, which is beautifully made, belongs to a group of five portraits, all of which testify, down to the very last detail, to the existence of a common original. They are kept in Toronto (Royal Ontario Museum, inv. 933.27.4), Toulouse (Musée Saint-Raymond, inv. Ra 120 a), Saint Petersburg (Hermitage Museum, A 3184), London, Bonhams Antiquities (sale dated 3<sup>rd</sup> April 2014) and Melbourne (National Gallery of Victoria, inv. 1490/56). The <a href="/images/comp-ra-120-a-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-120-a-1.jpg" alt="Septimius Severus, Courtesy of the Royal Ontario Museum, Toronto, Brian Boyle"/></span>Toronto copy</a>, which originated in Ostia, is the most well-preserved. To complete this group of five portraits, there is also a colossus head from Turkey (Istanbul Archaeological Museums, inv. 46), crowned with oak leaves, as well as a head wearing a crown of laurel leaves in Tyre, now kept at the museum in Beirut (National Museum, inv. DGA 13204). The discrepancies observed in these last two portraits can be explained by the different practices implemented by oriental workshops, but also by the difficulties that must have been encountered in disseminating this first image of the emperor throughout the provinces.

In these portraits, based on Septimius Severus's first iconographic type, the hair is considerably shorter and closer to the skull than in the following two types, which portrays him with long, puffy locks around the temples, a smoother forehead, less pronounced eyebrows, and uncovered ears, all of which completely alters his appearance. This is indeed the soldier emperor who is depicted here, waging a war against those vying for the Empire, whereas the following types were to have a completely different meaning. When comparing them, it is important to note the gaze, which is keener and looking in the opposite directions, making this emperor a man of action. The "advent" type shows him looking to the right, but not yet upwards, as in some specimens of the "adoption" and "Serapis" types, which confirm his close contact with the gods.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 85-93.
