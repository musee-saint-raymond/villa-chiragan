---
title: Head of an adolescent boy (Tiberius Gemellus ?)
id_inventaire: Ra 122
donnees_biographiques1: 19 - 37/38
donnees_biographiques2: Grandson of Tiberius
type_oeuvre: Of the "Capitole-Toulouse" type
date_creation: Year 30 of the 1<sup>st</sup> century
materiau: Lychnites marble (island of Paros)
hauteur: "26"
largeur: '19,5'
longueur: ''
profondeur: "17"
epaisseur:
id_wikidata: Q26707665
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 50
priority: 3
id_notice: ra-122
image_principale: ra-122-1
vues: 8
image_imprime:
redirect_from:
- ra-122
- ark:/87276/a_ra_122
id_ark: a_ra_122
precisions_biblio:
- clef: balty_les_2005
  note: p. 101-118, fig. p. 100, p. 102 (n° 30), 104 (n° 34), 105 (n° 32), 108, 109, 115 (n° 42), 117 (n° 43)
- clef: bergmann_chiragan_1999
  note: p. 28, n° 143
- clef: bonifacio_ritratti_1997
  note: p. 95-96
- clef: boschung_bidlnistypen_1993
  note: p. 48
- clef: boschung_gens_2002
  note: p. 128-129, n° 45.2, pl. 95.2 ("Marcellus ?")
- clef: cazes_musee_1999-1
  note: p. 118, fig. 120
- clef: curtius_ikonographische_1932
  note: p. 229
- clef: curtius_ikonographische_1948
  note: ''
- clef: de_franciscis_il_1951
  note: p. 47-48
- clef: esperandieu_recueil_1908
  note: n° 957
- clef: fittschen_katalog_1985
  note: p. 19-20 à propos du n° 19 (réplique "d" ; "Marcellus")
- clef: giuliano_museo_1987
  note: p. 144
- clef: hannestad_tradition_1994
  note: p. 133, fig. 86-88
- clef: heilmeyer_kaiser_1988
  note: p. 329
- clef: joulin_les_1901
  note: n° 268
- clef: kiss_iconographie_1975
  note: p. 66
- clef: lebegue_notice_1891
  note: p. 415, pl. XXVIII.1
- clef: lebegue_notice_1892
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 242, fig. 102
- clef: musee_saint-raymond_regard_1995
  note: p. 63, fig. 22, p. 87, fig. 45
- clef: pietrangeli_agrippa_1958
  note: p. 159
- clef: pietrangeli_famiglia_1938
  note: p. 44-45
- clef: queyrel_antonia_1992
  note: p. 78-81
- clef: rachou_catalogue_1912
  note: p. 60, n° 122
- clef: rosso_image_2006
  note: p. 440-442, n° 210
- clef: rosso_portrait_1998
  note: ''
- clef: salviat_a_1980
  note: p. 69, fig. 4
- clef: studnicska_agrippa_1917
  note: à propos du n° 1001 p. 10 (Agrippa Postumus)
- clef: trunk_casa_2002
  note: p. 166 à propos du n° 7 (Marcellus)
- clef: west_romische_1933
  note: p. 166-167 (Agrippa Postumus)
---

This young boy's face, still marked by childhood, can only be that of a prince of the imperial Julio-Claudian family, and may therefore date back to the end of the reign of Tiberius or the beginning of that of Caligula (around AD 40). The work is very similar to a head kept in <a href="/images/comp-ra-122-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-122-1.jpg" alt="Marcus Junius Brutus, Capitoline Museums, Carlo Brogi /Wikimedia Commons Public Domain"/></span>Rome (at the Capitoline Museums)</a>, which has been famous since the 18th century, and is the first issue ("Leitstück"), in other words, the replica of an official iconographic type which, because to its quality, must be considered the work that is closest to the initial creation, or prototype. A total of three portraits belong to the same iconographic type, the first in Rome, as related above, the second in Naples (from Pompeii) {% cite bonifacio_ritratti_1997 -L none -l n° 37, p. 94-96, pl. XXXa-d %}, which is the eldest, and finally the head in Toulouse. The latter was obviously created by the same workshop as the one at the Capitoline Museums. The teenager's head was apparently energetically turned to the right, as can be seen from the remaining part of the neck on this side, and the resulting asymmetry. The fringe forms a kind of visor, and refers to a style of hairstyle known since Hellenistic times.

This young man has generally always been seen as a member of the *domus Augusta*; that is the imperial household of Augustus. Marcellus, a nephew of Augustus, is a possible candidate {% cite fittschen_katalog_1985 -L none -l n° 19, p. 19-21 %}, yet Tiberius Gemellus may be a better guess. This young man, born in AD 19, was the grandson of Tiberius and had a twin brother who died in AD 23. He was adopted by his cousin Caligula, thanks to whom he obtained the status of heir presumptive to the throne, and was subsequently assassinated by the same emperor in 37 or 38, allegedly for conspiracy. Tiberius was 17 or 18 at the time. This highly classical work imparts a certain sense of coldness, reinforced by the lifeless fringe, the polished complexion, and the subtle design of the overlapping, sickle-shaped strands of hair. The dominant style of the Tiberian period is obvious, as can be seen in the portraits of the Emperor and his son, Drusus the Younger, father of Tiberius Gemellus.

According to J.-C. Balty 2005, *Les portraits romains, 1 : Époque julio-claudienne, 1.1* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 99-118.
