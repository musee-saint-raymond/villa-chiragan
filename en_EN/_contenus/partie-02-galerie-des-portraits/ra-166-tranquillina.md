---
title: Portrait of Tranquillina (?)
id_inventaire: Ra 166
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 241 - 244
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "50"
largeur: "34"
longueur:
profondeur: '21,5'
epaisseur:
id_wikidata: Q26704019
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 380
priority: 3
id_notice: ra-166
image_principale: ra-166-1
vues: 8
image_imprime:
redirect_from:
- ra-166
- ark:/87276/a_ra_166
id_ark: a_ra_166
precisions_biblio:
- clef: bergmann_studien_1977
  note: p. 39
- clef: braemer_culte_1999
  note: p. 53, fig. 2
- clef: bulletin_1936
  note: p. 837
- clef: cazes_musee_1999-1
  note: p. 141
- clef: esperandieu_recueil_1908
  note: p. 90-91, n° 1001
- clef: felletti_maj_iconografia_1958
  note: p. 179, n° 220
- clef: joulin_les_1901
  note: n° 298 E
- clef: poulsen_catalogue_1951
  note: p. 523, n° 754
- clef: rachou_catalogue_1912
  note: n° 166
- clef: rosso_image_2006
  note: p. 482-484, n° 234
- clef: veyne_empire_2005
  note: fig. 38
- clef: wegner_verzeichnis_1979
  note: p. 56

---

The pleated tunic, fastened on the right shoulder by means of small buttons, is very soft and delicate-looking, and a cloak forms a large semicircle that enhances the portrait. The pitted surface at the back of the neck proves that the lower part of the chignon was added separately.

There are ten such portraits of the same type, kept in Rome, Copenhagen, Dresden, Florence, Liverpool and London, which proves that this is indeed the representation of an imperial figure. This very young woman, with her melancholy expression, has sometimes been identified as Otacilia Severa, wife of Emperor Philip the Arab, but it is more likely that of Tranquillina, wife of Gordian III (238-244). She was married to the emperor in 241, and died just three years later, while still very young. She was not condemned to oblivion (*damnatio memoriae*) after her demise, which probably explains the existence of replicas, numerous for that time, and still extant.

According to E. Rosso 2006, *L’image de l’empereur en Gaule romaine, portraits et inscriptions*, p. 482-484.
