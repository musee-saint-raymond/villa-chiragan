---
title: Portrait of Caracalla as a child
id_inventaire: Ra 168
donnees_biographiques1: 188 - 217
donnees_biographiques2: Son of Septimius Severus and Julia Domna, brother of Geta. Emperor
de 211 à 217:
type_oeuvre: Of the "Arcus Argentariorum" type, or "Successor Type II"
Typus Argentarierbogen » ou « 1. Thronfolgertypus »":
date_creation: 200 - 205
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "25"
largeur: '17,5'
longueur:
profondeur: "19"
epaisseur:
id_wikidata: Q26707731
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 241
priority: 3
id_notice: ra-168
image_principale: ra-168-1
vues: 8
image_imprime:
redirect_from:
- ra-168
- ark:/87276/a_ra_168
id_ark: a_ra_168
precisions_biblio:
- clef: balty_les_2012
  note: p. 267, fig. 200
- clef: braemer_les_1952
  note: p. 143-148
- clef: cazes_musee_1999-1
  note: p. 130
- clef: esperandieu_recueil_1908
  note: n° 999
- clef: fittschen_bemerkungen_1969
  note: p. 197 à 236
- clef: fittschen_katalog_1985
  note: n° 86 p. 99
- clef: ghisellini_museo_1988
  note: p. 336-338
- clef: heintze_studien_1966
  note: p. 199
- clef: joulin_les_1901
  note: n° 304 E
- clef: rachou_catalogue_1912
  note: n° 168
- clef: rosso_image_2006
  note: p. 475-477, n° 230
- clef: wiggers_caracalla_1971
  note: p. 46, 87


---

This head discovered at *Villa* Chiragan may have belonged to a full-length statue. Like the other portrait of the same prince (*inv*. [Ra 119](/en/ra-119-ra-58-c)), this work is similar to the one portrayed on the Arcus Argentariorum in Rome. Yet the Chiragan portrait is more refined, and more typical of an adolescent youth. The hair is flatter and distinguished, in particular, by the tapered strand above the right eye. The characteristics of the hair as well as the shape of the face are comparable to the <a href="/images/comp-ra-168-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-168-1.jpg" alt="Bust of child Caracalla discovered in Markouna, Algeria, inv. Ma 1173 (MNB 782), the Louvre, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>portrait discovered in Markuna</a>, Algeria, now in the Louvre. Thus, while its type is still the same as the one on the Arcus Argentariorum, to which the other portrait of Caracalla as a child discovered in Chiragan (*inv*. [Ra 119](/en/ra-119-ra-58-c)) is also associated, the date in which it was designed should, in this case, be lowered somewhat, preferably to the years 200-205.

A third incomplete portrait of Septimius Severus’s son at a later age is now kept in the reserves (*inv.* [2000.32.1](/2000-32-1)). Finding several representations of Caracalla on the same site is quite exceptional.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 140-144.
