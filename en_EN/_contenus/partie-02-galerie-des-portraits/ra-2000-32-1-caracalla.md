---
title: Incomplete portrait of Caracalla
id_inventaire: 2000.32.1
donnees_biographiques1: 188 - 217
donnees_biographiques2: Son of Septimius Severus and Julia Domna, brother of Geta. Emperor de 211 à 217
type_oeuvre: "Of the “Alleinherrscher” type"
date_creation: 211 - 217
materiau: Marble
hauteur: "16"
largeur: "19"
longueur:
profondeur: "22"
epaisseur:
id_wikidata: Q63787582
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 242
priority: 3
id_notice: 2000-32-1
image_principale: 2000-32-1-1
vues: 3
image_imprime:
redirect_from:
- 2000-32-1
- ark:/87276/a_2000_32_1
id_ark: a_2000_32_1
precisions_biblio:
- clef: balty_les_2021
  note: p. 64-65, 146-150
- clef: wood_roman_1986
  note: ''
- clef: sande_greek_1991
  note: ''
- clef: rosso_image_2006
  note: p. 479-480, n°232
- clef: leitmer_getas_2007
  note: ''
- clef: joulin_les_1901
  note: pl. XXV, n° 319 D
- clef: balty_universalite_1986
  note: ''
- clef: fittschen_sul_1985
  note: ''

---

Although now incomplete, the quality of this portrait is obvious in the smallest remaining details: the beard, locks of hair, shape of the lips, as well as the subtle rendering of the area around the inner corner of the eye. Everything points to the work of one of the finest official workshops in Rome. This is hardly surprising as the portraits of this emperor that belong to the "Alleinherrscher", the "prototype", or "Leitstücke" type series (those closest to the prototype or "Urbild" originally created by the imperial workshop) are now considered to be among the best portraits to have been made at the time. With this model, the artist summoned to the Court to create the "Urbild", produced one of the most impressive types ever made during antiquity, so much so that it has been referred to as the "Master Caracalla" portrait {% cite wood_roman_1986 -l 49 %}. In fact, the famous <a href="/images/comp-2000-32-1-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-2000-32-1-1.jpg" alt="Brutus by Michel-Ange, Bargello Florence Museum, Miguel Hermoso Cuesta / Wikimedia Commons CC BY-SA"/></span>bust of Brutus by Michelangelo</a> was inspired by this type, the sudden movement of the head being associated with a look of fury to demonstrate the character's ardour {% cite fittschen_sul_1985 balty_universalite_1986 -L none -l p. 410-411, fig. 385-390 -L none -l p. 324, pl. XI, 3-4 %}.

The workmanship of this portrait unearthed in Chiragan is similar to that of the famous portrait displayed at the Naples National Archaeological Museum: similar rendering of the mouth - with an even shorter but just as fleshy lower lip, in Toulouse - and tufts of beard that show "the same use of an extremely fine trepanning tool, that has been used to breathe life and vitality into the curls, the details of which are simply carved." {% cite rosso_image_2006 -l 480%} The subtle restitution of the curves of the face, and the volume given to the short curls of the beard punctuated with drill holes are also found on a lovely head in Oslo (former Fett collection), that belongs to the type that immediately preceded it (the "Thronfolgertypus II", or "Successor Type II") {% cite sande_greek_1991 -L none -l n° 63 p. 77-78, pl. LXII %}. The stylistic connection between this specimen in Oslo and the one found in Chiragan is particularly clear: Septimius Severus had artificially attached himself to the previous Antonine dynasty, which immediately resulted in his two children, Caracalla and Geta, being depicted as actual little Antonine princes in their first portraits. Yet all the arguments that subsequently attempted to justify this filiation were gradually eroded. Thus, based on copies of this "Successor Type II", during the last years of the emperor's reign, the portraits essentially aim to highlight the *Concordia Augustorum*; that is the (apparent) concord between the Augustans (confirmed on the reverse side of coins by the legend CONCORDIA AVGVSTORVM or CONCORDIAE AVGG) {% cite leitmeir_getas_2007 -L none -l p. 15-16 et n° 57 %}. A profound change took place, far from the image of emperors during the previous century, from Hadrian to Septimius Severus. The revolutionary creation of the "Master Caracalla" portrait gave concrete expression to this change in the sovereign's image. Caracalla was indeed the first of the "military emperors", and this type of portrait, which so profoundly broke away with the usual physiognomy of 2nd century emperors, was to have a lasting influence on the iconography of several of his successors.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 146-150.
