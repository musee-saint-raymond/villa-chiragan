---
title: Bust of Augustus crowned with oak leaves
id_inventaire: Ra 57
donnees_biographiques1: 63 BC – AD 14.
donnees_biographiques2: Emperor from 27 BC to AD 14
type_oeuvre: Known as "of the Prima Porta type"
date_creation: First half of the 1<sup>st</sup> century
materiau: Lychnites marble (island of Paros)
hauteur: "51"
largeur: "34"
longueur: ''
profondeur: "25"
epaisseur:
id_wikidata: Q24580187
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 10
priority: 3
id_notice: ra-57
image_principale: ra-57-1
vues: 8
image_imprime:
redirect_from:
- ra-57
- ark:/87276/a_ra_57
id_ark: a_ra_57

precisions_biblio:
- clef: salviat_a_1980
  note: p. 22-25
- clef: rosso_image_2006
  note: 'p. 448-450, n° 213 '
- clef: rachou_catalogue_1912
  note: p. 41, n° 57
- clef: musee_saint-raymond_essentiel_2011
  note: p. 40-41
- clef: musee_saint-raymond_regard_1995
  note: p. 39, n° 1
- clef: musée_saint-raymond_cirque_1990-1
  note: p. 52, n° 9
- clef: musée_archeologique_henri-prades_cirque_1990
  note: p. 364, n° 96
- clef: montini_il_1938
  note: p. 51-52, p. 93, n° 118
- clef: massendari_haute-garonne_2006-1
  note: 'ill. de couv.,  p. 242, fig. 100'
- clef: joulin_les_1901
  note: p. 115, pl. XVIII, n° 255 b, 256 b
- clef: johansen_portrait_1976
  note: p. 57, n° 23
- clef: hertel_untersuchungen_1982
  note: p. 66, n° 127, p. 271
- clef: hausmann_zur_1981
  note: p. 583-584, n° 261
- clef: gonse_les_1904
  note: p. 330 et fig.
- clef: frigerio_augusto_1938
  note: p. 49, fig. 25
- clef: esperandieu_recueil_1908
  note: p. 60, n° 948
- clef: curtius_ikonographische_1935
  note: p. 281
- clef: cazes_musee_1999-1
  note: p. 118 et fig.
- clef: braemer_culte_1999
  note: p. 54, fig. 3
- clef: bramer_les_1952
  note: p. 143-148
- clef: boschung_gens_2002
  note: n° 45.1, p. 128, 131, pl. 95.1
- clef: boschung_bildnisse_1993
  note: p. 44, 86, 102, n° 61-62, 138, 142, 158, 163, 168, 494, cat. n° 199 p. 190, pl. 166.1-4, 223.2
- clef: bernoulli_romische_1882
  note: 'II : Die Bildnisse der römischen Kaiser. 3 : Von Pertinax bis Theodosius, p. 39, n° 61'
- clef: bergmann_chiragan_1999
  note: p. 28, n° 142
- clef: benoit_sanctuaire_1952
  note: p. 38
- clef: balty_les_2005
  note: p. 45-47, fig. 16-17, p. 75-98, fig. p. 74, 76, 78, 81 n° 7, 83 n° 10, 86 n° 16, 89 n° 21, 90, 92-93, 96 n° 26
- clef: du_mege_notice_1828
  note: p. 60-61, nº120
- clef: du_mege_notice_1830
  note: p. 396
- clef: du_mege_description_1835
  note: p. 98-99, nº 188
- clef: roschach_catalogue_1865
  note: p. 32, nº 57

---

Octavian, grandnephew of Julius Caesar, is the saviour of the Republic and founder of the Julio-Claudian dynasty. He was elevated to Augustus (an epithet that refers to the religious sphere, and raises the beneficiary above other men) by the Senate on the 16<sup>th</sup> January 27 BC. *Augustus* soon became his first name, and that of all his successors.

The manner in which his forehead locks are depicted is identical to that of the head, associated with a barefooted body wearing a cuirass, of the <a href="/images/comp-ra-57-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-57-1.jpg" alt="Augustus of Prima Porta, Vatican Museum, Michal Osmenda / Wikimedia Commons CC BY"/></span>statue of Augustus discovered in Prima Porta</a> on the outskirts of Rome, in the *villa* that belonged to his wife Livia. This statue, now kept in the Vatican Museums, shows a face which, far from the physiognomy of the emperor, was inspired by that of the Doryphoros ("spear bearer"), a bronze sculpture known from some Roman marble replicas sculpted around 440 BC by Polykleitos, one of the great masters of Classical Antiquity. Art in Greece in the 5th century BC was indeed a constant reference point in Augustan art. The Greek ideal was seen as a way of giving the eternally young emperor an unchanging image that reflected the stability of the Empire. The iconographic genre of the head of the Prima Porta statue is associated with a statuary type that is symbolically highly significant: bare feet, which undoubtedly referred to a religious context or to heroism, and a decorated breastplate showing the restitution, in 20 BC, of the standards taken by the Parthians from the Roman legions of Crassus during the battle of Carrhae (present-day Turkey), in 53 BC. As we know, only one type of portrait, constantly reproduced and unchanging, could be associated with different types of statues: wearing a cuirass, a toga (bare headed or veiled), a bare torso with a mantle rolled at the waist, sitting or on horseback. The portrait of Augustus, which is therefore identical to that of the Prima Porta statue kept at the Vatican, boasts a hairstyle that departs, from the norm previously adopted to represent the Emperor, especially in the depiction of the hair above the forehead. It shows the same hairstyle that had been strictly adhered to throughout his reign, determined by long strands of hair swept from right to left to form a fork, and "crab claw"-shaped strands of hair. The hairstyle is flatter, with less volume than in more youthful portraits designed during the civil war. The present features are marked by the balance and fulfilment that are inherent to classic Greek art, in keeping with the carefully disciplined hair, both being a metaphor for the *Pax Romana* insured by the avenger of Caesar’s death. This type of portrayal, which was to remain unchanged, was a way of unequivocally identifying the *primus inter pares* (first among equals). From the moment he became known as *Augustus* and seized power within the Republic (without there being any real question of a monarchical type of regime) in 27 BC, this portrait of the *imperator* was created and asserted. This ideal image was then rapidly disseminated throughout the length and breadth of the Empire.

The statue found in Chiragan wears a *corona civica* ("civic crown"), in the form of a chaplet of oak leaves, symbol of Jupiter. This honorary emblem was awarded by the Senate in 27 BC, during the investiture ceremony of the one who had put an end to the civil wars, restored peace and ensured the preservation of the values of the oligarchic Republic. Two other portraits of the same type, and also crowned with oak leaves, kept in the <a href="/images/comp-ra-57-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-57-2.jpg" alt="Portrait of Augustus of the Prima Porta type, the Louvre Museum Ma 1247 (MR 426), Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>Louvre Museum</a> and the <a href="/images/comp-ra-57-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-57-3.jpg" alt="”Augustus Bevilacqua”, Munich Glyptothek, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Munich Glyptothek</a>, have been likened to the head unearthed in Chiragan. In all three works, as in many others, the fact that the skull is flattened at the back is particularly noteworthy; it would seem that this recurring feature found on marble portraits of the emperor represents one of his actual physical attributes, in the same way as the bump on the bridge of the nose, as reported by Suetonius {% cite suetone_auguste_nodate %} in his detailed description of Augustus.

According to J.-C. Balty 2005, *Les portraits romains, 1 : Époque julio-claudienne, 1.1* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 73-98.
