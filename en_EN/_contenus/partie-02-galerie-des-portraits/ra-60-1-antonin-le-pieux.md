---
title: Head of Antoninus Pius
id_inventaire: Ra 60 (1)
donnees_biographiques1: 86 - 161
donnees_biographiques2: Emperor from 138 to 161
type_oeuvre:
date_creation: Approximately 138 - 140
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "42"
largeur: '25,5'
longueur: ''
profondeur: '26,5'
epaisseur:
id_wikidata: Q24436741
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 140
priority: 3
id_notice: ra-60-1
image_principale: ra-60-1-1
vues: 8
image_imprime:
redirect_from:
- ra-60-1
- ark:/87276/a_ra_60_1
id_ark: a_ra_60_1
precisions_biblio:
- clef: balty_les_2012
  note: p. 29-34, fig. 20-24, 27, p. 197-198, 200, 202-205, fig. 123, 124, 127, 130, 132, 134-136
- clef: braemer_les_1952
  note: p. 145
- clef: braemer_restauration_1981
  note: p. 56
- clef: cazes_musee_1999-1
  note: p. 124
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 90-91 n° 16
- clef: du_mege_description_1835
  note: p. 113-5, n° 201
- clef: du_mege_notice_1828
  note: p. 64-65
- clef: esperandieu_recueil_1908
  note: p. 67, n° 962
- clef: fittschen_katalog_1985
  note: p. 180
- clef: joulin_les_1901
  note: n° 276 B p. 118
- clef: landes_stade_1994
  note: p. 193, n° 30
- clef: massendari_haute-garonne_2006-1
  note: p. 244, fig. 106
- clef: musee_darcheologie_mediterraneenne_egypte_1997
  note: n° 22
- clef: musee_saint-raymond_image_2011
  note: p. 10, 41
- clef: musee_saint-raymond_regard_1995
  note: p. 43, n° 5
- clef: rachou_catalogue_1912
  note: p. 42-3, n° 60
- clef: rosso_image_2006
  note: p. 458-459, n° 219
- clef: wegner_herrscherbildnisse_1939
  note: p. 151, 279, 282
- clef: wegner_verzeichnis_1979
  note: p. 121

---

Unlike his predecessor Hadrian, Emperor Antoninus Pius had only one type of portrait, both on coins and for in the round marble carvings, despite having reigned for twenty-three years. This one, unlike all the other portraits of this emperor, seems to be the only one on which the iris and pupil have been omitted from the eyes. It is known, however, that this technique was adopted around the year 130 in lieu of the paint that had hitherto been used to bring these representations to life.

This outstanding portrait, which was designed in Rome, still has three small "bumps" at the back of the head, obviously overlooked by the sculptor in charge of the final polishing process. They are explained by the fact that the duplication of an official portrait relied on faithfully reproducing a prototype, in plaster or clay, which served as a reference for all copies made of noble materials such as marble. A pair of compasses and a plumb line were used to transfer reference points selected on the model to be copied, onto the marble block. The "bumps" or measuring points still present on this work prove that on the best copies that came out of the capital's workshops, the back of the head was copied with as much care as the face. This portrait was probably sent to Chiragan shortly after the advent of Antoninus Pius to complete the series of imperial representation on display on the estate.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 195-205.
