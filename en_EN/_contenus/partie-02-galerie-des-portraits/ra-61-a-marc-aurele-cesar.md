---
title: Bust of Marcus Aurelius Caesar
id_inventaire: Ra 61 a
donnees_biographiques1: 121 - 180
donnees_biographiques2: Emperor from 161 to 180
type_oeuvre: Known as the "Offices - Toulouse" type
date_creation: Between 144 and 147
materiau: Göktepe Marble (Turkey)
hauteur: '78,5'
largeur: '56,5'
longueur:
profondeur: '32,5'
epaisseur:
id_wikidata: Q24475762
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 150
priority: 3
id_notice: ra-61-a
image_principale: ra-61-a-1
vues: 8
image_imprime:
redirect_from:
- ra-61-a
- ark:/87276/a_ra_61_a
id_ark: a_ra_61_a
precisions_biblio:
- clef: balty_les_2012
  note: p. 34-35, fig. 28-30, p. 208, 210, 212, 214-215, fig. 137-141, 143-144, 146-148, p. 262, fig.193
- clef: bergmann_marc_1978
  note: p. 40
- clef: bernoulli_romische_1882
  note: 'II : Die Bildnisse der römischen Kaiser. 2 : Von Galba bis Commodus, p. 176, n° 122'
- clef: braemer_les_1952
  note: p. 145
- clef: cazes_musee_1999-1
  note: p. 125
- clef: clarac_musee_1841
  note: p. 587
- clef: du_mege_description_1835
  note: p. 116, n° 205
- clef: du_mege_description_1844
  note: n° 367
- clef: du_mege_notice_1828
  note: p. 65-66, n° 126
- clef: esperandieu_recueil_1908
  note: p. 66, n° 960
- clef: fittschen_prinzenbildnisse_1999
  note: p. 23, pl.
- clef: flisi_questioni_1989
  note: p. 57, n° 37 d
- clef: joulin_les_1901
  note: p. 118 et pl. XX, n° 282 B
- clef: musee_saint-raymond_image_2011
  note: p. 50
- clef: musee_saint-raymond_regard_1995
  note: p. 164
- clef: musees_departementaux_de_loire-atlantique_tresor_1987
  note: p. 76, n° 98, fig. p. 20, 51
- clef: rachou_catalogue_1912
  note: p. 43, n° 61 a
- clef: roschach_catalogue_1892
  note: p. 33, n° 61 a
- clef: rosso_image_2006
  note: p. 460-461, n° 220
- clef: wegner_herrscherbildnisse_1939
  note: p. 20, 204
- clef: wegner_verzeichnis_1979
  note: p. 176

---

Marcus Annius (?) Catilius Severus, later known as Marcus Aurelius, was born on 26<sup>th</sup> April 121 in Rome. His mother was Hadrian's half-sister, making him the emperor's nephew. The *Historia Augusta* records that he "was brought up under the watchful eye of Hadrian, who \[...\]elevated him to an honourable status at the age of six by bestowing on him an *Equus publicus*, and had him join the Salii \[the leaping priests of Mars\] at the age of eight. \[…\] He took to wearing the *toga virilis* (toga of manhood) at the age of fifteen, and was immediately engaged to the daughter of L. Ceionius Commodus, according to Hadrian's wishes." He was adopted by Antoninus Pius at the same time as Pius was adopted by Hadrian on the 25<sup>th</sup> February 138 at the age of 16. It is therefore hardly surprising that a fairly large number of portraits of the young man exist. Two successive iconographic types were used to depict him, but these soon merged. This one, the second type for Marcus Aurelius, known as the "Offices - Toulouse" type, is dated between 140 and 160/161.

During those twenty to twenty-two years (139/140 - 160/161), which correspond almost exactly to the reign of Antoninus Pius, he wielded his power under the name of Aurelius Caesar. At Antoninus's request, he was forced to break off his engagement to marry the Emperor's own daughter, Annia Galeria Faustina, nine years his junior. In 140, he was associated with the third consulate of Antoninus, and invited to settle in Tiberius's palace (*domus Tiberiana*) on the Palatine Hill. Aurelius Caesar was eighteen years old in 139 and almost forty by the time he acceded to the imperial throne, when Antoninus died on 7<sup>th</sup> March 161. And it wasn’t until then that a new iconographic type began to circulate. These unchanging images were consistent with Antoninus himself, who was represented throughout this period thanks to a single iconographic type.

Although still an adolescent in 139, Aurelius Caesar was already a man of mature years in the decade that preceded his accession. As the father of a large family - Faustina was pregnant with twins; that is their tenth and eleventh children, at that time - he could no longer be portrayed as he had throughout the past ten to fifteen years. As sometimes happened under the Empire, the "adolescent type", was not fundamentally modified, but gradually "aged" by certain iconographic artifices. The beard was shown to be thicker and a small beauty spot was added to the lower lip. This and his moustache grew larger still in the last copies, which closely heralded the type that was to depict him at the time of his accession, in which his features were simply slimmed down and his hairstyle was slightly altered. By carefully examining the coins, it is possible to date these different adaptations with some precision.

The bust in Toulouse belongs to one of these sub-groups (sub-group 'c' in the classification made by Klaus Fittschen) that dates back to the years 144-147, of which there are nine known copies, discovered in Gaul, Africa, Asia Minor and Italy. This relatively high number of portraits is most probably explained by the fact that Aurelius Caesar was chosen for a second consulate in 144, an office he assumed in 145, and married Faustina in the spring of that same year. This marriage definitively sealed Antoninus's choice to dedicate himself to the Empire. The bust found in Chiragan therefore dates back to these decisive years, and it was undoubtedly Aurelius Caesar's new status that resulted in his official image being displayed in the villa, alongside the portrait of the reigning sovereign and the imperial images of previous reigns.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 207-218.
