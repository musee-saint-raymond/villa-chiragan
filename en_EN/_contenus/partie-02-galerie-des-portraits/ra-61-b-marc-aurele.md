---
title: Bust of Marcus Aurelius, older and wearing a cuirass
id_inventaire: Ra 61 b
donnees_biographiques1: 121 - 180
donnees_biographiques2: Emperor from 161 to 180
type_oeuvre: of the "IV (variant A)" type
date_creation: Between 170 and 180
materiau: Dokimium marble (Turkey)
hauteur: '76,5'
largeur: '53,5'
longueur:
profondeur: "29"
epaisseur:
id_wikidata: Q24266956
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 160
priority: 3
id_notice: ra-61-b
image_principale: ra-61-b-1
image_imprime:
redirect_from:
- ra-61-b
- ark:/87276/a_ra_61_b
id_ark: a_ra_61_b
bibliographie: oui
vues: 8
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 232, fig. 24
- clef: balty_les_2012
  note: p. 36-37, fig. 31-33, p. 220, 223-225, 226, 228, 231, fig. 151-156, 159-160, p. 262, fig.194
- clef: bergmann_marc_1978
  note: p. 41
- clef: bernoulli_romische_1882
  note: vol. II-2, p. 170, n° 57
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 102-103
- clef: clarac_musee_1841
  note: p. 587
- clef: du_mege_description_1835
  note: p. 116, n° 206
- clef: du_mege_description_1844
  note: n° 367
- clef: du_mege_notice_1828
  note: p. 66, n° 127
- clef: esperandieu_recueil_1908
  note: p. 66-67, n° 961
- clef: fittschen_katalog_1985
  note: p. 74-75, n° 68
- clef: joulin_les_1901
  note: p. 118, pl. XX, n° 281 B
- clef: massendari_haute-garonne_2006-1
  note: p. 244, fig. 107
- clef: musee_archeologique_du_val-doise_tresors_2003
  note: ''
- clef: musee_darcheologie_mediterraneenne_egypte_1997
  note: n° 23, fig.
- clef: musee_petrarque_triomphe_2004
  note: n° 37, p. 92
- clef: musee_royal_mariemont_au_2018
  note: p. 77, fig. 1, III.A.1, p. 309
- clef: musee_saint-raymond_essentiel_2011
  note: p. 44-45
- clef: musee_saint-raymond_image_2011
  note: p. 8, 66
- clef: musee_saint-raymond_regard_1995
  note: p. 165
- clef: rachou_catalogue_1912
  note: p. 43, n° 61 B
- clef: roschach_catalogue_1892
  note: p. 33, n° 61 B
- clef: rosso_image_2006
  note: p. 461-463, n° 221
- clef: sanchez_montes_civilizacion._2006
  note: p. 214
- clef: wegner_herrscherbildnisse_1939
  note: p. 104, 204
- clef: wegner_verzeichnis_1979
  note: p. 176
---

This second bust of Marcus Aurelius belongs to type IV of the emperor's iconography, generally thought to date from around 170/180, that is to say from the last years of his reign. One of the most likely reasons for the creation of this portrait seems to be the important change brought about by the completely unexpected death of Lucius Verus in January or February 169, after eight years of joint reign with Marcus Aurelius, and the fact that the latter now found himself alone at the head of the Empire. But there is more: the coins minted in 171 contain clear allusions to the celebration of the *decennalia* (ten years of reign) on 7 March 170, and introduces the epithet *Germanicus* into the Emperor's title, which crowned his military campaigns in the provinces of Germania. Could type IV have been created at this point in time?

During these same years, Marcus Aurelius was certainly not in Rome: having left the *Urbs* in September-October 169 for a first expedition, he was not to return until November 176. It must therefore be admitted that the official portrait was created outside Rome, probably during the army's winter quarters at *Carnuntum* in Pannonia (present-day Austria), and subsequently sent to Rome, where the main official workshops copied and distributed it.

When seeking, as did Marianne Bergmann - whose subdivision of this fourth iconographic type into two subgroups was not, however, retained by Klaus Fittschen - to distinguish between variant A portraits, with their softer-looking and broader beard, and variant B, with their more compact beard, it is in the first subgroup that the Toulouse portrait should be placed, as she herself did. Furthermore, the Chiragan bust has two characteristics that distinguish it from almost all other known statues and cuirass-wearing busts: the part of the armour that is supposed to protect the nape of the neck seems to be almost glued to it, which makes it look as if the cuirass was too small for the wearer, and appears to be "strangling" him, but this is probably just a clumsy mistake on the sculptor's part. More curiously no doubt, the upper part of the right arm was attached via a large mortise (a technical detail often encountered in statues, and which can obviously be explained, in this case, by the desire to avoid wasting material by working on the arm separately, as it is not an integral part of the body) and the perimeter of that mortise is encircled by a sort of toothed collar that has not been found on any of the other armoured busts found so far. This collar cannot be the beginning of the leather straps that covered the sleeves of the tunic and the upper arms, as these are usually made to look much more flexible, and are sometimes given a certain appearance of movement (these "teeth" are in fact complete and therefore not broken straps). These elements seem to be attached to the edge of the armour. Were they made of metal, like the armour, or leather (?), like the straps (as their rather stiff aspect, which differs from the rest of the armour, seems to suggest)?

Although very similar in terms of workmanship to a fine copy at the Palazzo Braschi in Rome, which Klaus Fittschen is tempted to attribute to the same workshop, the work in Toulouse differs greatly from it on a technical level, as seen in the stiffness of the bust and the aforementioned details. So much so in fact that, whereas it is possible to imagine that the head was made by the same sculptor, who had apparently mastered the head-making technique to perfection, it is just as easy to admit that the bust was entrusted to a less skilled or less experienced craftsman. It would therefore seem that tasks were shared by several sculptors working for the same workshop, a fact that tends to be corroborated by similar observations regarding other portraits.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2*, Toulouse, p. 220-232.
