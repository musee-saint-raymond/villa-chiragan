---
title: Bust of Geta as a boy
id_inventaire: Ra 62
donnees_biographiques1: 198 - 211
donnees_biographiques2: Son of Septimius Severus and Julia Domna, brother of Caracalla
type_oeuvre: of the "Munich-Toulouse" type
date_creation: Between 202 and 205
materiau: Göktepe marble, district 3 (Turkey) for the head, and Altintaş marble (Turkey)
pour le buste:
hauteur: "58"
largeur: "45"
longueur: ''
profondeur: "27"
epaisseur:
id_wikidata: Q27096193
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 230
priority: 3
id_notice: ra-62
image_principale: ra-62-1
vues: 8
image_imprime:
redirect_from:
- ra-62
- ark:/87276/a_ra_62
id_ark: a_ra_62
precisions_biblio:
- clef: balty_les_2012
  note: p. 40-41, fig. 38-40, p. 266, fig.198
- clef: balty_les_2021
  note: p. 16-20, 66-67, 152-159, fig. 8
- clef: balty_style_1983
  note: p. 306, fig. 10, p. 308
- clef: braemer_les_1952
  note: p. 143-147
- clef: budde_jugendbildnisse_1951
  note: pl. 20
- clef: cazes_musee_1999-1
  note: p. 131
- clef: du_mege_description_1835
  note: n° 208
- clef: du_mege_notice_1828
  note: n° 129
- clef: esperandieu_recueil_1908
  note: n° 1011
- clef: fittschen_bemerkungen_1969
  note: p. 197-236
- clef: heintze_studien_1966
  note: p. 195-6, 199
- clef: joulin_les_1901
  note: n° 291 B
- clef: rachou_catalogue_1912
  note: n° 62
- clef: roschach_catalogue_1865
  note: n° 62
- clef: roschach_catalogue_1892
  note: n° 62
- clef: rosso_image_2006
  note: p. 477-478, n° 231
- clef: wiggers_caracalla_1971
  note: p. 97, 112, pl. 26 c-d

---

This lovely child's head belongs to a series of eleven portraits that date back to a common original ("Urbild") made in Rome in 198, and referred to as of the "Munich -Toulouse" type, as both cities boast a fine and specific specimen. This type was designed when Geta was awarded the title of *Caesar*, at the tender age of ten. This portrait was reproduced by the workshops in charge of distributing imperial images until 204. The slight differences from one work to another are intended to specify the age of the young prince during the few years in which the iconographic type was distributed.

In this case, the head and bust obviously do not belong together. The head appears too large for the size of the bust, and the type of pedestal clearly predates the portrait.

The outstanding portraits of Septimius Severus and his two sons, Caracalla and Geta, discovered in Villa Chiragan, date back to the same period; that is 198-204. But there are considerable differences between the images of the two brothers. Geta's hair is certainly softer-looking, more finely honed, without the vigorously carved out volume that characterises the effigies of Severus and Caracalla. The actual design of the locks of hair is also quite different, and announces that of the *"a* *penna"* technique (small feather-shaped extremities) used on portraits made during the first third of the century.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 151-159.
