---
title: Head of Lucius Verus
id_inventaire: Ra 63
donnees_biographiques1: 130 - 169
donnees_biographiques2: Co-emperor with Marcus Aurelius from 161 to 169
type_oeuvre: of the "IV Samtherrschaftstypus” type, during his corule with Marcus Aurelius)
date_creation: Between 161 and 169
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '35,5'
largeur: '26,5'
longueur:
profondeur: "27"
epaisseur:
id_wikidata: Q24476631
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 170
priority: 3
id_notice: ra-63
image_principale: ra-63-1
vues: 8
image_imprime:
redirect_from:
- ra-63
- ark:/87276/a_ra_63
id_ark: a_ra_63
precisions_biblio:
- clef: balty_les_2012
  note: p. 38, 39 ; fig. 34-37
- clef: bernoulli_romische_1882
  note: p. 210, n° 41
- clef: braemer_les_1952
  note: p. 145
- clef: bulletin_1936
  note: p. 651
- clef: cazes_musee_1999-1
  note: p. 125
- clef: du_mege_description_1835
  note: p. 116-117, n° 207
- clef: du_mege_description_1844
  note: n° 369
- clef: du_mege_notice_1828
  note: p. 66-67, n° 128
- clef: esperandieu_recueil_1908
  note: p. 83, n° 987
- clef: musee_saint-raymond_image_2011
  note: p. 56
- clef: musee_saint-raymond_regard_1995
  note: p. 168
- clef: rachou_catalogue_1912
  note: p. 44, n° 63
- clef: roschach_catalogue_1865
  note: n° 63
- clef: roschach_catalogue_1892
  note: p. 34, n° 63
- clef: rosso_image_2006
  note: p. 463-465, n° 222
- clef: wegner_herrscherbildnisse_1939
  note: p. 247
- clef: wegner_verzeichnis_1979
  note: p. 62

---

The young Lucius Ceionius Commodus was adopted by Antoninus Pius at Hadrian's request on the 25<sup>th</sup> February 138, at the same time as the future Marcus Aurelius. From then onwards he went by the name of Lucius Aelius Aurelius Commodus, but was better known as Lucius Verus. From the age of seven, he became part of the *gens Aurelia* made up of Antoninus's family. Portraits of the young prince as a child and adolescent were therefore safeguarded (three successive types, two of which correspond to those of *Marcus*), as were those of his adoptive brother *Marcus*.

Although he was part of the *Domus Augusta*, according to the *Historia Augusta* (*Life of Verus*, 3, 4), Verus "lived for a long time as a simple citizen, without enjoying any of the marks of honour that were lavished on *Marcus*". But he was appointed consul in 154 and 161. When Antoninus died on the 7<sup>th</sup> March 161, Marcus Aurelius invested Lucius with imperial dignity, conferred all honours on him (annual tribune's power was also bestowed on him, and he became consul again in 167) and shared his power with him.

It is at this time that the numerous portraits were made (about a hundred of them are known today), representing him as a mature, 31-year old man. During the eight years of his joint reign alongside Marcus Aurelius, his image remained unchanged, as did the type III depicting Marcus Aurelius, which also remained unchanged from 161 to the death of Verus (January-February 169), and was similarly disseminated throughout the Empire. The only differences that can be observed from one copy to the next are the result of different workshop practices. One example is the extensive use of the drill bit that characterises the portrait statue found in Chiragan, like most of those that are still extant. Yet this is not reproduced in some copies made by other workshops, where the hair is almost exclusively worked using straight chisels (such as the one discovered in the colony forum, in modern-day Terragona). Thanks to his great drilling skills, the sculptor who created the head in Toulouse has done a masterful and extremely precise job of reproducing the curls of the "Urbild" (the prototype created by the imperial palace workshops), including at the back and at the nape of the neck, where the hair tends to resemble a lush plant. The abundant curls that falls quite low on the forehead were subsequently imitated in private portraits produced during the same years, as dandies strove to reproduce the emperor's hairstyle (the *Historia Augusta*, *Life of Verus*, 10, 7 speaks of the great care he lavished on his blond hair), thus creating a veritable fashion that is still reflected in many portraits in modern-day museums, which are often mistakenly identified as portraits of Verus.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 233-242.
