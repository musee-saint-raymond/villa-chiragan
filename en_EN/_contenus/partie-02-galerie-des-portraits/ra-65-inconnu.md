---
title: Bust of a high-ranking official of the Empire wearing a fringed paludamentum
id_inventaire: Ra 65
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 195 - 205
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '62,5'
largeur: "58"
longueur:
profondeur: "30"
epaisseur:
id_wikidata: Q30160658
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 280
priority: 3
id_notice: ra-65
image_principale: ra-65-1
vues: 8
image_imprime:
redirect_from:
- ra-65
- ark:/87276/a_ra_65
id_ark: a_ra_65
precisions_biblio:
- clef: balty_les_2012
  note: p. 27, fig. 18-19, p. 268, fig.203
- clef: balty_les_2021
  note: p. 48, 191-198
- clef: cazes_musee_1999-1
  note: p. 137
- clef: du_mege_description_1835
  note: n° 200
- clef: du_mege_description_1844
  note: n° 363
- clef: du_mege_notice_1828
  note: p. 63-64, n° 124
- clef: esperandieu_recueil_1908
  note: p. 72, n° 970
- clef: fittschen_zum_1971
  note: p. 237-240, fig. 31-32
- clef: guillevic_toulouse_1989
  note: n° 36
- clef: joulin_les_1901
  note: p. 337, pl. XXIV, n° 308
- clef: musee_saint-raymond_regard_1995
  note: p. 91, n° 49
- clef: nguyen-van_reconstruire_2015
  note: ''
- clef: rachou_catalogue_1912
  note: n° 65 ou 73 D
- clef: roschach_catalogue_1892
  note: n° 65 ou 73 D

---

This is another example of one of the best workshops in the *Urbs*, if only we knew the identity of this man, who is most certainly a high-ranking official of the Empire, and a member of the emperor's entourage, although we have no idea of the position he held on the Chiragan estate.

Various portraits from the very beginning of the Severan dynasty continue to sport a hairstyle that had been popular among young people since the Antonine dynasty: particularly bouffant, and combed from the top of the head in wavy, and apparently disorderly locks that fell quite low on the forehead and the nape of the neck. This style was inspired in particular by images of Antinous, a favourite of Emperor Hadrian (whose influence on portraits of young men from the early Antonine dynasty is well known) and the portraits of Lucius Verus as a child. Up until the beginning of the Severan dynasty, several portraits that were devoid of the shell-shaped curls that prevailed throughout most of the works of the Antonine dynasty, attest to the popularity of these puffy hairstyles, with locks swept across the entire forehead. This high-ranking official discovered in Chiragan is perhaps one of the oldest to still sport this style.

From a technical point of view, it is certainly one of the most beautiful portraits brought to light in Martres-Tolosane. The particularly sensitive modelling of the cheeks and eye surface (with the fine wrinkles chiselled under the lower left eyelid), the astonishing expression that is both piercing and somewhat ironic, and the strength that emanates from the lower part of the face, with its tight jaw, all make for an extraordinary psychological portrait that is indisputably one of the masterpieces of Roman art. It is therefore highly regrettable that 19th century restorers cut so deeply into the nose to attach the prosthesis shown in old photographs, a practice that has irretrievably disfigured this portrait’s profile.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 191-198.
