---
title: Bust of a high-ranking official of the Empire wearing a cuirass and draped in a paludamentum 
id_inventaire: Ra 71
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 220 - 235
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "80"
largeur: "67"
longueur:
profondeur: "38"
epaisseur:
id_wikidata: Q30160698
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 370
priority: 3
id_notice: ra-71
image_principale: ra-71-1
vues: 8
image_imprime:
redirect_from:
- ra-71
- ark:/87276/a_ra_71
id_ark: a_ra_71
precisions_biblio:
- clef: balty_les_2021
  note: p. 39-41, 245-252
- clef: cazes_musee_1999-1
  note: p. 140
- clef: du_mege_description_1835
  note: n° 223
- clef: du_mege_notice_1828
  note: n° 137
- clef: esperandieu_recueil_1908
  note: n° 977
- clef: joulin_les_1901
  note: n° 327
- clef: mesple_raccords_1948
  note: p. 157
- clef: rachou_catalogue_1912
  note: n° 71
- clef: roschach_catalogue_1892
  note: n° 137

---


While the *"a penne"* technique (meaning in the form of small feathers) used to depict strands of hair appeared as early as the year 215 on some statue portraits of Caracalla, it continued, depending on the workshop, until the reign of Decius (249-251). When dealing with anonymous works, it is therefore necessary to opt for a relatively wide timeframe that allows for different workshop practices.

Because the bust kept in Toulouse is a true double of <a href="/images/comp-ra-71-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-71-1.jpg" alt="Decius, inv. MC 482, Capitole Museum, Sailko/Wikimedia Commons CC BY"/></span>Decius</a>, one cannot help but wonder if it is a portrait of the emperor a few years before the proclamation by the troops of Pannonia in 249. Yet we must refrain from hasty identification and simply note, once again, that this is the image of one of the officials from the emperor's immediate entourage, which is hardly surprising, given Chiragan’s context.

The head and bust were joined together in the restoration that immediately followed their discovery, but they do not belong together. This is a modern reassembly carried out by the sculptors in Toulouse to enable these works to be displayed in the "Emperors’ Gallery" exhibition in 1835. The bust dates back to the Antonine dynasty; that is the century that precedes this type of portrait, as can be seen from the already quite distinctly marked top of the arms, the width of the shoulders, and the oval shape that is found on several representations of Antoninus Pius. In addition to this modern reconstruction, the head itself bears the trace of important remodelling work, which could mean it was actually re-sculpted during Antiquity. The pavilion of the left ear seems to have been substantially reworked from a portrait that apparently had larger ears partly covered by curls of hair; at the back of the left ear, as on the right side, the surrounding coarsely pockmarked area differs markedly from the careful work of the *penne* that constitute the rest of the hairstyle. Finally, the thickness of the neck in relation to the narrow head could also be explained by the systematic reshaping of an earlier portrait.

Yet the quality of this portrait is quite outstanding: the subtle modelling of the face, the intensity of the gaze, the care taken in detailing the hair, and the locks of the beard are all proof of the skills of the artisans working for workshops of the *Urbs* (Rome) when it came to producing this type of work.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 245-252.
