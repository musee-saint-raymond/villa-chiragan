---
title: Bust of a high-ranking official of the Empire wearing a fringed paludamentum
id_inventaire: Ra 72
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre: of the "Palazzo Corsini - Toulouse" type
date_creation: Late 2\<sup>nd\</sup> - Early 3\<sup>rd\</sup> century
materiau: Göktepe marble, district 4 (Turkey)
hauteur: "82"
largeur: "58"
longueur: ''
profondeur: "34"
epaisseur:
id_wikidata: Q27096201
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 290
priority: 3
id_notice: ra-72
image_principale: ra-72-1
vues: 8
image_imprime:
redirect_from:
- ra-72
- ark:/87276/a_ra_72
id_ark: a_ra_72
precisions_biblio:
- clef: balty_les_2021
  note: p. 41-44, 177-190
- clef: bergmann_studien_1977
  note: p.33
- clef: du_mege_description_1835
  note: n° 225
- clef: du_mege_notice_1828
  note: n° 140
- clef: esperandieu_recueil_1908
  note: n° 983
- clef: joulin_les_1901
  note: n° 310
- clef: rachou_catalogue_1912
  note: n° 72
- clef: roschach_catalogue_1865
  note: p. 36-37, n°72
- clef: roschach_catalogue_1892
  note: n° 72
- clef: wood_roman_1986
  note: ''

---

M. Bergmann {% cite bergmann_studien_1977 -L none -l n. 104 p. 33 %} noticed that the bust in Toulouse represented the same person as a statue portrait in the Palazzo Corsini in Rome. This is therefore, yet again, a private portrait of which, by chance, at least two replicas have been preserved. And inscriptions do indeed tell us that the greatest figures of the Empire were honoured with numerous statues and busts, both in the town from whence they came, and in those in which they carried out their administrative duties.

Both works in Rome and Toulouse show absolutely identical characteristics: proportions and facial expression, shape of eyebrows arches, eyes, lips and moustache, and design and volume of the beard, the very aquiline nose, nostrils and nasolabial folds. The only differences are the design and modelling of the locks of hair around the temples and at the back of the skull, and the fact that the ears are completely uncovered in the portrait in Toulouse whereas in the one in Rome the tops of the ears are slightly covered by hair. In Toulouse, these strands of hair have been carved with extreme care, each lock having been made independently from the others, and marked with fine lines using a trepanning tool to give them more substance, whereas those in the Corsini portrait seem banal and "mechanical", as if the sculptor lacked a model for this part of his work, and almost randomly copied the shell-shaped curls found on all the portraits of the Antonine and early Severan dynasties. The appalling carnage perpetrated in Toulouse to the top of the skull for the purpose of attaching plaster hair is all the more regrettable. In this particular point, the Corsini replica is therefore a privileged witness with regard to the fringe at the front, which, like everything else that affected the physiognomy of this portrait, was duty bound to be identical to the model provided.

The large, fringed cloak (*paludamentum*) is secured above the right shoulder by a large, round, rosette-shaped fibula and covers the entire torso; only the shoulder and the beginning of the right arm are exposed. This type of garment of late Hellenistic origin has been associated with military functions. According to our current documentation, it would seem that the oldest busts covered with a fringed *paludamentum* date back to the reign of Antoninus Pius. The type of bust that includes a fringed *paludamentum* worn over a tunic to represent emperors during the reigns of Marcus Aurelius and Lucius Verus, and even Septimius Severus, appears to have been reproduced in private portraiture as early as the Antonine dynasty, and seems to have been very popular at the end of this period as well as at the beginning of Severus's reign.

It should come as no surprise that a portrait of a high-ranking official, a copy of which is also preserved in collections of antiquities in Rome and Italy, was also unearthed in Chiragan. If the *villa* in Martres-Tolosane is indeed the imperial domain we believe it is, then, when attempting to identify these portraits, we should be thinking along the lines of the procurators of imperial heritage (*patrimonium principis*) as well as, perhaps, those who managed the quarries and neighbouring forests on the emperor's behalf, and were undoubtedly also posted in Chiragan.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 177-190.
