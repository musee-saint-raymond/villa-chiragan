---
title: Bust of a young man
id_inventaire: Ra 73 a
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately AD 120
materiau: Göktepe marble, district 4 (Turkey)
hauteur: "63"
largeur: "43"
longueur:
profondeur: "26"
epaisseur:
id_wikidata: Q24477793
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 80
priority: 3
id_notice: ra-73-a
image_principale: ra-73-a-1
vues: 8
image_imprime:
redirect_from:
- ra-73-a
- ark:/87276/a_ra_73_a
id_ark: a_ra_73_a
precisions_biblio:
- clef: balty_les_2005
  note: p. 44, fig. 15
- clef: balty_les_2012
  note: p.13-14, fig. 1-3, p.121-127,129-130, fig.46-58
- clef: cazes_musee_1999-1
  note: p. 123
- clef: daltrop_stadtromischen_1958
  note: p. 129
- clef: esperandieu_recueil_1908
  note: p. 89, n° 998
- clef: joulin_les_1901
  note: p. 332, pl. XVII, n° 258
- clef: massendari_haute-garonne_2006-1
  note: p. 243, fig 105
- clef: musee_saint-raymond_image_2011
  note: p. 78-79
- clef: musee_saint-raymond_regard_1995
  note: n° 157

---

This bust is one of the finest portraits of a stranger in the collection owned by the Musée Saint-Raymond. The type of hairstyle is inspired by those of representations of Trajan; but the hair has more volume, and the locks are wavier and more lifelike, and herald the portraits of Hadrian.

A few elements allow us to date it with greater accuracy. These include the absence of irises and pupils on the eyes, details that were originally added using paint, a practice now no longer implemented. It was not until around 130 that these details were carved onto the eyes of statues, yet here the eyes are smooth. This work cannot therefore have been made any later. An additional consideration is the shape of the bust itself: any representation of the upper abdomen is absent, as on a bust of Hadrian at the <a href="/images/comp-ra-73-a-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-73-a-1.jpg" alt="Bust of Hadrien, British Museum, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>British Museum</a> which has been dated to between 118 and 121, or even 119 to be more precise (the third consulate of the emperor). Therefore, a date close to 120 seems to be the most feasible.

It is impossible to identify this outstanding portrait, although one unfortunately very damaged head from the Quirinal Palace in Rome could represent the same person. This means that this young boy was important. Of course, this type of portrait, with its heroic pose (the bared torso) and the boy's young age cannot be equated with that of a high-ranking imperial official, as was the case at the end of the 2nd and 3rd centuries for several "private" portraits found in Chiragan. Yet the very existence of a replica in Rome proves the close relationship between the *villa* and the "seat of power".

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 119-130.
