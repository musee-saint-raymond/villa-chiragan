---
title: Bust of a young man
id_inventaire: Ra 73 b
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Between 120 and 130
materiau: Göktepe marble, district 4 (Turkey)
hauteur: "66"
largeur: "45"
longueur: ''
profondeur: "28"
epaisseur:
id_wikidata: Q24478254
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 85
priority: 3
id_notice: ra-73-b
image_principale: ra-73-b-1
vues: 8
image_imprime:
redirect_from:
- ra-73-b
- ark:/87276/a_ra_73_b
id_ark: a_ra_73_b
precisions_biblio:
- clef: balty_les_2005
  note: p. 44, fig. 15
- clef: balty_les_2012
  note: p. 15, fig.4-5, p.133-136, 139, 141, fig. 59-63, 69-71
- clef: cazes_musee_1999-1
  note: p. 123
- clef: du_mege_description_1835
  note: n° 189 ou 190
- clef: du_mege_notice_1828
  note: n° 118
- clef: joulin_les_1901
  note: n° 265
- clef: massendari_haute-garonne_2006-1
  note: p. 243, fig. 104
- clef: musee_saint-raymond_image_2011
  note: p. 80
- clef: rachou_catalogue_1912
  note: n° 73 b
- clef: roschach_catalogue_1892
  note: n° 73 a-b

---

The face and hairstyle of this young man appear to emulate those of the first portraits of Hadrian. Furthermore, in terms of chronologically the entire bust coincides with the beginning of Hadrian's reign. The piedouche (plinth) is now decorated with volutes, and a new fashion has emerged in the form of bearded faces. Like Emperor Hadrian, as depicted in his first iconographic type, this young man wears a chinstrap beard. This face can therefore be compared to one of the portraits on the Arch of Trajan in Benevento in the *comune* of Campania, on the attic on the upper parts of the monument, which is generally believed to be that of Hadrian (the reliefs are thought to have been completed after Trajan's death, at the very beginning of his successor's reign).

The naked bust resembles a mythological hero, just like several other contemporary works depicting young people. The hair is shown to have more volume than that of the male bust inv. Ra 73a, without, however, being as thick as those of the various iconographic types adopted for Trajan's successor, and is devoid of the characteristic back-to-front wave-like movement and the "baroque" scrolls. This is therefore a pivotal moment at the beginning of Hadrian's reign: a new form of bust and pedestal, a new way of breathing life into, and adding sparkle to, the gaze of portrayed figures, and a new fashion in facial hair.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 131-143.
