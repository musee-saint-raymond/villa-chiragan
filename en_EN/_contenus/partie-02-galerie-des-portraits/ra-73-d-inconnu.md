\---
title: Portrait of an unknown man
id_inventaire: Ra 73
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 215 - 220
materiau: Dokimium marble (Turkey)
hauteur: "34"
largeur: "22"
longueur:
profondeur: "24"
epaisseur:
id_wikidata: Q28797775
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 331
priority: 3
id_notice: ra-73-d
image_principale: ra-73-d-1
vues: 8
image_imprime:
redirect_from:
- ra-73-d
- ark:/87276/a_ra_73_d
id_ark: a_ra_73_d
precisions_biblio:
- clef: balty_les_2021
  note: p. 27-33, 233-237, fig. 19, 22
- clef: cazes_musee_1999-1
  note: p. 139
- clef: esperandieu_recueil_1908
  note: n° 1021
- clef: rachou_catalogue_1912
  note: n° 73 d

---

This very lovely portrait of a relatively young man in his prime is part of a group that includes three other heads (inv. Ra 73 g, Ra 73 f and Ra 160) whose features and character are very different to the heads and busts of much older people. It is however impossible, unfortunately, to ascertain who they were, and why their portraits were displayed in the halls of *Villa* Chiragan. They belonged to the same generation as the sons of Septimius Severus, and remained in office during the very last years of his reign as well as during the reign of Caracalla. It is quite likely that they depicted those chosen to renew the body of executives in charge of imperial administration, which was one of the essential characteristics of that time.

The shape of the part that served to insert the head, at the beginning of the left shoulder, seems to indicate that this head belonged to a statue depicted wearing a cuirass; unless it was a bust whose torso was made of a different type of marble. In this case, it is reminiscent of another work found in Chiragan: the magnificent bust in Pavonazzetto marble, mistakenly associated with a head made from a different kind of the same stone (inv. [Ra 121](/en/ra-121)).

The locks of hair, which are relatively short and not very thick, have a slight wave to them, and end in two or three end points. In this respect, they herald the *"a penna"* hairstyle that is inherent to the portraits of Severus Alexander, in which the hair is combed forward from the back and top of the head, and ends in a fairly straight fringe that leaves most of the forehead bare.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 233-237.
