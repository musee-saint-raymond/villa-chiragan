---
title: Bust of a man wearing a cuirass
id_inventaire: Ra 73 e
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately AD 130
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "78"
largeur: '54,5'
longueur:
profondeur: "29"
epaisseur:
id_wikidata: Q24520896
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 90
priority: 3
id_notice: ra-73-e
image_principale: ra-73-e-1
vues: 8
image_imprime:
redirect_from:
- ra-73-e
- ark:/87276/a_ra_73_e
id_ark: a_ra_73_e
precisions_biblio:
- clef: balty_les_2012
  note: p. 43-44, fig. 42-43, p.165-168, 170, fig. 90-91, 93, 95, 97-100, p. 268, fig. 202
- clef: cazes_musee_1999-1
  note: p. 136
- clef: du_mege_description_1835
  note: p. 119, n° 211
- clef: du_mege_description_1844
  note: n° 373
- clef: du_mege_notice_1828
  note: p. 68, n° 131
- clef: esperandieu_recueil_1908
  note: p. 76-77, n° 978
- clef: joulin_les_1901
  note: pl. XXIII, n° 302 b
- clef: musee_saint-raymond_regard_1995
  note: p. 158
- clef: rachou_catalogue_1912
  note: p. 48-49, n° 73 e

---

Up until the reign of Hadrian, only emperors were depicted wearing a cuirass, which is why examples are relatively rare. This portrait is very similar to several busts of the emperor, especially with regard to certain details, such as the Gorgon head (*Gorgoneion*), with wings strangely raised, on the breastplate of the cuirass.

The very expressive gaze, under prominent, frowning eyebrows, the hawk nose and the disdainful mouth are a reminder that the realistic current in Roman portraiture was still thriving. This realistic vein is undoubtedly particularly characteristic of the portraits of soldiers and, to a lesser extent, of philosophers and rhetoricians, dependent on an iconography that emerged in Greece towards the end of the classical period, and even certain "middle class" people, and freed slaves in particular.

As members of the equestrian order, these armoured strangers are not (or are no longer) soldiers. They should probably be seen as men who ran for and obtained the office of procurator. In the provinces, these senior officials assumed administrative and fiscal responsibilities. Authorised since the reign of Augustus to wear military garb, they even displayed their equestrian status in their marble representations.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 163-170.
