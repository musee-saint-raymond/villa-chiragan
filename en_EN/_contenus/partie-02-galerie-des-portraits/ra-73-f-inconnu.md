---
title: Head of an unknown man
id_inventaire: Ra 73 f
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 195 - 205
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "29"
largeur: '23,1'
longueur:
profondeur: '23,7'
epaisseur:
id_wikidata: Q48323047
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 310
priority: 3
id_notice: ra-73-f
image_principale: ra-73-f-1
vues: 8
image_imprime:
redirect_from:
- ra-73-f
- ark:/87276/a_ra_73_f
id_ark: a_ra_73_f
precisions_biblio:
- clef: balty_les_2021
  note: p. 48-54, 199-204
- clef: rachou_catalogue_1912
  note: n° 73 f
- clef: roschach_catalogue_1892
  note: n° 73 f
- clef: joulin_les_1901
  note: n° 275
- clef: esperandieu_recueil_1908
  note: n° 951

---

Although this portrait unearthed at *Villa* Chiragan was undeniably produced by a workshop in the *Urbs* (Rome), it is not easily dated. It could be from the late Antonine dynasty, or from the early days of the reign of Septimius Severus, and it is tempting to blame the latter rather than the reign of Marcus Aurelius or Commodus for the exaggerated use of the trepanning tool that has ruined the curls around the temples, and for the beard, which is shorter than during the late Antonine dynasty. This portrait is therefore to be compared with representations of Septimius Severus rather than his predecessors.

This is a middle-aged man, whose features were rendered with less realism than the bust of the unknown man inv. Ra 70, but who nevertheless remains very real, judging by the frowning eyebrows and the rather fixed gaze, as well as by the marked naso-labial folds and the curious expression produced by the slightly open mouth.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 199-204.
