---
title: Man’s head
id_inventaire: Ra 73 g
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre: of the "Pérouse - Toulouse" type
date_creation: Between 195 and 205
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "37"
largeur: '24,5'
longueur:
profondeur: "25"
epaisseur:
id_wikidata: Q27164560
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 270
priority: 3
id_notice: ra-73-g
image_principale: ra-73-g-1
vues: 8
image_imprime:
redirect_from:
- ra-73-g
- ark:/87276/a_ra_73_g
id_ark: a_ra_73_g
precisions_biblio:
- clef: balty_les_2021
  note: p. 44-46, 211-215
- clef: cazes_musee_1999-1
  note: p. 138
- clef: dareggi_proposito_1988
  note: p. 321-322, fig.1,3, p. 322-323, fig. 2,4
- clef: du_mege_description_1835
  note: n° 220
- clef: esperandieu_recueil_1907
  note: n° 980
- clef: fittschen_zum_1971
  note: p. 214-252
- clef: joulin_les_1901
  note: n° 297 b
- clef: rachou_catalogue_1912
  note: n° 73 g
- clef: roschach_catalogue_1892
  note: n° 73 g

---

Gallienus and Tetricus are two names of 3rd century emperors that have been successively suggested when seeking to identify this man, who is however not an emperor but an important figure in his entourage, as indicated by the presence of a replica spotted by G. Dareggi in Perugia {% cite dareggi_proposito_1988 -L none -l p. 322-323, fig. 2 et 4, -l p. 321-322, fig. 1 et 3 %}. This remarkably aesthetic portrait is marked from the outset by the energy that emanates from the very obvious movement of the head to the right - a movement emphasised by the direction of the gaze - and by the rather puffy hairstyle, with its long fringe slightly raised from the surface of the forehead, which also makes for a very dynamic overall impression. The hair is smoother on the top of the skull and at the back of the head, and quite long at the nape of the neck. The very short beard extends to just below the cheekbones and covers the entire lower part of the face - which is probably what led to him being confused with a portrait of Gallienus in the past. Unlike the specimen in Toulouse, the head in Perugia is tilted and the gaze is directed to the left, and its workmanship is also much less refined.

The hairstyle, with its long hair combed close to the skull, and the fringe distributed fairly symmetrically on both sides of the forehead, is characteristic of a whole series of portraits of young and middle-aged men who are, on the whole, contemporary to the first statue portraits of Caracalla and Geta.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 211-215.
