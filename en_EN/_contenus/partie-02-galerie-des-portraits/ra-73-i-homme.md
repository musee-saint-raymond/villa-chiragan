---
title: Portrait of a man
id_inventaire: Ra 73 i
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 225 - 250
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '34,5'
largeur: '18,5'
longueur:
profondeur: "23"
epaisseur:
id_wikidata: Q29005580
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 350
priority: 3
id_notice: ra-73-i
image_principale: ra-73-i-1
vues: 8
image_imprime:
redirect_from:
- ra-73-i
- ark:/87276/a_ra_73_i
id_ark: a_ra_73_i
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 140
- clef: du_mege_description_1835
  note: n° 224
- clef: du_mege_notice_1828
  note: n° 139
- clef: esperandieu_recueil_1908
  note: n° 994
- clef: joulin_les_1901
  note: n° 309
- clef: massendari_haute-garonne_2006-1
  note: p. 247, fig 111
- clef: rachou_catalogue_1912
  note: n° 73
- clef: roschach_catalogue_1892
  note: n° 73 i

---

The large eyes with their hollowed irises, the hair rendered with the *"a penna"* (feather) technique, and combed close to the skull, and the full chinstrap beard and very fine moustache are characteristic of 3rd century portraits.

The anxious gaze and the deeply furrowed brow express the new turn taken by Roman portraiture in this time of crisis (economic, governmental, financial and societal) and uncertainty. This was initially believed to be Volusianus, Emperor from 251 to 253.

P. Capus
