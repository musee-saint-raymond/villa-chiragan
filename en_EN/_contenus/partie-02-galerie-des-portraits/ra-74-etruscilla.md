---
title: Portrait of Herennia Etruscilla (?)
id_inventaire: Ra 74
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Middle of the 3\<sup>rd\</sup> century
materiau: Göktepe marble, district 4 (Turkey)
hauteur: '64,5'
largeur: "64"
longueur:
profondeur: "29"
epaisseur:
id_wikidata: Q26703756
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 400
priority: 3
id_notice: ra-74
image_principale: ra-74-1
vues: 8
image_imprime:
redirect_from:
- ra-74
- ark:/87276/a_ra_74
id_ark: a_ra_74
precisions_biblio:
- clef: braemer_culte_1999
  note: p. 53
- clef: cazes_musee_1999-1
  note: p. 142
- clef: du_mege_notice_1828
  note: p. 69, n° 133
- clef: esperandieu_recueil_1908
  note: p. 86-87, n° 993
- clef: felletti_maj_iconografia_1958
  note: n° 328, p. 240
- clef: joulin_les_1901
  note: p. 339-340, pl. XXV, n° 314
- clef: massendari_haute-garonne_2006-1
  note: p. 246, fig 110
- clef: musee_saint-raymond_regard_1995
  note: p. 155, n° 110
- clef: rosso_image_2006
  note: p. 484-486, n° 235
- clef: saletti_ritratti_1967
  note: p. 39, n° 29
- clef: wegner_verzeichnis_1979
  note: p. 134
- clef: wiggers_caracalla_1971
  note: p. 129

---

This is one of the largest known female portraits in Roman art. The right hand holds a fold of the great mantle that covers the shoulders. At the back of the head, a flat braid has been brought up to the top of the skull, but the lower part of this chignon was carved separately and glued into place. According to German terminology, this is known as the "half-body bust" (Halbkörperbüste) type, as it is cut off at the waist. This process is rare in Roman sculpture, and did not appear until the reign of Gordian III (238-244), an emperor whose image, now displayed in the Louvre Museum, was depicted in the same way.

The head could be compared to a portrait kept in the Palazzo dei Conservatori in Rome (inv. 2689), which seems to represent the same figure, a member of the imperial family no doubt. The same hairstyle and profile can be found on the <a href="/images/comp-ra-74-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-74-1.jpg" alt="Sesterce d'Etruscilla, Rome 250, inv. 2000.23.508 musée Saint-Raymond Toulouse, Daniel Martin CC BY-SA"/></span>coins of Empress Herennia Etruscilla</a>, spouse of Trajan Decius (249-251), now believed to be the woman portrayed here, rather than Cornelia Salonina, wife of Gallienus, or Plautilla, who was married to Caracalla, as previously suggested.

According to E. Rosso 2006, *L’image de l’empereur en Gaule romaine, portraits et inscriptions*, p. 484-486.
