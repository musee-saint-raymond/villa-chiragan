---
title: Bust of Sabina
id_inventaire: Ra 76
donnees_biographiques1: 85/87? - 136/137
donnees_biographiques2:
type_oeuvre: of the principal, or "Vatican, Busti 359" type
date_creation: Between 128 and 137
materiau: Lychnites marble (island of Paros)
hauteur: "44"
largeur: '34,5'
longueur: ''
profondeur: '40,5'
epaisseur:
id_wikidata: Q24068193
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 100
priority: 3
id_notice: ra-76
image_principale: ra-76-1
vues: 8
image_imprime:
redirect_from:
- ra-76
- ark:/87276/a_ra_76
id_ark: a_ra_76
precisions_biblio:
- clef: alexandridis_frauen_2004
  note: p. 184, n° 182 et fig. 3
- clef: balty_les_2012
  note: p. 51, fig. 50, p. 147-149, 152, 158-159, fig. 78-79, 81, 83, 86, 88-89
- clef: braemer_les_1952
  note: p. 145
- clef: carandini_vibia_1969
  note: p. 185, n° 49, pl. XCIV.221
- clef: cazes_musee_1999-1
  note: p. 124
- clef: du_mege_description_1835
  note: p. 113, n° 199
- clef: esperandieu_recueil_1907
  note: p. 86, n° 992
- clef: fittschen_katalog_1985
  note: p. 11 réplique n° 21
- clef: joulin_les_1901
  note: p. 335, n° 278
- clef: musee_saint-raymond_image_2011
  note: p. 124
- clef: musee_saint-raymond_regard_1995
  note: p. 42, n° 4
- clef: musees_departementaux_de_loire-atlantique_tresor_1987
  note: n° 96
- clef: rosso_image_2006
  note: p. 456-457, n° 218
- clef: wegner_hadrian_1956
  note: p. 90 et 130
- clef: wegner_verzeichnis_1984
  note: p. 155

---

Nothing proves that this bust was found at Chiragan. Its origin therefore remains a mystery to this day. Its inclusion in this catalogue is something of a tradition on the museum's part, as this work has always been exhibited alongside the series of portraits attributed to the *villa*. The importance of this portrait, together with the striking absence of female figures in this prestigious ensemble probably explain why successive curators have always included it.

Although broken at the bottom, the work depicts a woman wearing a crown, and a cloak that probably covered a tunic, now lost. In spite of the many alterations, this is clearly a portrait of Vibia Sabina, wife of Hadrian, as she was depicted in the most widespread type of iconography. Her features are regular, her lips delicately defined, and her almond-shaped eyes are shown to advantage by the pure line of her brow. On her head, she wears a tiara, the ends of which are hidden in the wavy mass of her hair. All of the above give this portrait a timeless quality akin to the representations of the goddesses of classical Greece. This idealised image thus introduces a strong break in the iconography used to depict the women of the imperial house. The hairstyle no longer includes braids (natural or postiche), or the curls and waves achieved by using a curling iron, as was the norm among the ladies of Trajan's family, the *Ulpii*. On the contrary, a simple and natural look is privileged here. The high chignon differs from the "nest" (*Nestfrisur*) hairstyles of the official portraits of women during Trajan's reign, as it is devoid of braids. The long wavy and twisted strands form a high, basket-shaped bun. This bust therefore belongs to the last type of official portraits of the empress, and is even, despite its poor condition, a very good replica. Some thirty copies of this type are known today, which explains the German term "Haupttypus" (main type) when referring to this series.

None of these symbols reveal the precise meaning of the last official image created during Sabina's lifetime. Yet, they depict the empress as an ideal, and even sacred, figure. These attributes also refer to a retrospective style deeply marked by Greece, in keeping with the trends at the end of Hadrian's reign.

According to E. Rosso 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 145-161.
