---
title: Bust of an unknown woman
id_inventaire: Ra 77
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 125 - 135
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "47"
largeur: "36"
longueur: ''
profondeur: "32"
epaisseur:
id_wikidata: Q24490548
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 110
priority: 3
id_notice: ra-77
image_principale: ra-77-1
vues: 8
image_imprime:
redirect_from:
- ra-77
- ark:/87276/a_ra_77
id_ark: a_ra_77
precisions_biblio:
- clef: balty_les_2012
  note: p. 47-48, fig. 46-48, p. 172, 174-176, 178, 181-182, fig. 101-104, 107, 110-112
- clef: cazes_musee_1999-1
  note: p. 133
- clef: du_mege_description_1835
  note: n° 204
- clef: du_mege_notice_1828
  note: n° 142
- clef: esperandieu_recueil_1908
  note: n° 989
- clef: joulin_les_1901
  note: n° 289
- clef: massendari_haute-garonne_2006-1
  note: p. 249, fig. 119
- clef: musee_saint-raymond_essentiel_2011
  note: p. 42-43
- clef: musee_saint-raymond_image_2011
  note: p. 84-85
- clef: rachou_catalogue_1912
  note: n° 77
- clef: roschach_catalogue_1892
  note: n° 77

---

This stylishly rendered and very graceful bust shows a young woman dressed in a pleated tunic, buttoned on the right shoulder and covered with a mantle that is wrapped around her shoulders. The outline follows the lower line of the breasts before flaring out and upwards to include the shoulders, where a break on the left side has damaged the draped fabric. The head is clearly turned to the left, and portrayed in a graceful position above a long, slim neck. The eyes, with their wide-eyed expression, are highlighted by narrow brows that extend towards the temples. The upper lip of the generous and delicately-drawn mouth has a slight upward curl.

The smooth lines of the face, with its classic oval shape, and uncarved eyes and eyebrows are in keeping with the subtle rendering of the hair. Above the forehead, it has been combed away from a centre parting, pulled back and plaited, the three braids then being used to encircle the head while completely exposing the ears. Wrapped round the head four times, these braids form a thick turban that encircles the entire skull. Finally, there are two elegant curls at the temples and a few stray wavy strands at the nape of the neck. The workmanship is remarkable inasmuch as it has been almost exclusively achieved using a straight chisel for the eyes and hair: each individual hair obtained by means of very fine incisions, executed with exceptional precision and sensitivity.

The simplicity of this hairstyle differs from the extremely varied and refined compositions that characterise female portraits during the first half of the 2nd century: it does not include the curly tiara-shaped arrangement that was popular during the Trajan dynasty, nor the elaborate hairstyles adorned with curls made with a hot iron, as popularised by portraits produced during the Antonine dynasty. The official portraits of ladies of the imperial household are, as we all know, an essential reference point when seeking to date the many anonymous faces that have been passed down to us. The most convincing elements of comparison, in this case, are certain portraits of Empress Sabina or those of her contemporaries - and a statue of Sabina depicted as the <a href="/images/comp-ra-77-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-77-1.jpg" alt="Sabine in Venus Genetrix, inv. 24, Ostie Museum, Lalupa / Wikimedia Commons CC BY-SA"/></span>Venus Genetrix from the Sodales Augustales in Ostia</a> in particular, the identification of which is controversial.

As for private portraits, the one found in Chiragan belongs to a coherent series of works dated between the reign of Hadrian and the beginning of the Antonine dynasty: variants are identified according to the higher or lower position of the band of braided hair around the forehead or head. In fact, comparing this statue portrait with other specimens produced in the *Urbs* or its surroundings area reveals the extraordinary quality of its workmanship, which can only have been achieved by an outstanding workshop. It is not impossible that this is the portrait of a prominent figure of Roman aristocracy at that time.

Some meagre clues allow us to date it with a little more precision: the absence of carved pupils and irises mean it cannot have been produced much earlier than AD 130, although this yardstick is not infallible. But when combined with the portrait's countenance, which is marked by a certain graceful gravity, we have enough evidence to link this work to that time, making this piece a very lovely specimen of a "period" face from the time of the reign of Hadrian, similar to those of the deified princesses of the *Ulpii* family, from Plotina to Marciana, the wife and sister of Emperor Trajan.

According to E. Rosso 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 171-183.
