---
title: Woman’s head
id_inventaire: Ra 82
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: The Theodosian dynasty. Approximately 375 - 425
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: '33,5'
largeur: "30"
longueur:
profondeur: "30"
epaisseur:
id_wikidata: Q24636324
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 5
order: 440
priority: 3
id_notice: ra-82
image_principale: ra-82-1
vues: 8
image_imprime:
redirect_from:
- ra-82
- ark:/87276/a_ra_82
id_ark: a_ra_82
precisions_biblio:
- clef: alfoldi-rosenbaum_portrait_1968
  note: p. 35-40, fig. 25-28
- clef: balmelle_les_2001
  note: p. 83, fig. 22, p. 229-230
- clef: baratte_histoire_1996
  note: p. 250-251
- clef: beckmann_idiom_2020
  note: ''
- clef: cazes_periple_2003
  note: p. 198-201, n° 364
- clef: centre_culturel_abbaye_de_daoulas_rome_1993
  note: n° 85.07
- clef: delbrueck_spatantike_1933
  note: n° 49 f, fig. 19
- clef: du_mege_description_1835
  note: p. 134-135, n° 239
- clef: du_mege_notice_1828
  note: p. 100, n° 242
- clef: ensoli_aurea_2000
  note: ''
- clef: esperandieu_recueil_1908
  note: p. 100, n° 1030
- clef: eydoux_france_1962-1
  note: chap. XI, p. 179
- clef: eydoux_monuments_1958
  note: couv.
- clef: gauzi_au_1925
  note: p. 84-85
- clef: heintze_spatantikes_1971
  note: p. 90, n° 104
- clef: kiilerich_late_1993
  note: p. 123-124, p. 354, fig. 68 bis
- clef: meischner_bildnisse_2001
  note: p. 403, pl. 92-fig. 2
- clef: meischner_portrat_1991
  note: p. 385‑407, n° 106
- clef: musee_saint-raymond_essentiel_2011
  note: p. 48-49
- clef: musee_saint-raymond_regard_1995
  note: p. 45, n° 7
- clef: musee_saint-raymond_wisigoths_2020
  note: p. 71-72
- clef: mussot-goulard_les_1999
  note: p. 114
- clef: popova-moroz_roman_1992
  note: p. 13-21. (bibl. de comp.)
- clef: rachou_catalogue_1912
  note: p. 52, n° 82
- clef: roschach_catalogue_1892
  note: p. 38, n° 79
- clef: stutzinger_bronzebildnis_1986
  note: p. 155, n° 37, pl. 26 b
- clef: veyne_kunst_2009
  note: p. 112
- clef: veyssiere_occupation_2008
  note: p. 327

---

This female portrait clearly stands out from all the other faces discovered in Chiragan. What’s more, despite the assertions made by the curators Ernest Roschach (1865) and Henri Rachou (1912), we have no way of confirming that this work was actually found on the grounds of the *villa* on the banks of the Garonne.

This woman’s expression, which appears in turn stern or melancholic, depending on the angle from which she is viewed, commands deep respect. The cheekbones are prominent, as is the chin, there are shadows under the eyes, the eyelids are heavy, and the angular profile of the arches of the eyebrows extends, both graphically and geometrically, to form a long, fine nose. The nasolabial folds reflect the woman's age, although she may be no more than forty {% cite cazes_periple_2003 -l 200 %}. The hairstyle is relatively similar to that of a <a href="/images/comp-ra-82-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-82-1.jpg" alt="Bust of a woman in parchment, inv. 66.25 Metropolitan museum, Public Domain http://www.metmuseum.org/art/collection/search/468716"/></span>Byzantine bust of a young woman, kept at the Metropolitan Museum of Art in New York</a> {% cite alfoldi-rosenbaum_portrait_1968 -L none -l p. 21, fig. 2-5 %}.

The schematic style and gravity of this noble face both point to a later date. The work is contemporary to the separation of the Eastern Roman Empire from the Western Roman Empire following the death of Theodosius I in 395. The reign of this emperor was therefore used as a reference point. In our opinion, this portrait probably goes back to the earliest date, the *terminus post quem,* or limit after which this portrait should be situated, the latest date being the end of the reign of Theodosius II, in the middle of the 5th century. This striking face can therefore be attributed to the reign of Theodosius. Yet, and contrary to justifiable belief, this work, which may have belonged to a statue, was designed neither in the East, nor in Rome in an oriental workshop, but in the foothills of the Pyrenees. The tests undertaken by D. Attanasio, M. Bruno and W. Prochaska reveal that the marble comes from Saint-Béat (Haute-Garonne). If this head was found on the Chiragan site, this information alone would prove not only the continued activity of the *villa* during this late period (coinage from the time of Theodosius and Arcadius was also found on site) but also the importance of its owners, under whose orders such experienced workshops laboured.

Another recently unearthed archaeological record must be taken into special consideration in the study of this portrait, the origin of which is uncertain. The record in question is a limestone head, discovered in 2005 within the framework of an archaeological intervention led by the Inrap, on the outskirts of Toulouse, on the site of Barricou, in Beauzelle. It was discovered in the fillings of one of the wells of a farm {% cite veyssiere_occupation_2008 -L none %}. The same characteristic veil encircles the head, forming a band, which, in this case if more like a roll of fabric that frames a mature face with huge eyes. The representation of this Lady of Beauzelle belongs to a category of a sculpture that is conventionally referred to as "provincial". Although the portrait is unforgiving and clumsy, it nevertheless allows for a common model to be taken into consideration, and therefore represents an interesting parallel. It would appear to bear witness to the notoriety of a person for whom an easily identifiable type of representation was created. This person, whose identity is now lost in the mist of time, must have wielded a certain power. Could this eminent figure have been part of the imperial circle?

Finally, it is worth remembering that a significant amount of small and medium sized statues retrieved from the ruins of the villa date back to the 4th and even to the first half of the 5th century. Whether inherited from previous owners or possible ancestors, or newly acquired, mythological sculptures, which had passed through the aesthetic filter of the Roman East, remained common in the middle of the 5th century. Thus, this portrait, which finds itself at the heart of such a collection, may not be as isolated as initially thought.

P. Capus
