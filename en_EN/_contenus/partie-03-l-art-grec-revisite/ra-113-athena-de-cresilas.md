---
title: Statuette of Athena
id_inventaire: Ra 113
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre: Of the "Velletri" type
date_creation: Middle of the 2<sup>nd</sup> century (?), based on an original created around 430 BC
materiau: Göktepe marble (Turkey)
hauteur: "61"
largeur: "23"
longueur: ''
profondeur: "16"
epaisseur:
id_wikidata: Q27096521
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 70
priority: 3
id_notice: ra-113
image_principale: ra-113-1
vues: 8
image_imprime:
redirect_from:
- ra-113
- ark:/87276/a_ra_113
id_ark: a_ra_113
precisions_biblio:
- clef: bulletin_1936
  note: p. 830
- clef: cazes_musee_1999-1
  note: p. 109-110
- clef: esperandieu_recueil_1908
  note: n° 907
- clef: joulin_les_1901
  note: n° 136 D
- clef: rachou_catalogue_1912
  note: n° 113
- clef: reinach_repertoire_1897
  note: n° 292
- clef: slavazzi_italia_1996
  note: p. 40-41, 185-186

---

Here the aegis is clearly visible just below the goddess's neckline, a goatskin trimmed with snakes and stamped with the head of the Gorgon Medusa. This armour, which is both defensive and offensive, together with the spear and the shield, is part of the warrior attributes of the daughter of Zeus.

This bronze statue, attributed to Kresilas, a sculptor of Cretan origin, was designed around 430 BC, and is said to have served as a model to the Roman sculptor. The right arm was raised, and her hand probably held a spear that rested on the ground. It is likely that the left arm, slightly bent, would have been pointing downwards, the hand likely resting on a shield.

The Louvre museum owns a colossal marble replica of this statuette, which is over three metres tall, and was discovered in Velletri, near Rome. Size and attitude are likely to conform to the original statue, while the arms are part of a modern restoration by Vincenzo Pacetti (1746-1820) {% cite nocca_dalla_1997 %}. This <a href="/images/comp-ra-113-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-113-1.jpg" alt="Pallas Velletri, the Louvre, Kimberly Vardeman / Wikimedia Commons CC BY"/></span>"Pallas Velletri"</a> was linked to fragmentary plaster casts discovered during the excavation of a Roman copyist workshop in Baiae, near Pozzuoli (Campania). The moulds had been made directly on Greek originals or copies of originals, thus testifying to the practice of moulding. These negative impressions are of great importance when trying to understand how works were replicated. Having acquired them, these more or less busy workshops could use them to design copies and variants intended for the Italian art market, the repute of which explains their commercialisation.

Although the statuette discovered in the remains of *Villa* Chiragan is therefore of the same type as the large statue in the Louvre, there are nevertheless some differences. The aegis and the draped fabric boast particularly deep folds, creating a more sophisticated play of light and shadow than in the monumental work. Other details distinguish the two replicas, such as the belt around the *chiton* or the length of the mantle. The two sculptures should not therefore, be seen as strict representations of the Greek type.

P. Capus
