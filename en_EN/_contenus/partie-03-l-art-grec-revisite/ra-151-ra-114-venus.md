---
title: Venus
id_inventaire: Ra 151-Ra 114
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 4<sup>th</sup> century?
materiau: Marble
hauteur: '77,5'
largeur: '28,5'
longueur:
profondeur: "20"
epaisseur:
id_wikidata: Q28523491
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 160
priority: 3
id_notice: ra-151-ra-114
image_principale: ra-151-ra-114-1
vues: 8
image_imprime:
redirect_from:
- ra-151-ra-114
- ark:/87276/a_ra_151_ra_114
id_ark: a_ra_151_ra_114
precisions_biblio:
- clef: bergmann_chiragan_1999
  note: ''
- clef: besnard_age_2019-1
  note: repr. p. 225
- clef: capus_minceur_2020
  note: ''
- clef: cazes_martres-tolosane_2005
  note: ''
- clef: esperandieu_recueil_1908
  note: n° 903, 905
- clef: joulin_les_1901
  note: p. 95-96, pl. X, n° 123 D, n° 128 E
- clef: mesple_raccords_1948
  note: p. 156
- clef: musee_saint-raymond_dieux_2010-1
  note: p. 49
- clef: musee_saint-raymond_wisigoths_2020
  note: p. 169-179 (notice P. Capus)
- clef: perrot_rapport_1891
  note: p. 64
- clef: rachou_catalogue_1912
  note: n° 114, n° 151
- clef: stirling_gods_1996
  note: ''
- clef: stirling_learned_2005
  note: ''

---

While many of the works found during the excavations of the villa were immediately given special attention and exhibited, some were relegated to the reserves. The fact that some works were not put on display can be explained. Firstly, it is important to mention the uncertainties and questions regarding the origin of a number of these sculptures: the vagueness of inventories, as well as 19th century sources, have often led curators to remain cautious and reserved when it comes to attributing certain marble items to the *villa*. And of course there were aesthetic biases, which resulted in some works being excluded. These obviously suffered from preconceptions based on the history of forms. They were attributed to less competent workshops, too often compared to more classical statuary, and therefore sentenced to a life in exile. And for each curator confronted with a lack of space in the exhibition areas, this sentence is validated by the obligation to make a drastic selection. Typically, this statue of Venus was no exception, as it embodies, better than any other, a type of work that is easily overlooked.

Crowned with a tiara, the figure is characterised by an unusual slimness and proportions that immediately draw one’s attention. Its elongated forms and flat buttocks and torso certainly set the work apart. When the two fragments that make up her body were discovered in 1890, *Villa* Chiragan was undergoing its third major official excavation campaign directed by Albert Lebègue, professor at the Faculty of Literature in Toulouse {% cite perrot_rapport_1891 -l 415 %}. The break was just above the umbilical region, and both parts were therefore stuck back together again. The right hand holding a strand of hair is said to have been unearthed at the same time {% cite esperandieu_recueil_1908 -L none -l n° 903 %}. The head, probably discovered during the first half of the century, and already kept among the collections of the museum in Toulouse, was not reunited with its body until 1948 by Paul Mesplé {% cite mesple_raccords_1948 -l 156 %}. Émile Espérandieu erroneously attributed his discovery to the excavations led by Léon Joulin in 1897-1899 {% cite esperandieu_recueil_1908 -L none -l n° 905 %}. This head may have been mentioned on the first page, now unfortunately missing, of the handwritten notebook corresponding to the discoveries of the 19<sup>th</sup> and 20<sup>th</sup> September 1826 {% cite balty_les_2005 -l 35 %}.

#### Description

This Venus is entirely naked. The weight of the body is resting on the left leg. Her head is turned to the left, and slightly bent forward. The bust is particularly slender, the breasts small, and the pelvis, which is wide but very short, contrasts with the upper part of the body. The neck, finally, which is long and thin when viewed from the front, appears thicker when viewed from the left, looking more like a cylinder whose connection with the face (which is delicate and clearly oval in shape) appears less natural. On her head she wears a smooth tiara. Two strands of hair have escaped from her chignon, one straying over her left shoulder and the other over her right arm. They were probably damaged and restored using stucco, as the marks made at shoulder blade level with a point tend to prove, as these could correspond to so many pit marks made to facilitate the adherence of the stucco. Five points of contact, four at the top of the right thigh, and another in the lower abdominal area, show that the left hand was positioned in front of the pubic area. On the left thigh, at the level of the vastus lateralis muscle, a tenon indicates the position formerly occupied by the element that once ensured the statue's stability. Three points of contact on the hip probably correspond to the end of the caudal fin of a dolphin, endowed, according to the ancient iconographic tradition regarding this animal, with several appendages. The cetacean's head probably rested directly on a moulded plinth, a block shared by the goddess and her seafaring companion.

#### A famous model…

This work has been said to be a very late variant of the Knidian Aphrodite, an illustrious and recurring model created in the workshop of Praxiteles around 360 BC {% cite pasquier_praxite_2007 -l 172-195 %}. The original, made of marble, and intended as an ode to the beauty of the beloved Phryne, whose radiant silhouette could but suit the goddess of Cyprus, is now lost. The Roman version at the Vatican Museums, known as the "Venus of the Belvedere" (inv. 4260), is said to adhere to the same design. But this body, whose total and radiant nudity did not require superfluous finery, referred to a bodily beauty hitherto limited only to the male anatomy. It generated an infinite number of copies and all kinds of by-products, as evidenced by the variants attributed to oriental workshops. Driven by a constantly growing market of collectors, between the end of the 2nd century BC and the course of the following century, the sculptors of Rhodes or Alexandria designed a medium-sized statuary, often full of references and allusions to the masterpieces produced during the classical period. Such is the case of a Rhodian Aphrodite Anadyomene in particular, whose body, when seen from the sides, is characterised by its very slim frame {% cite queyrel_sculpture_2016 palaggia_atticism_1998 -L none -l p. 95-96, fig. 77 -l 94-99 %}.

It is widely known that the reproduction industry and Western commercial outlets were to prove fundamental in passing on mythological references from the ancient Greek civilization to rich buyers eager to collect and display them around their homes and even in associative and commercial premises. The decorative statuary designed especially for them was not devoid of sensuality and even humorous details, as well as delightful anecdotal references (*Pan and* *Aphrodite* commissioned by the Beiruti merchants' association of Delos, and now kept at the National Museum of Athens, is a good example of this).

Some five hundred years later, the Venus discovered at *Villa* Chiragan is the natural heiress of such compositions. The left hand held in front of the pubic area, a gesture intended to convey her modesty but also to draw attention to the vigorous fertility of this daughter of Uranus, maintains a direct link with Praxiteles' masterpiece. It is indeed reminiscent of the sculpture acquired by the island of Knidos, to which the leftward position of the head and gaze also refer {% cite corso_art_2007 -l 9-22 %}. With this statue of Venus however, it is the left hand that is held in front of the genitals. This inversion can be explained by the aesthetic variations, of which there are very few considering the number of centuries that have gone by, that nevertheless act as so many filters between the original work and the works produced during the imperial period. The most famous of these derivatives, which is characteristic of this inverted gesture, is of the *pudica* type, also known as the Venus de' Medici. It is also worth noting the position of the right arm, which is also broken, but still bears a stray strand of hair. Her arm was vigorously held aloft in a gestures reminiscent of the Aphrodite Anadyomene, a creation of the painter Apelles of Kos, if we are to believe Athenaeus {% cite athenee_deipnosophistes_nodate -L none -l XIII, 59 %}. She is also said to have been inspired by the bewitching courtesan Phryne {% cite athenee_deipnosophistes_nodate -L none -l XIII, 59 %}.

Exported to Rome following the Roman conquests at the end of the Republic, and exhibited on the orders of Augustus in the temple of Caesar according to Pliny, Apelles's tablet (*pinax*) was severely damaged before being reproduced during the reign of Nero {% cite pline_lancien_histoire_nodate -L none -l 36.28 %}. In the same way that we imagine that Praxiteles' Venus is about to enter the water {% cite corso_art_2007 -l 14 %}, we can easily picture that the *anadyomene* is emerging from it, gathering her wet hair with both hands on either side of her head as she does, to wring the water from it. This gesture would be reminiscent of that of the athlete tying his headband, a famous marble statue known as *Diadumenos* created by Polyclitus {% cite rolley_sculpture_1999 -l 35-36 %}. This in the round *anadyomene*, a female transposition of the work attributed to Polykleitos, was probably created in the fruitful context of Alexandrian workshops during the first century BC. It emphasises and conveys the Greek concept of *charis* (grace) of this female body with its distinctive curves, full lines, wide hips and small, firm breasts. Like her pubic hair, the hair on her head is shown to symbolise the fertile power of women, as well as their seductive power and strong urge to please {% cite bodiou_chevelure_2006 -l 187 %}. The twisting of the wet hair would appear to refer to a different form of bondage: that of bound breasts, a symbolic gesture that stems from a long tradition in the Eastern world {% cite de_matteis_sullafrodite_2004 -l 129 %}.

#### …but a model that has been readapted

Although the system of ideal proportions adopted for the representation of Venus during the classical period depended on a canon that gave pride of place to round, robust forms, there is nothing of the kind in our sculpture. The fact that this work was relegated to the museum’s reserves, and the unjustified indifference towards it, could be explained as we have said, by aesthetic differences when compared to the prototype: unattractive proportions, the unconventional slenderness of her thorax, the flatness of the whole body characterised by the unformed buttocks that extend her already unshapely back, and finally the neck, which resembles a long cylinder when viewed from the front, and is too thick when viewed from the side. When seen from the left, its very abrupt and seamless connection with the face is unsightly.

While Venus's body appears particularly flat and slender when viewed from behind, and even more so when seen from a three-quarter angle, it may well be that this elongation and deformation is based on a particular taste and, obviously a style introduced in certain oriental workshops during the last centuries of antiquity {% cite stirling_gods_1996 stirling_divinities_1996 -l 215 -l 103-143 %}. To understand this, we need to refer to a group of statues from the Cyrene Sculpture Museum {% cite noauthor_lexicon_1990 -L none -l II, 1, 157, 69 ; II, 2, 161, fig. 69 %}, which were nevertheless probably produced at an earlier date, as well as a <a href="/images/comp-ra-151-ra-114-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-151-ra-114-1.jpg" alt="Artemis of the Rospigliosi-Latran type, Staatlichen Kunstsammlungen Dresde" data-link="https://skd-online-collection.skd.museum/Details/Index/166375" data-link-title="skd.museum"/></span>series of medium-sized statues kept in Dresden</a> (of the "Rospigliosi-Latran" type), {% cite noauthor_lexicon_1990 -L none -l II, 1, 646, 251 - 283 %}), whose very slim torsos associated with solid lower limbs, border on the strange, and allow for a graphic rather than a realistic representation of the body {% cite bergmann_chiragan_1999 -L none -l pl. 56-57 %}. Some other elements betray this aesthetic trend, such as the S-shaped curl on the forehead, the slightly abrupt transitions of the outlines of the face, the eyebrows that form an acute angle, and the deep furrows that separate the wavy strands of hair in a rather artificial way. These characteristics allow us to link together a series of works, the late dating of which was appropriately justified by Marianne Bergmann: Hygieia of Rhodes {% cite bergmann_chiragan_1999 -L none -l 51, pl. 49-1 %}, female heads from Dijon {% cite bergmann_chiragan_1999 -L none -l p. 45, p. 47, pl. 55-2, 56-2 %} and Chiragan {% cite bergmann_chiragan_1999 -L none -l p. 54, pl. 55-3.4, 56-3 %}, Apollo from Saint-Georges-de-Montagne, {% cite bergmann_chiragan_1999 -L none -l p. 54, pl. 55-1, 56-4 %} as well as a large female portrait at the Musei capitolini {% cite bergmann_chiragan_1999 erim_aphrodisias_1986 floriani_squarciapino_scuola_1943 -l 61-63 -L p. 59, pl. 19-20 %}.

Venus is apparently, and by far, the most popular mythological figure in *villae* in activity during this period, followed by representations of satyrs, which evoked the other dominant god: Dionysus {% cite videbech_private_2015 -l 473 %}. Characterised as it is by its hybrid gestures, Chiragan's sculpture therefore belongs to a later period, during which such small and medium sized statues flourished, and were distributed in entrances, dining rooms and baths, and of course gardens, some in private spheres, and others in semi-public and even public areas.

P. Capus
