---
title: The abduction of Persephone
id_inventaire: Ra 152
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 1<sup>st</sup> century
materiau: Marble
hauteur: "43"
largeur: "46"
longueur: ''
profondeur: '4,5'
epaisseur:
id_wikidata: Q25216237
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 110
priority: 3
id_notice: ra-152
image_principale: ra-152-1
vues: 1
image_imprime:
redirect_from:
- ra-152
- ark:/87276/a_ra_152
id_ark: a_ra_152
precisions_biblio:
- clef: bulletin_1936
  note: p. 835, n° 152, p. 837
- clef: cazes_musee_1999-1
  note: p. 74, 116-117
- clef: esperandieu_recueil_1908
  note: p. 35-36, n° 898
- clef: joulin_les_1901
  note: p. 97-98, pl. XI, n° 134 E
- clef: labrousse_art_1956
  note: p. 31, n° 34
- clef: massendari_haute-garonne_2006-1
  note: p. 257, fig. 144
- clef: musee_petrarque_triomphe_2004
  note: p. 103, n° 58
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 96
- clef: musee_saint-raymond_essentiel_2011
  note: p. 34-35
- clef: rachou_catalogue_1912
  note: p. 66, n° 152

---

Marble tablets are very rare. They are inspired by works painted using fresco or encaustic techniques on wood, which made them easy to carry. These panel-paintings, called *pinakes* in Greek and *tabulae* in Latin, were extremely popular in beautiful dwellings. Between 1897 and 1899, Léon Joulin had the pleasure of discovering this remarkable, small, neo-Attic relief, which had been broken into several pieces. Dated from the Early Roman Empire, it depicts the abduction of Persephone, or Kore, by Hades, god of the underworld (Proserpina and Pluto in Rome). Hermes, the messenger of the gods, shows the way while Athena, who is armed, and one of the captive's companions, try to stop the chariot.

This is one of the great Greek myths on which the famous Eleusinian Mysteries were based, the liturgies of which were still very much alive under the Roman Empire. The young Persephone, daughter of Demeter and Zeus, was picking narcissi in the Sicilian countryside when a quadriga led at high speed by Hades burst through a cleft in the earth. Because he was in love with Persephone, the god of the underworld abducted her. Having been informed of this misfortune, the desperate Demeter sets out in search of her daughter. Helios, the sun, who was the only witness to the drama, tells her that the master of underworld powers is her captor. Aphrodite inspired this guilty love in him, Zeus consented and Hermes aided and abetted. In order to take revenge on the four godly accomplices, Demeter leaves Olympus and prevents all the plants from growing, thus rendering the earth sterile. In order for the soil to become fertile again, Persephone is forced to share her existence between Hades and Demeter: during the first half of the year she must reside in the underworld and during the second half she must remain on Earth. The yearly return of the young girl brings about the rebirth of vegetation, especially the germination of cereals, sown by Triptolemus, hero of Eleusis, protected by Demeter. Although the lush fields provide good food and sustain life, the abduction of the girl and her stay in the underworld is, on the contrary, an allegory of death.

Chiragan's tablet shows the quadriga led by a winged putto who is not only responsible for Hades' feelings, but also for all that ensued from it. Two other putti fly overhead, one holding a crown of victory over Hades's head. The latter, turning suddenly, holds the horse-leading cupid in one hand and carries Persephone on his shoulders. She struggles vigorously, clothes and hair blowing in the wind, uttering a loud cry that is heard throughout the universe. One of her companions, perhaps her half-sister Artemis, is surprised as she crouches down in front of a vase. Her other half-sister, Athena, wearing a helmet and holding a spear and shield, confronts Hermes, who indicates the route to the underworld. A third woman, a nymph, goddess or simple friend of Persephone’s, attempts to sever the reins of the carriage with her sword. At this very moment, this stampede is disrupted by the attack of a large snake, a chthonic symbol related to the womb-like forces of the underworld. The reptile alarms the immortal black horses, causing them to rear up. In this image, where so many actions are occurring at once, the forces of nature and the gods come to blows. The movement of the flowing fabrics, sometimes pressed against the body, sometimes flapping loose, as well as the jerky tempo, help to create an extremely animated relief, the details of which also enhance the quality of the work.

A second less complete tablet was also found among the remains of the *villa*. ([*inv*. Ra 32](/en/ra-32)). It was long seen to depict fauns, small country genii who represent untamed nature, as do satyrs, nymphs and sylvani.

P. Capus
