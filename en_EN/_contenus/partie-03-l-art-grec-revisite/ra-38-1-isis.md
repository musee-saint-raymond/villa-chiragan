---
title: Isis
id_inventaire: Ra 38 (1)
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: '3<sup>rd</sup>- first third of the 4<sup>th</sup> century(?) '
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "186"
largeur: "74"
longueur: ''
profondeur: "40"
epaisseur:
id_wikidata: Q26721217
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 40
priority: 3
id_notice: ra-38-1
image_principale: ra-38-1-1
vues: 1
image_imprime:
redirect_from:
- ra-38-1
- ark:/87276/a_ra_38_1
id_ark: a_ra_38_1
precisions_biblio:
- clef: balty_les_2008
  note: p. 16
- clef: cazes_musee_1999-1
  note: p. 100
- clef: clarac_musee_1841
  note: p. 586
- clef: du_mege_description_1835
  note: n° 109
- clef: du_mege_notice_1828
  note: n° 46
- clef: esperandieu_recueil_1908
  note: n° 927
- clef: joulin_les_1901
  note: n° 177 B
- clef: massendari_haute-garonne_2006-1
  note: p. 254, fig. 139
- clef: rachou_catalogue_1912
  note: n° 38
- clef: roschach_catalogue_1865
  note: n° 38

---

Egypt and its culture were fashionable throughout the Roman Empire. While the figure of Isis is often found on certain everyday objects, she appears to have reigned supreme at *Villa* Chiragan. This fragmentary sculpture is indeed the largest to have been found among the impressive quantity of marble discovered on the grounds of the former residence. Even without its head, the work is almost two meters tall. The black veined marble, which was extracted from Saint-Béat in the Pyrenees (Haute-Garonne), reflects the symbolic colour of the goddess. The sacred mantle edged with a fringe is another distinctive sign {% cite malaise_a_2011 -l 471-472 %}. Most importantly, several sections of it, drawn up towards the bust and between the breasts, are secured by a so-called Isiac knot, reminiscent of the hieroglyph *ankh*, a symbol of life itself. Cavities had been created to accommodate <a href="/images/comp-ra-38-1-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-38-1-1.jpg" alt="Isis, Kunsthistorisches Museum Vienne, Gryffindor / Wikimedia Commons CC BY"/></span>"the feet, arms and head, probably carved in white marble, which have also disappeared.</a>"

The Ptolemies, masters of Egypt since the end of the 4th century BC, endowed the great Egyptian goddess with a Greek appearance following the introduction in Alexandria of the divine triad she formed with Serapis and Harpocrates. Isis, who is associated with the origins of the Universe and is the embodiment of fertility, human activities, justice and moral precepts, is also a healer.

Because of the links maintained by the tyrants of Syracuse with the Ptolemies, her cult had spread throughout Eastern Greek Sicily by at least the end of the 3rd century BC. The spread of the Isiac cult in the Mediterranean and the Western world however began as a result of individual initiatives, particularly in the context of commercial travel. In La Campania, Rome, and probably elsewhere in the Italian peninsula, Isis worship dates back to the 2nd century BC. Even more than Sicily, it is probably Delos, in the Aegean Sea, that should be seen as the major source of this import. The island of the legendary birth of the god Apollo then became the epicentre of the slave trade in the eastern Mediterranean, and the place where many Italians, businessmen, bankers and even craftsmen associated with these cults that originated in Alexandria, acquired considerable wealth {% cite bricault_isis_2007 bricault_isis_2004 -l 265-267 %}. As early as the 1st century BC, the Isiac cults had permeated the other regions of the western world, and although strongly opposed by Augustus and Tiberius, they were to find a zealous protector in the person of Caligula. But it is also worth noting the support afforded in the 2nd century by Hadrian in particular, whose enthusiasm for the aura surrounding Egyptian spirituality is intimately associated with his personal history, and the death of his lover Antinous.

As for numismatics, very few issues depicting the so-called Isiac deities were minted from the end of the 1st century AD. But the gods of the Delta were represented much more frequently during the Severan dynasty, and the first third of the 3<sup>rd</sup> century in particular. Later, from the Tetrarchy and the Emperor Diocletian, some modest brass coins stamped with the words VOTA PUBLICA (*Public Vows*), distributed on the 3<sup>rd</sup> of January, were associated for the first time with the Isiac gods. Imperial extravagance, expressed through the distribution of these small items to the working classes, reflected the wish for prosperity addressed to the Empire and its subjects, and therefore included the demiurge goddess Isis, Serapis and, to a lesser degree, Harpocrates. Although paradoxically pursued during the reign of the Christian emperors during the 4th century, the minting of these "coins" could in fact testify to the claims made by the conservative current, and by an anti-Christian aristocratic class which, in Rome, aspired to fidelity to the ancestral polytheistic religion rather than to a specific attachment to the Alexandrian gods {% cite bricault_isis_2011 -l 16 %}.

The importance given to the Isiac deities by the augusti Diocletian and Maximian Hercules is reflected in the copper coins of the *Vota publica*. This phenomenon may have something to do with the presence of the monumental Isiac statues unearthed at *Villa* Chiragan. The size of these sculptures, which is exceptional in a private setting, is indeed food for thought. Although it is difficult to speculate on the existence of a sanctuary inside the *villa* itself, the presence of this triad undoubtedly refers to a particular attachment on the part of one of the owners of the site, to the healing and fertilising powers of these Alexandrian gods.

P. Capus
