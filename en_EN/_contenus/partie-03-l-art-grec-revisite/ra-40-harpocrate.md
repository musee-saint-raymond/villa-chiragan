---
title: Harpocrates
id_inventaire: Ra 40
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 3<sup>rd</sup>-first third of the 4<sup>th</sup> century(?)
materiau: Marble
hauteur: '111,5'
largeur: '43,8'
longueur: ''
profondeur: "28"
epaisseur:
id_wikidata: Q26720973
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 60
priority: 3
id_notice: ra-40
image_principale: ra-40-1
vues: 4
image_imprime:
redirect_from:
- ra-40
- ark:/87276/a_ra_40
id_ark: a_ra_40
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 102
- clef: clarac_musee_1841
  note: p. 585
- clef: du_mege_description_1835
  note: n° 126
- clef: du_mege_notice_1828
  note: n° 56
- clef: esperandieu_recueil_1908
  note: p. 49, n° 923
- clef: hostein_roman_2016
  note: vol. IX, n° 282
- clef: joulin_les_1901
  note: n° 179 B
- clef: lafore_culte_nodate
  note: p. 10
- clef: lebegue_ecole_1889
  note: p. 10-11
- clef: massendari_haute-garonne_2006-1
  note: p. 254, fig. 140
- clef: rachou_catalogue_1912
  note: n° 40
- clef: reinach_repertoire_1897
  note: t. II, vol. II, p. 430, fig. 8
- clef: roschach_catalogue_1865
  note: n° 40
---

This statue is the largest representation of Harpocrates ever discovered in France. The god carries a horn of plenty, symbol of fertility and fecundity. Although the neck and legs have been reconstructed, but the right arm has not because the exact position of the hand is not known. Yet is it highly likely that the index finger was raised to the lips, which for the Romans symbolized an appeal for silence, thus signifying that any intelligence was restricted to those who worshipped Isis {% cite matthey_chut_2011 -l 545-546 %}. Although this invitation to secrecy is mentioned by written sources in the 1st century AD, the gesture was originally shown by a hieroglyph meaning "child" (meaning he or she who does not speak).

The Macedonian Ptolemaic dynasty that had ruled Egypt since the end of the 4th century BC introduced a divine triad consisting of Isis, Serapis and Harpocrates in Alexandria, its capital. In a now Hellenised form, the latter referred to Horus, the ancestral Egyptian falcon-god, posthumous son of Osiris begotten by Isis. In Pharaonic Egypt, Horus took the name Heru-pa-khered (Horus the Younger), when he was represented and venerated in the form of a child, the name from which the deity’s Greek name was taken. It is therefore this youthful aspect that was preserved during the Hellenistic period, and again during the Roman period. In this context, the horn of plenty is suggestive of childbirth. The god was closely linked to the rising sun, thus, in the Eastern Greek world, as in the Rome of the last days of the Republic, Harpocrates' relationship to the sun was reinforced by his assimilation with Apollo.

Although fairly rarely mentioned in inscriptions, the son of Isis was, on the contrary, abundantly represented on lamps or in hard stone, bronze or terracotta artefacts from the 2nd century onwards. Harpocrates went on to became a deity who protected homes, and was particularly popular among the lower classes. The posture, gestures and attributes of the sculpture discovered in Chiragan are also found on some <a href="/images/comp-ra-40-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-40-1.jpg" alt="Tetradrachm of Trajan, © University of Oxford Roman Provincial Coinage Online"/></span>provincial coins minted in Alexandria under Trajan</a>. The same characteristics can be seen on issues from Asia Minor during the 3<sup>rd</sup> century, coins minted in the province of Lycia et Pamphilia, for Gaius Iulius Verus Maximus, son of the emperor Maximinus Thrax (235-238), or in that of Bithynia, in the effigy of Trajan Decius and even <a href="/images/comp-ra-40-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-40-2.jpg" alt="Estruscilla coin, workshop of Nicea (Bithynia), Bibliothèque nationale de France, Public domain" data-link="https://gallica.bnf.fr/ark:/12148/btv1b8555621g/f2.item.zoom" data-link-title="gallica.bnf.fr"/></span>his wife Herenia Estruscilla</a>, in the middle of the century. On them, the empress is associated with a figure of Harpocrates, which undoubtedly referred to the couple's children and, consequently, to the future of the imperial house {% cite amandry_roman_2015 -L none -l 4183 %} {% cite hostein_roman_2016 -L none -l n° 282 %}. Although a portrait of Herenia Estruscilla was discovered in Chiragan (it is one of the most imposing female images known to have been produced during the entire Roman period), there are no clues to link it to the Isiac sculptures unearthed on the site of the *villa*.

P. Capus
