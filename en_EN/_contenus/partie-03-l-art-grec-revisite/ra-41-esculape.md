---
title: Asclepius
id_inventaire: Ra 41
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: '2<sup>nd</sup> century '
materiau: Afyon marble (Turkey)
hauteur: "69"
largeur: "43"
longueur: ''
profondeur: '19,5'
epaisseur:
id_wikidata: Q28464617
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 100
priority: 3
id_notice: ra-41
image_principale: ra-41-1
vues: 2
image_imprime:
redirect_from:
- ra-41
- ark:/87276/a_ra_41
id_ark: a_ra_41
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 110-111
- clef: du_mege_description_1835
  note: n° 116
- clef: du_mege_notice_1828
  note: n° 53
- clef: esperandieu_recueil_1908
  note: n° 912
- clef: joulin_les_1901
  note: n° 144
- clef: landes_dieux_1992
  note: n° 10
- clef: rachou_catalogue_1912
  note: n° 41
- clef: roschach_catalogue_1865
  note: n° 41
- clef: thiollier-alexandrowicz_itineraires_1996
  note: p. 175

---
Asclepius (*Asklēpiós* in Greek), god of medicine, was entrusted at a very young age by his father, Apollo, to the centaur Chiron who taught him the art of healing. This discipline is symbolised, among other things, by the serpent, whose tail can still be seen on the left-hand side of the plinth, which would have been wrapped around the gnarled stick held by the god of Epidaurus.

Initially seen as a <a href="/images/comp-ra-41-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-41-1.jpg" alt="Asclepius of Bulla Regia, Bardo Museum (Tunisia), Elcèd77 / Wikimedia Commons CC BY-SA"/></span>variant of a Greek original</a> from the 5th century BC, this work is now listed as a pastiche. Assuming that this type was created during the imperial period, it still retains the essence of classical Greek art {% cite koppel_petite_2006 %}. The prototype proved a great success, as evidenced by <a href="/images/comp-ra-41-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-41-3.jpg" alt="Asclepius, National Archaeological Museum of Tarragone, Ramon Cornadó, Tarragone Museum"/></span>another replica</a> of the same size discovered in the cold room (*frigidarium*) of the lower baths of the *villa* d'Els Munts in Altafulla (Catalonia) {% cite koppel_decoracion_1995 -l 42 %}. Could this medium sized statue discovered in Chiragan at the beginning of the excavations by Alexandre Du Mège in 1826, also come from one of the *villa*'s two bathing complexes? The deep connection between water and the healing god does not contradict this theory.

P. Capus
