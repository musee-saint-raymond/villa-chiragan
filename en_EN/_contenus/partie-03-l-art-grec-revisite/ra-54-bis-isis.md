---
title: Head of a deity (Isis?)
id_inventaire: Ra 54 bis
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 2<sup>nd</sup>- 3<sup>rd</sup> century
materiau: Marble
hauteur: "45"
largeur: "28"
longueur: ''
profondeur: "25"
epaisseur:
id_wikidata: Q27145780
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 30
priority: 3
id_notice: ra-54-bis
image_principale: ra-54-bis-1
vues: 8
image_imprime:
redirect_from:
- ra-54-bis
- ark:/87276/a_ra_54_bis
id_ark: a_ra_54_bis
author: Daniel Cazes
author_id: Cazes D.
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 100-101
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: 'p. 154, n° 85 '
- clef: esperandieu_recueil_1908
  note: n° 926
- clef: joulin_les_1901
  note: n° 180
- clef: massendari_haute-garonne_2006-1
  note: p. 256, fig. 142
- clef: rachou_catalogue_1912
  note: n° 54 bis

---

Was this lovely female head actually found in Chiragan? It was originally believed to represent Ariadne, the companion of Theseus and later Dionysos, or Isis. The representations of the Egyptian goddess show a very similar hairstyle, with characteristic ringlets falling down to her shoulders. Here, the braided strands are held back by a double ribbon positioned above the forehead and to the sides of the head. Maybe the three strands arranged on the top of her head, which were either left unfinished, or subsequently broken or reshaped, were used to support a crescent moon, horns, or a disc? In this case, we could safely assume that this is indeed the head of Isis.

D. Cazes
