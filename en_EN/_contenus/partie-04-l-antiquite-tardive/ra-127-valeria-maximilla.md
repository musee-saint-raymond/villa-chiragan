---
title: Portrait of Valeria Maximilla (?)
id_inventaire: Ra 127
donnees_biographiques1: Daughter of Emperor Galerius (Caesar then Augustus), wife of Emperor Maxentius
donnees_biographiques2: Around 280 – after 312
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: '32,5'
largeur: '26,5'
longueur:
profondeur: '24,5'
epaisseur:
id_wikidata: Q26707632
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 40
priority: 3
id_notice: ra-127
image_principale: ra-127-1
vues: 7
image_imprime:
redirect_from:
- ra-127
- ark:/87276/a_ra_127
id_ark: a_ra_127
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 230-231, fig. 125 d
- clef: balty_les_2008
  note: p. 21-22, 112, 115-119, 121, fig. 100 et 4e de couv.
- clef: bergmann_chiragan_1999
  note: p. 34, 40-41
- clef: cazes_musee_1999-1
  note: p. 144
- clef: ensoli_aurea_2000
  note: p. 459, n° 57
- clef: esperandieu_recueil_1908
  note: p. 92-93, n° 1004
- clef: hannestad_tradition_1994
  note: p. 133
- clef: joulin_les_1901
  note: p. 338-339, pl. XXIII et n° 301 D
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig. 115
- clef: musee_saint-raymond_regard_1995
  note: p. 238, n° 174
- clef: rachou_catalogue_1912
  note: p. 61, n° 127
- clef: rosso_image_2006
  note: p. 490-491, n° 239

---

This portrait could well be that of the daughter of Galerius (who was Caesar to Diocletian), married to Maxentius in 293. There are no other known examples of this type of hairstyle in ancient portraiture (but curiously, old photographs of Croatian folk traditions include quite similar styles). Although the fashion for "melon-coiffure" hairstyles dates back to the 2nd century (with Empress Faustina the Younger), never before did it reach such a degree of complexity as here. Roman hairdressers and wigmakers, as we know, surpassed themselves in their art.

This work, by its workmanship and proportions, alludes to the portraits that surround it. This series forms an exceptional family ensemble in the form of monumental, full-length sculptures of which, unfortunately, only the heads remain.

According to J.-C. Balty, *Les portraits romains, La Tétrarchie, 1.5* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, 2008, p. 111-122.
