---
title: Decorative plaque
id_inventaire: 2000.176.2
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 3<sup>rd</sup>-4<sup>th</sup> century
materiau: Marble from the Pyrenees
hauteur: "18"
largeur: '28,5'
longueur:
profondeur: '3,5'
epaisseur:
id_wikidata: Q48572297
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 420
priority: 3
id_notice: 2000-176-2
image_principale: 2000-176-2-1
vues: 1
image_imprime:
redirect_from:
- 2000-176-2
- ark:/87276/a_2000_176_2
id_ark: a_2000_176_2
precisions_biblio:
- clef: cazes_musee_1999-1
  note: ''
- clef: joulin_les_1901
  note: p. 83, pl. V, n° 27

---
