---
title: Pilaster capital
id_inventaire: 2000.180.1
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Marble
hauteur: "20"
largeur: "30"
longueur:
profondeur: '8,5'
epaisseur:
id_wikidata: Q47532029
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 310
priority: 3
id_notice: 2000-180-1
image_principale: 2000-180-1-1
vues: 1
image_imprime:
redirect_from:
- 2000-180-1
- ark:/87276/a_2000_180_1
id_ark: a_2000_180_1
precisions_biblio:
- clef: balmelle_les_2001
  note: ''
- clef: cazes_musee_1999-1
  note: p. 79-82
- clef: joulin_les_1901
  note: p. 84, pl. V, n° 18 B
- clef: kramer_korinthische_1994
  note: ''

---

If the style of this capital is to be linked to the Corinthian order characterised by rows of superimposed leaves, here the sculptor has taken some liberties with the conventions of the classical period. The thick, flat leaves are separated from each other by means of drop-shaped holes made with a trepanning tool (a sort of drill bit), a practice more in keeping with oriental aesthetics. It is indeed to Asia Minor, and especially Constantinople, that one must turn to find similar compositions and carving techniques. This crowning element therefore also tends to support the idea that Chiragan was involved with workshops of oriental origin, or at least workshops subject to the aesthetic trends favoured in Asia Minor.

P. Capus
