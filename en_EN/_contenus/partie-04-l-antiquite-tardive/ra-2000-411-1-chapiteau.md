---
title: Corinthian capital with foliate head
id_inventaire: 2000.411.1
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Marble
hauteur: '49,5'
longueur:
largeur: "76"
profondeur: '17,2'
epaisseur:
id_wikidata: Q47531958
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 300
priority: 3
id_notice: 2000-411-1
image_principale: 2000-411-1-1
vues: 1
image_imprime:
redirect_from:
- 2000-411-1
- ark:/87276/a_2000_411_1
id_ark: a_2000_411_1
precisions_biblio:
- clef: agusta-boularot_les_2016
  note: ''
- clef: beckmann_idiom_2020
  note: p. 145-146, fig. 11
- clef: cazes_musee_1999-1
  note: p. 81
- clef: hirschland_head-capitals_1967
  note: p. 14
- clef: joulin_les_1901
  note: pl. IV, n° 6 B
- clef: massendari_haute-garonne_2006-1
  note: p. 240, fig. 98
- clef: mercklin_antike_1962
  note: ''
- clef: musee_saint-raymond_palladia_1988
  note: ''
- clef: picard_acroteres_1963
  note: ''
---

In Chiragan, a series of capitals similar to this one had been created for pilasters, and another unfinished capital is now kept in the museum's reserves. Here, a head surrounded by foliage surmounts two rows of acanthus leaves. The head itself resembles a mask, and is located at abacus level, above the central acanthus of the *calathus* (basket), in lieu of the canonical rosette of the Corinthian order.

Head motifs had already been used to decorate capitals in Pharaonic Egypt, as evidenced by the famous "hathoric" style {% cite bernhauer_hathorsaulen_2005 %}. A different composition, in line with Greco-Roman sensibilities, once again reunites both elements in the West during Classical Antiquity. This recurring ornamental theme can be found in Etruria, Vulci and Sovana, and in the Greek cultural context of southern Italy, in Campania or Apulia, in Taranto, Arpi and Canosa, which boast numerous examples {% cite pensabene_il_1990 -L none -l 283-285 et 302-306, fig. 13, 18, Pl. CXV-CXIX %}. In the 1st century BC, capitals and figured acroteria in *Glanum* (Saint-Rémy-de-Provence), Vernègues, Nîmes, Toulouse and more recently Narbonne (Colonia Narbo Martius), among others, bear witness to this practice of associating figures with a crowning architectural ornament {% cite mercklin_antike_1962 picard_acroteres_1963 musee_saint-raymond_palladia_1988 agusta-boularot_les_2016 -L none -l 109, no299-300 et fig. 534-535 et 538, 112, no313 et fig. 555-556 -L none -l p. 168-174, fig. 46, 48 et 49 -L none -l n° 256, daté du Ie siècle de n. è. -L none -l p. 283, fig. 10-12, p. 286-288, fig. 17 et 18 %}. Capitals sporting foliate masks were apparently produced in Asia Minor from the end of the 2nd century and during the following century {% cite hirschland_head-capitals_1967 -l 14 %}. The style and patterns of these oriental productions are said to have been taught in sculptors' workshops in the West, and initially in Rome during Late Antiquity, and the workshop commissioned to contribute to Chiragan's lavish decor was one of them.

P. Capus
