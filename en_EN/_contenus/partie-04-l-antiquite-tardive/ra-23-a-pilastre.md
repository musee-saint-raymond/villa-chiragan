---
title: Fragment of a pilaster or door jamb
id_inventaire: Ra 23 a
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Marble
hauteur: "197"
largeur: "64"
longueur:
profondeur: "12"
epaisseur:
id_wikidata: Q48327507
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 320
priority: 3
id_notice: ra-23-a
image_principale: ra-23-a-1
vues: 1
image_imprime:
redirect_from:
- ra-23-a
- ark:/87276/a_ra_23_a
id_ark: a_ra_23_a
precisions_biblio:
- clef: bulletin_1936
  note: p. 587
- clef: cazes_musee_1999-1
  note: p. 80
- clef: du_mege_description_1835
  note: n° 258
- clef: du_mege_notice_1828
  note: n° 110
- clef: joulin_les_1901
  note: fig. 2 b
- clef: massendari_haute-garonne_2006-1
  note: p. 238, fig. 96
- clef: nodier_voyages_1833
  note: pl. 31 bis
- clef: rachou_catalogue_1912
  note: n° 23 a
- clef: roschach_catalogue_1892
  note: n° 23 a

---

The double-curved moulding of this pilaster is decorated with palm leaves. A large scroll of foliage and flowers inhabited by birds, butterflies, a lizard, a snake, a snail and a cricket emerge from the lower part of a magnificent acanthus plant.

Scrolls sprouting from the base of an acanthus plant in the form of a candelabrum or, as in these examples found in Chiragan, a succession of volutes ending in flowers, have been a common motif since Greek times. G. Sauron underlined the symbolic significance of the acanthus, so profusely represented on the walls of the marble enclosure of the Altar of Augustan Peace (*Ara Pacis Augustae*), inaugurated in 13 BC, where this plant was represented on "the largest preserved surface scrolls still extant from Greek and Roman antiquity" {% cite sauron_histoire_2000 -l 30 %}. For the monument commissioned by Augustus marked the return of the golden age, after the civil wars that Caesar's heir had managed to curtail. In other words, this plant as well as animals were put at the service of a political programme, under the aegis of Apollo and vigorously substantiated by the poetry of Virgil {% cite sauron_histoire_2000 -L none -l p. 76-80 et 115-116 %}. The composition proved a great success.

It is possible that the great pilasters in Chiragan served to separate the reliefs of Hercules. Assuming that the medallions of the gods were added to this large marble decoration, the ensemble would then have formed a sort of vast allegorical poem. Victory and renewal were thus placed under divine authority, vectors of good fortune to which the luxuriance of the acanthus plants inhabited by small creatures, also contributed.

P. Capus
