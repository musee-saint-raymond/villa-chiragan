---
title: Fragment of a pilaster or door jamb
id_inventaire: Ra 23 f
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Marble
hauteur: "93"
largeur: "98"
longueur:
profondeur: '14,5'
epaisseur:
id_wikidata: Q48409284
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 360
priority: 3
id_notice: ra-23-f
image_principale: ra-23-f-1
vues: 1
image_imprime:
redirect_from:
- ra-23-f
- ark:/87276/a_ra_23_f
id_ark: a_ra_23_f
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 80
- clef: du_mege_description_1835
  note: n° 262
- clef: du_mege_notice_1828
  note: n° 113
- clef: joulin_les_1901
  note: fig. 1 b
- clef: massendari_haute-garonne_2006-1
  note: 'p. 238, fig. 96 '
- clef: rachou_catalogue_1912
  note: n° 23 f
- clef: roschach_catalogue_1892
  note: n° 23 f

---

The bird, lizard, snail and cricket that inhabit this scroll confirm that this pilaster, and at least two others, were designed by the same sculptor.
