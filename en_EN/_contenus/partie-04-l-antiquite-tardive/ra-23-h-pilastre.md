---
title: Fragment of a pilaster or door jamb
id_inventaire: Ra 23 h
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Marble
hauteur: 79
largeur: "44"
longueur:
profondeur: 8
epaisseur:
id_wikidata: Q48314948
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 350
priority: 3
id_notice: ra-23-h
image_principale: ra-23-h-1
vues: 1
image_imprime:
redirect_from:
- ra-23-h
- ark:/87276/a_ra_23_h
id_ark: a_ra_23_h
precisions_biblio:
- clef: joulin_les_1901
  note: fig. 3 c
- clef: rachou_catalogue_1912
  note: n° 23 h
- clef: roschach_catalogue_1892
  note: n° 23 h

---

The decoration consists of four scrolls of intertwined ivy, its berries clearly raised from the background.
