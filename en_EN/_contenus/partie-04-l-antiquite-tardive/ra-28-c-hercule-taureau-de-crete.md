---
title: Hercules and the Cretan bull
id_inventaire: Ra 28 c
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "49"
largeur: "48"
longueur:
profondeur: "20"
epaisseur:
id_wikidata: Q24649914
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 160
priority: 3
id_notice: ra-28-c
image_principale: ra-28-c-1
vues: 1
image_imprime:
redirect_from:
- ra-28-c
- ark:/87276/a_ra_28_c
id_ark: a_ra_28_c
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 91
- clef: du_mege_description_1835
  note: n° 165
- clef: du_mege_notice_1828
  note: n° 77
- clef: joulin_les_1901
  note: n° 117 B
- clef: massendari_haute-garonne_2006-1
  note: 'p. 250, fig. 122 '
- clef: rachou_catalogue_1912
  note: n° 28 c
- clef: roschach_catalogue_1865
  note: n° 28 h

---

The seventh of twelve Labours is set on the island of Crete. Hercules managed to control the furious bull who was terrorising the island by grabbing him by the horns and bringing him back to his cousin Eurystheus.

Although only one identifiable fragment of the original large relief remains, it is of very high quality. Grasping a horn in one hand, Hercules encircles the bull's neck with his other arm, while the animal’s lolling tongue clearly expresses its impending demise.

P. Capus
