---
title: Hercules and Cerberus
id_inventaire: Ra 28 e
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "143"
largeur: '85,5'
longueur: ''
profondeur: "19"
epaisseur:
id_wikidata: Q24662338
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 140
priority: 3
id_notice: ra-28-e
image_principale: ra-28-e-1
vues: 1
image_imprime:
redirect_from:
- ra-28-e
- ark:/87276/a_ra_28_e
id_ark: a_ra_28_e
precisions_biblio:
- clef: esperandieu_recueil_1908
  note: p. 37, n° 899 (6)
- clef: joulin_les_1901
  note: pl. VIII, n° 92 B, 103 B, 117 B, 106 B
- clef: jourdain-annequin_heracles_1989
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 250, fig. 124
- clef: rachou_catalogue_1912
  note: p. 2c
- clef: roschach_catalogue_1865
  note: n° 28 h
  
---

The journey to the underworld, of which the three-headed dog Cerberus was the guardian, was Heracles/Hercules' very last Labour, a trial already present in Homer's work {% cite homere_iliade_nodate -L none -l chant 8, vers 362-369 %} {% cite homere_odyssee_nodate -L none -l chant 2, vers 623-626 %}. Alcmene's son is instructed not to kill but to bring this monster back from the world of the dead, from which, of course, no human could ever return. Hades/Pluto, master of this infernal kingdom, authorises Hercules to capture Cerberus on the condition that he remains unarmed. Having been bitten by the dragon that formed the tail end of the guardian of the underworld, Hercules grabs the dog by the neck. Unable to breathe, Cerberus allows himself to be dominated {% cite pseudo-apollodore_bibliotheque_nodate -L none -l II, 5, 12 %}. This tribute was then taken to Eurystheus, who was so terrified that he ordered his cousin to take it back to where he had found it.

On this relief, only the body of Hercules remains, yet the movement of the lion's pelt flapping behind him betrays his great agitation. One must no doubt imagine Cerberus, to the right of the hero, being dragged out of the underworld, the latter perhaps turning to look at this terrifying three-headed guardian. Hercules' attitude, which is similar to that often portrayed by artists on in the round works on sarcophagi or mosaics when illustrating this theme, has enabled us to identify this Labour {% cite noauthor_lexicon_1990 -L none -l 2649, 2656, 2661, 2662, 2664 %}.

There is no doubt that the theme of victory over everlasting night echoes throughout this infernal journey. We know that the hero has previously brushed with death, and maybe even seen it at close quarter during his passage through Erytheia, the island of the setting sun, with the three-headed monster Geryon, or with the Hesperides, daughters of the night, which is also symbolic of a frontier. The descent into the kingdom of the dead, or katabasis, is in no way specific to the myth of Hercules. It is an initiation whereby the soul is saved and elevated through apotheosis. In order to accomplish such an ordeal, Hercules must first be acquainted with the mysteries of the goddess Demeter in Eleusis, not far from Athens. This knowledge conditions communication with the underworld, and provides better protection in the dark, and greater knowledge of the kingdom of shadows. Only then can the dog of Hades be taken, which also conditions another victory: the granting of immortality by Zeus on Mount Oeta {% cite jourdain-annequin_heracles_1989 -l 511, 563-564 %}.

It is difficult not to think of the context of the trials faced by Emperor Maximian in those days, beginning with the peasant insurgents or Bagaudae in Gaul, and ending with the Berber separatist movements in Africa. This Herculean tale could not fail to resonate through these victorious battles, acting as a catalyst for imperial apotheosis, an elevation undoubtedly ardently desired.

P. Capus
