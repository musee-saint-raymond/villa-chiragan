---
title: Hercules and golden apples of the Hesperides
id_inventaire: Ra 28 f
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "148"
largeur: "63"
longueur:
profondeur: "21"
epaisseur:
id_wikidata: Q24662568
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 130
priority: 3
id_notice: ra-28-f
image_principale: ra-28-f-1
vues: 1
image_imprime:
redirect_from:
- ra-28-f
- ark:/87276/a_ra_28_f
id_ark: a_ra_28_f
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 98
- clef: du_mege_description_1835
  note: n° 172
- clef: du_mege_notice_1828
  note: n° 82
- clef: esperandieu_recueil_1908
  note: p. 36-37, n° 899 (4)
- clef: joulin_les_1901
  note: n° 91 B, 119 A, 105 B, 120 BDE
- clef: massendari_haute-garonne_2006-1
  note: 'p. 251, fig. 125 '
- clef: rachou_catalogue_1912
  note: p. 29, n° 28 k
- clef: roschach_catalogue_1865
  note: n° 28 i

---

The eleventh of twelve Labours, the trial that took place in the garden of the Hesperides is located on Mount Atlas, on the fringes of the known world. According to Hesiod, Atlas is situated "beyond the illustrious Ocean, towards the Empire of the Night, in the distant lands inhabited by the Hesperides with their sonorous voices" {% cite hesiode_theogonie_nodate -l 275 %}, and Apollodorus believed that this toponym represents Hyperborea, a mysterious northern land that served as Apollo’s winter residence {% cite pseudo-apollodore_bibliotheque_nodate bonnet_heracles_1992 -L none -l II, 5, 11 -l 289 %}. But it was mainly in *Lixus*, a Phoenician trading post founded on the Moroccan Atlantic coast, that the episode was located. Juno (the Greek Hera) was given golden apples by Gaia (goddess of the earth) as a wedding present. She had them guarded on the site of the setting sun by an immortal serpent and three "nymphs of evening": the Hesperides (from *Hesperos*, "the evening"). Gold, an inalterable, incorruptible metal that has always commanded the admiration of mankind, is seen here to symbolise the eternal life of the gods, and therefore immortality; that which Zeus granted to Hercules once he had completed his twelve Labours.

To pick these golden apples, the hero asks the giant Atlas to help him. But in other versions, Hercules kills the snake with an arrow and picks the apples himself. Eurystheus, not knowing what to do with the golden fruit, gave them back to Hercules, who chose to offer them to Minerva (the Greek Athena), the goddess who had supported him throughout all his trials. Although she subsequently returned them to the garden (*kepos*), as divine law forbade their removal from the sacred garden, Hercules's offering is given an allegorical meaning that alludes to reason and piety towards the gods. The hero is depicted as resisting the earthly passions that cause disorder and chaos {% cite pensabene_villa_2010 -l 42 %}.

The relief shows the completed Labour. Hercules, the quiver strap visible across his torso, holds the fruit in his left hand. In the upper right-hand corner among the leaves, we see the shaft of the arrow that killed the snake. In terms of its composition, this relief is very similar to the one depicting the death of the birds of Lake Stymphalia.

P. Capus
