---
title: Hercules and the birds of Lake Stymphalia
id_inventaire: Ra 28 g
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "130"
largeur: '90,5'
longueur:
profondeur: "16"
epaisseur:
id_wikidata: Q24659913
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 90
priority: 3
id_notice: ra-28-g
image_principale: ra-28-g-1
vues: 1
image_imprime:
redirect_from:
- ra-28-g
- ark:/87276/a_ra_28_g
id_ark: a_ra_28_g
precisions_biblio:
- clef: bergmann_chiragan_1999
  note: ''
- clef: cazes_musee_1999-1
  note: p. 89, 91
- clef: du_mege_description_1835
  note: n° 164
- clef: du_mege_notice_1828
  note: n° 76 bis
- clef: esperandieu_recueil_1908
  note: n° 10
- clef: joulin_les_1901
  note: n° 94 B
- clef: massendari_haute-garonne_2006-1
  note: 'p. 251, fig. 126 '
- clef: rachou_catalogue_1912
  note: n° 28 g
- clef: roschach_catalogue_1865
  note: n° 28 e
- clef: stirling_learned_2005
  note: ''
- clef: ziegle_hercule_2002
  note: p. 13
  
---

Lake Stymphalia in Arcadia, north of the Peloponnese, was surrounded by dense forests, and was home to a flock of carnivorous birds that terrified the people on its shores. Depending on the different versions of the myth, not only did the birds have bronze beaks and legs, they also boasted sharp metal feathers, which they shot at their enemies, likes arrows. It is easy to understand how these frightening birds were able to cause such havoc, destroy crops and kill people.

Heracles/Hercules's mission and sixth Labour was to eradicate them. To do so, he used the bow that had belonged to king Eurytus, one of his tutors, the latter having received it from the hands of Apollo himself. But Hercules chose to reinforce his weaponry and yet again demonstrated his cunning: in order to dislodge these monstrous birds from the trees, he used bronze percussion instruments in the form of castanets (*krotala*) given to him by Athena/Minerva and Hephaestus/Vulcan, as decoys. Diodorus, in his account, speaks of a "bronze drum" {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 13, 2 %}. In any case, the sound alarmed the birds, who flew out of the trees and, thus exposed, were shot one by one by arrows fired from the divine bow.

On this relief, Hercules, depicted with a quiver full of arrows across his back, watches the falling birds. At the top right-hand corner, a bird, pierced by an arrow, is seen falling from a tree. A second lies on the ground. Very strong similarities have been noticed between the impressive wings and claws of the birds in this panel and those of a dead eagle on a relief representing the myth of Prometheus in Aphrodisias {% cite bergmann_chiragan_1999 -l 62 %}. These details are convincing proof of the connection with workshops in Caria. As for Hercules himself, his body in this scene is the most awkward-looking portrayal in the entire cycle, marked as it is by a kind of torpor further accentuated by the right arm held away from the body. The abnormally enlarged muscles obviously contribute to his stiffness, visible in the highly developed ribcage, pectoralis, obliques, and rectus abdominis muscles, the geometrical inguinal bulge, and the marked and huge internal quadriceps above the kneecaps, that forms a rhombus and sharp lines. Such models are found in Cara, and in <a href="/images/comp-ra-28-g-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-28-g-1.jpg" alt="Bas-relief of Prometheus and Heracles from Sebasteion of Aphrosisias, Aphrodisias Museum, Egisto Sani CC BY-NC-SA, Flickr, https://www.flickr.com/photos/69716881@N02/42344930332"/></span>Aphrodisias</a> in particular, where workshops tended to emphasise bodies in a similar manner {% cite stirling_learned_2005 -l 57 %}.

P. Capus
