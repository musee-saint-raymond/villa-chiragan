---
title: Hercules and the queen of the Amazons
id_inventaire: Ra 28 h
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "146"
largeur: "66"
longueur: 
profondeur: "19"
epaisseur: 
id_wikidata: Q24659835
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 120
priority: 3
id_notice: ra-28-h
image_principale: ra-28-h-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-h
- ark:/87276/a_ra_28_h
id_ark: a_ra_28_h
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 94-95
- clef: du_mege_description_1835
  note: n° 167
- clef: du_mege_notice_1828
  note: n° 79
- clef: esperandieu_recueil_1908
  note: p. 37, n° 899, fig. 5
- clef: joulin_les_1901
  note: n° 96 B, D
- clef: massendari_haute-garonne_2006-1
  note: p. 251, fig. 127
- clef: rachou_catalogue_1912
  note: n° 28 h
- clef: roschach_catalogue_1865
  note: n° 28 f

---
The ninth of twelve Labours, this ordeal required Hercules/Heracles to travel to Asia Minor, to the lands of Hippolyta, queen of the Amazons in Cappadocia, where she was at the head of the legendary horsewomen warriors. The myth reflects the reversal of the roles of warfare and power, traditionally ensured by men but unusually assumed by women in these lands; a reversal that the Greeks believed to be synonymous with the great state of chaos that gripped the East {% cite jourdain-annequin_heracles_1992 -l 173 %}.

Hercules's journey to the mouth of the Therme River (Cappadocia) is motivated by his need to retrieve the golden girdle owned by Hippolyta. Because it was coveted by Eurystheus's daughter, the hero was ordered to steel the precious accessory. To do so, he was obliged to neutralise the hegemony of this tribe of women warriors by killing in single combat the twelve most eminent Amazons, and thus win the girdle. According to Apollodorus, Hera, a jealous and wrathful goddess, spread the rumour that Hercules aimed to kidnap the sovereign. The furious Amazons are said to have then engaged the hero in battle, which ended with the death of Hippolyta {% cite pseudo-apollodore_bibliotheque_nodate -L none -l II, 5, 9 %}.

On this relief, all that remains of Hercules is one foot. The horsewoman on the other hand, whether she embodies the queen or, more broadly, an Amazon, is well preserved. This oriental woman warrior wears a Thracian cap (_alopekis_) made of fox fur. In her right hand she holds a double-edged axe or labrys, while in the lower right-hand corner we see the crescent moon-shaped _pelte_, the distinctive, light and manoeuvrable shield used by the Amazons, which is characteristic of the armament favoured by oriental peoples. Her head is very similar to that of Attis, carved on one of the medallions of the gods, that may have been associated with this cycle.

In keeping with the other reliefs that make up the twelve Labours, the vitality of this scene is expressed by the fact that characters and objects extend beyond the frame. The tension of battle movements is also illustrated, in this scene in particular, by the shortened form of the horse, whose hindquarters, seen from behind, protrude from the image, while its front part, shown in very low relief, seems to be entering the panel. This technique, with which the masters of the Italian Renaissance were quite familiar, is known as _schiacciato_ ("compression"), a term that captures the intention to perfection, a highly subtle technique used to create a sense of depth. Ancient painting had long since adapted this method intended to create an illusion by means of an atmospheric monochrome perspective. And this lovely relief discovered in Chiragan is indeed pictorial in nature.

P. Capus
