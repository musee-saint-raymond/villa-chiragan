---
title: Bearded head of Hercules
id_inventaire: Ra 28 k
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: '29,5'
largeur: '23,5'
longueur:
profondeur: "22"
epaisseur:
id_wikidata: Q26720748
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 170
priority: 3
id_notice: ra-28-k
image_principale: ra-28-k-1
vues: 1
image_imprime:
redirect_from:
- ra-28-k
- ark:/87276/a_ra_28_k
id_ark: a_ra_28_k
precisions_biblio:
- clef: balty_les_2008
  note: p. 135 fig. 115
- clef: du_mege_description_1835
  note: n° 177
- clef: du_mege_notice_1828
  note: n° 85
- clef: esperandieu_recueil_1908
  note: n° 899, p. 37, fig. 4
- clef: joulin_les_1901
  note: n° 112 b
- clef: rachou_catalogue_1912
  note: n° 28 k
- clef: roschach_catalogue_1865
  note: n° 28 i

---

It is impossible to ascertain with any certainty to which relief depicting the Labours of Hercules this head may have belonged, although the beard does at least provide a clue as to its chronology: it could indeed relate to *[Hercules and](/en/ra-28-h) the Queen of the Amazons,* [*Hercules and the golden apples of the Hesperides*](/en/ra-28-f) or [*Hercules and Cerberus*](/en/ra-28-e). As stated in the note on the relief of *Hercules and the Lernaean Hydra*, the absence or presence of a beard is an indication of passing time, showing the effects of age from young adulthood to maturity. The hero’s facial hair therefore confirms his psychological growth.

The very prominent supraorbital ridge, the tapered locks of hair divided by means of a groove made with a straight chisel, and the deep hollows made with a trepanning tool, to clearly separate the generous curls of the beard, are so many signs that refer to a number of other heads that were once part of the *villa*’s lavish decor during Late Antiquity. Hercules's physiognomy is, in this case, very similar to that of the male deities depicted on shields, and Asclepius in particular.

P. Capus
