---
title: Minerva/Athena
id_inventaire: Ra 30
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "42"
largeur: "40"
longueur:
profondeur: '12,5'
epaisseur:
id_wikidata: Q26719033
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 180
priority: 3
id_notice: ra-30
image_principale: ra-30-1
vues: 1
image_imprime:
redirect_from:
- ra-30
- ark:/87276/a_ra_30
id_ark: a_ra_30
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 99
- clef: du_mege_description_1835
  note: p. 587
- clef: du_mege_notice_1828
  note: n° 93
- clef: esperandieu_recueil_1908
  note: n° 916
- clef: joulin_les_1901
  note: pl. IX, n° 113 B
- clef: massendari_haute-garonne_2006-1
  note: p. 252, fig. 131
- clef: rachou_catalogue_1912
  note: n° 30
- clef: roschach_catalogue_1892
  note: n° 30

---

This goddess actually helped Hercules with some of his Labours. The moulding, over which the figure's shield extends, indicates that this fragment belongs to the right-hand edge of one of the reliefs of Hercules. It could be the scene in which the son of Zeus and Alcmene smothers the Nemean lion. Only Hercules’s uncovered head has been found, which could be explained by the fact that he had not yet slain the lion, and was therefore not yet wearing its pelt.

P. Capus
