---
title: Head of quasi-colossus of Maximian Herculius
id_inventaire: Ra 34 b
donnees_biographiques1: Around 240/250-310, Emperor from 286 to 305 and from 306 to 310
donnees_biographiques2:
type_oeuvre:
date_creation: After 293
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "43"
largeur: "26"
longueur:
profondeur: '30,5'
epaisseur:
id_wikidata: Q25113430
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 10
priority: 3
id_notice: ra-34-b
image_principale: ra-34-b-1
vues: 8
image_imprime:
redirect_from:
- ra-34-b
- ark:/87276/a_ra_34_b
id_ark: a_ra_34_b
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 230, fig. 125 a
- clef: balty_les_2008
  note: ill. de couv., p. 13, 15, 24-25, 35, 37 fig. 5, 41 fig. 8, 45 fig. 14, 48, 50-51, 53 fig. 23, 127 fig 104, 128 fig. 106, 129 fig. 108
- clef: beckmann_idiom_2020
  note: p. 146-148, fig. 12
- clef: bergmann_chiragan_1999
  note: p. 26-44, pl. 9-3, 11-5, 12, 13-4
- clef: bergmann_ensemble_1995
  note: p. 198, fig. 2-3, p. 200
- clef: cazes_musee_1999-1
  note: p. 143
- clef: du_mege_description_1835
  note: p. 129-130, n° 230
- clef: ensoli_aurea_2000
  note: p. 459-460, n° 59
- clef: eppinger_hercules_2015
  note: p. 158
- clef: esperandieu_recueil_1908
  note: p. 32, n° 892.8
- clef: joulin_les_1901
  note: p. 304, pl. VI, n° 60
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig. 112
- clef: musee_saint-raymond_essentiel_2011
  note: p. 30-31
- clef: musee_saint-raymond_regard_1995
  note: p. 235, n° 171
- clef: rachou_catalogue_1912
  note: p. 32-33, n° 34 b
- clef: roschach_catalogue_1865
  note: p. 30, n° 50
- clef: rosso_image_2006
  note: p. 486-488, n° 236
- clef: turcan_constantin_2006
  note: p. 32, fig. 9

---

Diocletian was proclaimed emperor (Augustus) at the end of the year 284. Faced with the threats of foreign peoples on the *limes* (border) as well as the attempts made by certain generals to usurp his power, the sovereign appointed a co-emperor, Maximian, who was granted the title of Caesar. Both rulers were from Central Europe: Maximian, born in Pannonia between five and eight years before Diocletian, was originally from Dalmatia.

In 286, Diocletian raised Maximian to the rank of Augustus, a title that made him an emperor in his own right. This diarchy, or corule, which resembled that of Marcus Aurelius and Lucius Verus in the previous century, was however destined to evolve. Thus, on the 1<sup>st</sup> March 293, a new development in the history of imperial institutions was introduced in the form of a much more innovative form of government: the Tetrarchy. The two Augusti, Diocletian and Maximian, joined forces with two Caesars. Maximian, appointed Augustus of the West, was assisted by Constantius Chlorus, while Diocletian, appointed Augustus of the East, was assisted by Galerius. Maximian chose as his protector Hercules, a hero who had become immortal, and administered Italy, Raetia (a province between the Danube and the Veneto), Africa and Spain. His Caesar, Constantius Chlorus, father of the future Constantine the Great, devoted himself to the provinces of Gaul and Brittany (now Wales and England).

As explained in the introduction to this section, all the decorative elements made from marble sourced in Saint-Béat, as well as these four portraits probably belong to the period of this first Tetrarchy and seem to testify to the lavish restoration of *Villa* Chiragan under Maximian. The statue to which this head belonged must have stood 2.75 metres tall, meaning it was therefore larger than the second, very fragmentary representation of this same emperor, which forms a particularly impressive relief, the missing parts of which are now graphically reproduced in the museum.

The Emperor, who was not content to consider the now immortal Hercules as his personal tutelary god, chose to legitimise his power by insisting that he was actually the god’s direct descendant. He made him the patron saint of his family, which in turn became Herculean {% cite eppinger_hercules_2015 -l 158 %}. Finally, it is worth noting that in this portrait, the outline of the hair of the fringe, temples and beard, are unmistakably similar to those of the head of *Hercules fighting Diomedes* in the great sculptural cycle, and it is quite likely that both were displayed relatively close to each other.

According to J.-C. Balty, *Les portraits romains, La Tétrarchie, 1.5* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, 2008, p. 33-53.
