---
title: Diana (?)
id_inventaire: Ra 34 h
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "86"
largeur: "74"
longueur:
profondeur: "38"
epaisseur:
id_wikidata: Q48235114
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 230
priority: 3
id_notice: ra-34-h
image_principale: ra-34-h-3
vues: 5
image_imprime:
redirect_from:
- ra-34-h
- ark:/87276/a_ra_34_h
id_ark: a_ra_34_h
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 85
- clef: du_mege_description_1835
  note: n° 155
- clef: du_mege_notice_1828
  note: n° 68
- clef: esperandieu_recueil_1908
  note: n° 892.2
- clef: rachou_catalogue_1912
  note: n° 34 k
- clef: roschach_catalogue_1892
  note: n° 34 f, g, h

---

It is difficult to identify the goddess depicted in this sculpture. D. Cazes suggested that the smooth diadem could refer to Diana (the Greek Artemis), the goddess of hunting and wildlife. This daughter of Jupiter and Latona, whose main sanctuary was in Aricia in the Lazio (Latium) region, seems to have been very popular in domestic contexts during the 3rd and 4th centuries, where she is found in the form of statuettes. Very often her image accompanies those of Asclepius, Hercules and Pan, and this is indeed the case in Chiragan, despite the lack of evidence for the last of these gods for this period.

P. Capus
