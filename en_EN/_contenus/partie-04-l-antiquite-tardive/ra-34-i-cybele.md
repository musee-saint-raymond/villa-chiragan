---
title: Cybele
id_inventaire: Ra 34 i
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "91"
largeur: "72"
longueur:
profondeur: "40"
epaisseur:
id_wikidata: Q26720955
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 220
priority: 3
id_notice: ra-34-i
image_principale: ra-34-i-3
vues: 4
image_imprime:
redirect_from:
- ra-34-i
- ark:/87276/a_ra_34_i
id_ark: a_ra_34_i
precisions_biblio:
- clef: aupert_attis_1995
  note: ''
- clef: beckmann_idiom_2020
  note: p. 138-139, fig. 3
- clef: cazes_musee_1999-1
  note: p. 84
- clef: clarac_musee_1841
  note: p. 585
- clef: esperandieu_recueil_1908
  note: n° 892
- clef: rachou_catalogue_1912
  note: n° 34 i
- clef: roschach_catalogue_1892
  note: n° 34 c
- clef: sartre_orient_1991
  note: ''

---

Often referred to as "Great Mother of the Gods", Cybele was the patron saint of wildlife and fertility. Her cult had its roots in Phrygia (Anatolia) and gradually extended to the entire Mediterranean basin. In 204 BC, Rome, threatened by the Carthaginians and led by Hannibal, sent for the black stone (possibly a fragment of a meteorite) from the sanctuary of the goddess in Pessinus, Phrygia. The Great Mother of the Gods or *Mater deum magna* was then introduced into the sacred enclosure, and a temple was devoted to her on the Palatine Hill, where Rome originated. The cult, known as Metroac, gradually became synonymous with universal religion, and Cybele was adopted as a goddess of salvation for all levels of society {% cite sartre_orient_1991 -l 479 %}. Like Cybele, Attis, the paredra of the Universal Mother, also apparently enjoyed strong popularity under the Empire in the western provinces. Their cult became official in Lectoure (Gers) in particular; no less than twenty-two altars bore witness to the "regeneration of the forces of the Empire", from the time of the Antonines to the chaotic years of the military emperors during the 3rd century. The same probably happened, also in the 3rd century, in *Lugdunum Convenarum* (Saint-Bertrand-de-Comminges) {% cite aupert_attis_1995 -l 189-192 %}.

The damaged crown of the Great Mother in Chiragan resembles a rampart with towers, and is covered with a veil. This headdress points to the goddess’s protective powers in times of conflict. Admittedly, there was no shortage of conflict in the West during the 3rd century, and again at the beginning of the following century. Yet the goddess’s presence in the *villa* mainly testifies to her integration into the congregation of gods that made up the traditional pantheon. All of them, hanging from their shield-shaped mounts in the most prestigious parts of the estate, watched over the *villa*. It is easy to imagine the scenography of which they were an integral part: hung on the upper parts of the walls above the Labours of Hercules and their wealth of political references, these gods embodied the powerful connection between East and West.

P. Capus
