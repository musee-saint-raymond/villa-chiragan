---
title: Minerva
id_inventaire: Ra 34 j
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup>-first third of the 4<sup>th</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "79"
largeur: "76"
longueur:
profondeur: "42"
epaisseur:
id_wikidata: Q27824574
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 280
priority: 3
id_notice: ra-34-j
image_principale: ra-34-j-3
vues: 5
image_imprime:
redirect_from:
- ra-34-j
- ark:/87276/a_ra_34_j
id_ark: a_ra_34_j
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 138, fig. 4
- clef: cazes_musee_1999-1
  note: p. 85
- clef: du_mege_description_1835
  note: n° 156
- clef: du_mege_notice_1828
  note: n° 69
- clef: esperandieu_recueil_1908
  note: n° 11
- clef: rachou_catalogue_1912
  note: n° 34 j
- clef: roschach_catalogue_1892
  note: n° 34 d

---

Goddess of handicraft, wisdom, intelligence and warfare, Minerva was the result of syncretism with the Greek goddess Athena, believed to have been born from the forehead of her father Zeus. Here she is portrayed wearing a helmet, which has been pushed backwards to show the two holes in the visor where the eyes should be. The bust of the goddess is protected by the aegis, a goatskin cuirass trimmed with snakes and adorned with the head of the Gorgon Medusa, who had the power to petrify whoever made eye contact with her. Often depicted in figurative form on breastplates, she was considered to be a protective force.

P. Capus
