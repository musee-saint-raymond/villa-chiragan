---
title: Theatre masks
id_inventaire: Ra 35
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Marble
hauteur:
longueur:
largeur:
profondeur:
epaisseur:
id_wikidata: Q28444879
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 190
priority: 3
id_notice: ra-35
image_principale: ra-35-1
vues: 1
image_imprime:
redirect_from:
- ra-35
- ark:/87276/a_ra_35
id_ark: a_ra_35
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 144
- clef: cazes_musee_1999-1
  note: p. 82
- clef: esperandieu_recueil_1908
  note: p. 58, n° 944
- clef: joulin_les_1901
  note: pl. VII, fig. 71
- clef: landes_gout_1989
  note: n° 73
- clef: rachou_catalogue_1912
  note: n° 35-36
- clef: soukaras_catalogue_1989
  note: ''

---

This ensemble is similar to the small tablets (*pinakes*) or marble friezes that, in Greco-Roman art, more or less directly evoked Dionysus (Bacchus to the Romans) the god of theatre. In the Greek world, this god of Thebes was often represented in the form of a simple bearded mask hanging from a decorated pillar. He is therefore the mask-god and one of the few deities whose worship ensures everlasting life. Thus, the masks depicted in the decorative elements can be seen as an allegory of transformation, that of the actor, and of the devotee hoping for a glorious afterlife.

Reproductions of theatre masks are not uncommon in the Greek world, in Attica, in southern Italy (on the island of Lipari in particular, where the terracotta masks found in tombs are a remarkable illustration of the different theatrical genres) or in Asia Minor. The Roman Empire has also left us some works of the same type, in marble, terracotta or bronze. These items placed in the sanctuaries, whether for decorative or votive purposes, were reminiscent of the real masks, made of leather and even wood, worn by the actors (*histriones*) of tragedies, comedies and satiric dramas.

Because only a small number of actors were present on stage, these masks allowed the same individual to play several parts (woman, slave or old man) in the same play. The accessory (designated in Latin by the word *persona*) identified the character as soon as s/she set foot on the stage, and also served as a voice amplifier. In his famous thesaurus compiled during the 2nd century, and known as the Onomasticon, Julius Pollux from Naucratis drew up a catalogue of the different types of masks used in theatrical performances. It lists seventy-six models of tragic, satiric or comic masks. This list more or less enables us to identify the characters, who were well known to the public, and immediately identifiable thanks to the colour of their skin, their hair and their hairstyle (namely the *onkos*, an elaborate hairstyle or headdress).

The collection of numerous small and medium-sized Dionysian sculptures and theatre masks that made up the most recent decor of the *villa* is, no doubt, far from exceptional. Yet it takes on a special connotation in Chiragan because such a large number of artefacts are still exceptionally extant. The staging of the reliefs, that is the in the round works, and most probably the mosaics (now lost), was most certainly the result of a complex combination of metaphors. Such iconographic syntax did exist elsewhere, in a very few residences throughout the Empire, such as *Villa* Noheda (Villar de Domingo García, Cuenca) during Late Antiquity, the spectacular mosaics of which were recently unearthed, and boast themes that are identical to those of the marble decorations discovered in Chiragan. As in this residence on the banks of the Garonne, mythology, Dionysism and the theatre are indeed intimately connected.

P. Capus
