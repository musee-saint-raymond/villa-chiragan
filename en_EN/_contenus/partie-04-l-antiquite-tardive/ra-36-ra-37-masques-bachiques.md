---
title: Bacchic masks
id_inventaire: Ra 36 - Ra 37
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation:
materiau: Marble
hauteur:
longueur:
largeur:
profondeur:
epaisseur:
id_wikidata: Q63758870
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 200
priority: 3
id_notice: ra-36-ra-37
image_principale: ra-36-ra-37-1
vues: 6
image_imprime:
redirect_from:
- ra-36-ra-37
- ark:/87276/a_ra_36_ra_37
id_ark: a_ra_36_ra_37
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 133-160
- clef: bergmann_chiragan_1999
  note: p. 26-43
- clef: bergmann_ensemble_1995
  note: p. 197-205
- clef: cazes_musee_1999-1
  note: p. 82
- clef: du_mege_description_1835
  note: n° 267
- clef: du_mege_monumens_1814
  note: p. 262-263, pl. III, n° 1
- clef: du_mege_notice_1828
  note: n° 64
- clef: ensoli_aurea_2000
  note: ''
- clef: esperandieu_recueil_1908
  note: p. 48, n° 922
- clef: guillevic_toulouse_1989
  note: n° 2
- clef: joulin_les_1901
  note: p. 8 et 92-93, pl. VII, 63 A à 70 A
- clef: martene_voyage_1717
  note: 2e partie, p. 34-35
- clef: rachou_catalogue_1912
  note: n° 37
- clef: roschach_catalogue_1892
  note: n° 37

---

These heads, which are hollowed out at the back and intended to be hung on a wall, were undoubtedly part of the *villa*’s lavish decor during Late Antiquity. And several details even betray the fact that they were all made by the same workshop: the wide strands of hair separated by deep chiselled grooves; the small and perfect circle formed by the curled ends with their deep drill holes; the similarity between the satyr's hairstyle, with its tuft of hair (7) and that of Eurystheus, in the large relief depicting Hercules and the Erymanthian boar; the eyebrows arched in the shape of an acute angle; the round pupils, hollowed out with a drill bit; and the empty, sinuously-shaped cavities inside their half-open mouths. You only have to compare some of these faces with the representations of Hercules (the hero's isolated naked head, the one on the relief of the Lernaean hydra, and that of the panel depicting the Erymanthian boar) to see the formal coherence that is specific to this one workshop, and similar to the methods implemented in certain sites in Asia Minor. Finally, it is worth mentioning the material that was used to create this original series, as recently confirmed by analysing most of the sculptures discovered around the *villa*. Following the example of the Labours of Hercules, the theatre masks, medallions of the gods, portraits of Maximian and his loved ones and, finally, the large series of small and medium-sized mythological representations, these so-called "bacchic" heads were also carved from marble mined in Saint-Béat.

They include a silenus \[1\], two *horae* (seasons) or maenads \[2, 4\], a child Bacchus (?) \[3\], Pan \[5\] and two satyrs \[6, 7\]. It is easy to imagine these marble heads hanging on the walls of a banqueting hall. One mosaic in particular, which decorates the floor of the *triclinium* in the House of the Labours of Hercules in Volubilis, features both the seasons and the challenges endured by this Greco-Roman hero {% cite noauthor_lexicon_1990 -L none -l Horai/Horae 168 %}. Could such a combination have existed (in marble form) in a similar area of the Chiragan estate? The association of these demigods refers to the world of Bacchus (the Greek Dionysus), and the succession of seasons symbolises eternity and constant renewal, which is also promised by the god. Here, these *horae* could represent summer, crowned with ears of corn, and spring, adorned with flower buds. These daughters of Zeus and Themis (goddess of fairness) are therefore in no way contradictory with a Dionysian (or Bacchic) environment. On the contrary, since mythology has, moreover, made these guardians of Olympus the nursemaids of the infant Bacchus/Dionysus. {% cite nonnos_de_panopolis_dionysiaques_450 -L none -l IX, 11 %}.

P. Capus
