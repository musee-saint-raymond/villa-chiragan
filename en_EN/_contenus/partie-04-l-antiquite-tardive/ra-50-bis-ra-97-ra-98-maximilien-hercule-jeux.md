---
title: Maximian Herculius (?) signalling the opening of the games
id_inventaire: Ra 50 bis, Ra 97 and Ra 98
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: 30 (head) - 51 (arm) - 35 (left hand)
largeur: 22.5 (head) – 23.5 (arm) - 19 (left hand)
longueur:
profondeur: 16.5 (head) – 17.5 (arm) – 14.5 (left hand)
epaisseur:
id_wikidata: Q26708104
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 50
priority: 3
id_notice: ra-50-bis-ra-97-ra-98
image_principale: ra-50-bis-ra-97-ra-98-1
vues: 1
image_imprime:
redirect_from:
- ra-50-bis-ra-97-ra-98
- ark:/87276/a_ra_50_bis_ra_97_ra_98
id_ark: a_ra_50_bis_ra_97_ra_98
precisions_biblio:
- clef: balty_les_2008
  note: p. 24-25-26, fig. 22-23, p. 54, 56, 58-61, 71
- clef: beckmann_idiom_2020
  note: p. 149-154, fig. 14
- clef: bergmann_chiragan_1999
  note: p. 34, n° 9, pl. 13.1-3
- clef: esperandieu_recueil_1908
  note: n° 895.2
- clef: joulin_les_1901
  note: n° 114 B, p. 92 (308), p. 108, fig. 220 B, pl. XIV, n° 201 B
- clef: rachou_catalogue_1912
  note: n° 50 bis, n°97-98

---

Thanks to two statues kept in the Capitoline Museums in Rome, we have been able to recreate this character’s attitude and clothing: he wears a long-sleeved tunic that reaches down to his ankles, covered by a second, slightly shorter, sleeveless tunic, and a purple toga (*toga picta*). In his right hand, he holds a *mappa*. This white cloth was thrown by the *editor* (the magistrate who was responsible for ordering the games in the arena or circus) to signal the beginning of a fight, or the start of a chariot race. Immediately, a *tuba* (a long bronze horn) was heard and the competition could begin. The left hand holds a sceptre (*scipio*), the emblem of power. The same individual undeniably served as the model for a monumental portrait, also made from marble mined in Saint-Béat that was unearthed in the *villa* grounds. It is believed to be that of Emperor Maximian Herculius.

In the years 293/296, invasions by migrants from North Africa, and raids by Frankish pirates on the Spanish coast forced Maximian to travel to these provinces. The palace of Cercadilla near Córdoba was built during this period. The central Pyrenees then served as a passage for legions from Germania and Gaul. It is from this period of the Tetrarchy (rule of four of the Empire) at the end of the 3rd century, that villa Chiragan enjoyed a period of bustling activity and important architectural transformations. The extraordinary decor created during those years was no doubt intended to celebrate the Emperor's battles, which were compared to those fought by Hercules, Maximian’s divine patron saint.

According to J.-C. Balty, *Les portraits romains, La Tétrarchie, 1.5* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, 2008, p. 54-74.
