---
title: Juvenile head of Maxentius (?)
id_inventaire: Ra 93 ter
donnees_biographiques1: Son of Maximian and Eutropia, around 278-312
donnees_biographiques2: Emperor from 306 to 312
type_oeuvre:
date_creation: After 293
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "33"
largeur: "22"
longueur:
profondeur: "23"
epaisseur:
id_wikidata: Q26707615
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 30
priority: 3
id_notice: ra-93-ter
image_principale: ra-93-ter-1
vues: 8
image_imprime:
redirect_from:
- ra-93-ter
- ark:/87276/a_ra_93_ter
id_ark: a_ra_93_ter
precisions_biblio:
- clef: balmelle_les_2001
  note: 'p. 230-231, fig. 125 c'
- clef: balty_les_2008
  note: p. 18-19, 76, 79-80, fig. 50, p. 82, fig. 53, p. 84, fig. 55, p. 86, fig 59, p. 89, fig. 65,67, p. 90
- clef: bergmann_chiragan_1999
  note: p. 34, 40-41
- clef: cazes_musee_1999-1
  note: p. 145
- clef: christof_unerkanntes_2003
  note: p. 49-54, ill. tafel 14
- clef: ensoli_aurea_2000
  note: p. 459, n° 58
- clef: joulin_les_1901
  note: p. 340, pl. XXV, n° 328
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig.114
- clef: musee_saint-raymond_regard_1995
  note: p. 236, n° 172
- clef: rosso_image_2006
  note: p. 489-490, n° 238

---

The hair here is depicted in the same way as that of the colossal male head, now acknowledged to be that of Maximian Herculius. These details, and others, allow us to assume that we are in the presence of the heir Maxentius, son of Maximian, who was also to reign but over a limited territory (at most Italy and the African provinces, which supplied Rome with wheat and oil). Five (and perhaps six) <a href="/images/comp-ra-93-ter-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-93-ter-1.jpg" alt="Head of Maxentius, August Kestner Hanovre Museum, Marcus Cyron / Wikimedia Commons CC BY-SA"/></span>sculpted portraits of the adult Maxentius</a> have been identified to date, but this is the head of an adolescent. The work may correspond to the time of his betrothal to Valeria Maximilla in 293, or shortly afterwards. He would therefore have been at least 13 or 14 years old.

According to J.-C. Balty, *Les portraits romains, La Tétrarchie, 1.5* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, 2008, p. 75-90.
